
bin/main.elf:     file format elf32-littlearm


Disassembly of section .text:

08000188 <__do_global_dtors_aux>:
 8000188:	b510      	push	{r4, lr}
 800018a:	4c05      	ldr	r4, [pc, #20]	; (80001a0 <__do_global_dtors_aux+0x18>)
 800018c:	7823      	ldrb	r3, [r4, #0]
 800018e:	b933      	cbnz	r3, 800019e <__do_global_dtors_aux+0x16>
 8000190:	4b04      	ldr	r3, [pc, #16]	; (80001a4 <__do_global_dtors_aux+0x1c>)
 8000192:	b113      	cbz	r3, 800019a <__do_global_dtors_aux+0x12>
 8000194:	4804      	ldr	r0, [pc, #16]	; (80001a8 <__do_global_dtors_aux+0x20>)
 8000196:	f3af 8000 	nop.w
 800019a:	2301      	movs	r3, #1
 800019c:	7023      	strb	r3, [r4, #0]
 800019e:	bd10      	pop	{r4, pc}
 80001a0:	200008f0 	.word	0x200008f0
 80001a4:	00000000 	.word	0x00000000
 80001a8:	08002ae4 	.word	0x08002ae4

080001ac <frame_dummy>:
 80001ac:	4b08      	ldr	r3, [pc, #32]	; (80001d0 <frame_dummy+0x24>)
 80001ae:	b510      	push	{r4, lr}
 80001b0:	b11b      	cbz	r3, 80001ba <frame_dummy+0xe>
 80001b2:	4808      	ldr	r0, [pc, #32]	; (80001d4 <frame_dummy+0x28>)
 80001b4:	4908      	ldr	r1, [pc, #32]	; (80001d8 <frame_dummy+0x2c>)
 80001b6:	f3af 8000 	nop.w
 80001ba:	4808      	ldr	r0, [pc, #32]	; (80001dc <frame_dummy+0x30>)
 80001bc:	6803      	ldr	r3, [r0, #0]
 80001be:	b903      	cbnz	r3, 80001c2 <frame_dummy+0x16>
 80001c0:	bd10      	pop	{r4, pc}
 80001c2:	4b07      	ldr	r3, [pc, #28]	; (80001e0 <frame_dummy+0x34>)
 80001c4:	2b00      	cmp	r3, #0
 80001c6:	d0fb      	beq.n	80001c0 <frame_dummy+0x14>
 80001c8:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
 80001cc:	4718      	bx	r3
 80001ce:	bf00      	nop
 80001d0:	00000000 	.word	0x00000000
 80001d4:	08002ae4 	.word	0x08002ae4
 80001d8:	200008f4 	.word	0x200008f4
 80001dc:	200008ec 	.word	0x200008ec
 80001e0:	00000000 	.word	0x00000000

080001e4 <_mainCRTStartup>:
 80001e4:	4b15      	ldr	r3, [pc, #84]	; (800023c <_mainCRTStartup+0x58>)
 80001e6:	2b00      	cmp	r3, #0
 80001e8:	bf08      	it	eq
 80001ea:	4b13      	ldreq	r3, [pc, #76]	; (8000238 <_mainCRTStartup+0x54>)
 80001ec:	469d      	mov	sp, r3
 80001ee:	f5a3 3a80 	sub.w	sl, r3, #65536	; 0x10000
 80001f2:	2100      	movs	r1, #0
 80001f4:	468b      	mov	fp, r1
 80001f6:	460f      	mov	r7, r1
 80001f8:	4813      	ldr	r0, [pc, #76]	; (8000248 <_mainCRTStartup+0x64>)
 80001fa:	4a14      	ldr	r2, [pc, #80]	; (800024c <_mainCRTStartup+0x68>)
 80001fc:	1a12      	subs	r2, r2, r0
 80001fe:	f002 fb3f 	bl	8002880 <memset>
 8000202:	4b0f      	ldr	r3, [pc, #60]	; (8000240 <_mainCRTStartup+0x5c>)
 8000204:	2b00      	cmp	r3, #0
 8000206:	d000      	beq.n	800020a <_mainCRTStartup+0x26>
 8000208:	4798      	blx	r3
 800020a:	4b0e      	ldr	r3, [pc, #56]	; (8000244 <_mainCRTStartup+0x60>)
 800020c:	2b00      	cmp	r3, #0
 800020e:	d000      	beq.n	8000212 <_mainCRTStartup+0x2e>
 8000210:	4798      	blx	r3
 8000212:	2000      	movs	r0, #0
 8000214:	2100      	movs	r1, #0
 8000216:	0004      	movs	r4, r0
 8000218:	000d      	movs	r5, r1
 800021a:	480d      	ldr	r0, [pc, #52]	; (8000250 <_mainCRTStartup+0x6c>)
 800021c:	2800      	cmp	r0, #0
 800021e:	d002      	beq.n	8000226 <_mainCRTStartup+0x42>
 8000220:	480c      	ldr	r0, [pc, #48]	; (8000254 <_mainCRTStartup+0x70>)
 8000222:	f002 fbef 	bl	8002a04 <atexit>
 8000226:	f002 fb03 	bl	8002830 <__libc_init_array>
 800022a:	0020      	movs	r0, r4
 800022c:	0029      	movs	r1, r5
 800022e:	f002 f9e9 	bl	8002604 <main>
 8000232:	f002 faed 	bl	8002810 <exit>
 8000236:	bf00      	nop
 8000238:	00080000 	.word	0x00080000
	...
 8000248:	200008f0 	.word	0x200008f0
 800024c:	20009a80 	.word	0x20009a80
 8000250:	08002a05 	.word	0x08002a05
 8000254:	08002a11 	.word	0x08002a11

08000258 <CTX>:
    .TYPE	SRX,	%function
    .TYPE	PREG,	%function
    .TYPE	PDEC,	%function

CTX:
	MOV	R2,	R0
 8000258:	4602      	mov	r2, r0

0800025a <WAIT_TXE>:
WAIT_TXE:
	LRV	R3,	APB2,	USART6,	USART_SR
 800025a:	4829      	ldr	r0, [pc, #164]	; (8000300 <DEC_LOOP+0x20>)
 800025c:	6803      	ldr	r3, [r0, #0]
	TST	R3,	#(1 << 7)
 800025e:	f013 0f80 	tst.w	r3, #128	; 0x80
	BEQ	WAIT_TXE
 8000262:	d0fa      	beq.n	800025a <WAIT_TXE>

08000264 <SEND_C>:

SEND_C:
	OSRC	R2,	USART_DR,	B
 8000264:	7102      	strb	r2, [r0, #4]

08000266 <WAIT_TC>:

WAIT_TC:
	LRVC	R3,	USART_SR
 8000266:	6803      	ldr	r3, [r0, #0]
	TST	R3,	#(1 << 6)
 8000268:	f013 0f40 	tst.w	r3, #64	; 0x40
	BEQ	WAIT_TC
 800026c:	d0fb      	beq.n	8000266 <WAIT_TC>
	BX	LR
 800026e:	4770      	bx	lr

08000270 <CRX>:

@ -------------------------------------

CRX:
WAIT_RXNE:
	LRV	R3,	APB2,	USART6,	USART_SR
 8000270:	4823      	ldr	r0, [pc, #140]	; (8000300 <DEC_LOOP+0x20>)
 8000272:	6803      	ldr	r3, [r0, #0]
	TST	R3,	#(1 << 5)
 8000274:	f013 0f20 	tst.w	r3, #32
	BEQ	WAIT_RXNE
 8000278:	d0fa      	beq.n	8000270 <CRX>

0800027a <READ_C>:

READ_C:
	LRVC	R0,	USART_DR,	B
 800027a:	7900      	ldrb	r0, [r0, #4]
	BX	LR
 800027c:	4770      	bx	lr

0800027e <SRX>:

@ -------------------------------------

SRX:
	PUSH	{LR}
 800027e:	b500      	push	{lr}
	MOV	R1,	R0
 8000280:	4601      	mov	r1, r0

08000282 <RX_UNTIL_NEWLINE>:

RX_UNTIL_NEWLINE:
	BL	CRX
 8000282:	f7ff fff5 	bl	8000270 <CRX>

	TEQ	R0,	#0xA	@ Get out when R0 = 0xA (NOT 0x10 WTF) aka \n
 8000286:	f090 0f0a 	teq	r0, #10
	BEQ	EXIT_SRX
 800028a:	d002      	beq.n	8000292 <EXIT_SRX>

	STRB	R0,	[R1],	#0x1
 800028c:	f801 0b01 	strb.w	r0, [r1], #1

	B	RX_UNTIL_NEWLINE
 8000290:	e7f7      	b.n	8000282 <RX_UNTIL_NEWLINE>

08000292 <EXIT_SRX>:

EXIT_SRX:
	POP	{PC}
 8000292:	bd00      	pop	{pc}

08000294 <STX>:

@ -------------------------------------

STX:
	PUSH	{LR}
 8000294:	b500      	push	{lr}
	MOV	R1,	R0
 8000296:	4601      	mov	r1, r0

08000298 <RC_SEND>:

RC_SEND:
	LDRB	R0,	[R1],	#0x1
 8000298:	f811 0b01 	ldrb.w	r0, [r1], #1

	TST	R0,	#0xFF	@ Get out when R2 = 0x0
 800029c:	f010 0fff 	tst.w	r0, #255	; 0xff
	BEQ	EXIT_STR
 80002a0:	d002      	beq.n	80002a8 <EXIT_STR>

	BL	CTX
 80002a2:	f7ff ffd9 	bl	8000258 <CTX>
	B	RC_SEND
 80002a6:	e7f7      	b.n	8000298 <RC_SEND>

080002a8 <EXIT_STR>:

EXIT_STR:
	POP	{PC}
 80002a8:	bd00      	pop	{pc}

080002aa <PREG>:

@ -------------------------------------

PREG:
	PUSH	{R4, LR}
 80002aa:	b510      	push	{r4, lr}
	MOV	R1,	R0	@ Save Register
 80002ac:	4601      	mov	r1, r0
	MOV	R4, #8		@ Counter
 80002ae:	f04f 0408 	mov.w	r4, #8

080002b2 <HEX_SEND>:

	MOV	R2,	#'x'
	BL	CTX
*/
HEX_SEND:
	ROR	R1, R1, #28
 80002b2:	ea4f 7131 	mov.w	r1, r1, ror #28
	AND	R0, R1,	#0xF	@ Extract 1 digit
 80002b6:	f001 000f 	and.w	r0, r1, #15

	CMP	R0, #0xA
 80002ba:	280a      	cmp	r0, #10
	ITE	GE
 80002bc:	bfac      	ite	ge

	ADDGE	R0, #55		@ Convert to ASCII (Capital letters)
 80002be:	3037      	addge	r0, #55	; 0x37
	ADDLT	R0, #48 	@ Just use number 0-9
 80002c0:	3030      	addlt	r0, #48	; 0x30
	BL	CTX
 80002c2:	f7ff ffc9 	bl	8000258 <CTX>

	SUBS	R4, #0x1
 80002c6:	3c01      	subs	r4, #1
	BNE	HEX_SEND
 80002c8:	d1f3      	bne.n	80002b2 <HEX_SEND>

	POP	{R4, PC}
 80002ca:	bd10      	pop	{r4, pc}

080002cc <PDEC>:
@ Send Register value in Decimal
@ R0: SP; R1: Temp; R2: Register value;
@ R3: R2 / 10; R4: R2 - (R3 * 10) => Remainder


	PUSH	{R4, R5, LR}
 80002cc:	b530      	push	{r4, r5, lr}
	MOV	R2,	R0
 80002ce:	4602      	mov	r2, r0
	MOV	R0,	SP
 80002d0:	4668      	mov	r0, sp
	SUB	SP,	SP,	#12
 80002d2:	b083      	sub	sp, #12
	MOV	R1,	#0x0
 80002d4:	f04f 0100 	mov.w	r1, #0
	STRB	R1,	[R0, #-1]!
 80002d8:	f800 1d01 	strb.w	r1, [r0, #-1]!

	MOV	R5, #10
 80002dc:	f04f 050a 	mov.w	r5, #10

080002e0 <DEC_LOOP>:

DEC_LOOP:
	UDIV	R3,	R2,	R5
 80002e0:	fbb2 f3f5 	udiv	r3, r2, r5
	MUL	R1,	R3,	R5
 80002e4:	fb03 f105 	mul.w	r1, r3, r5
	SUB	R4,	R2,	R1
 80002e8:	eba2 0401 	sub.w	r4, r2, r1
	ADD	R4,	R4,	#48
 80002ec:	f104 0430 	add.w	r4, r4, #48	; 0x30
	STRB	R4,	[R0, #-1]!
 80002f0:	f800 4d01 	strb.w	r4, [r0, #-1]!

	MOVS	R2,	R3
 80002f4:	001a      	movs	r2, r3
	BNE	DEC_LOOP
 80002f6:	d1f3      	bne.n	80002e0 <DEC_LOOP>

	BL	STX
 80002f8:	f7ff ffcc 	bl	8000294 <STX>
	ADD	SP,	SP,	#12
 80002fc:	b003      	add	sp, #12

	@ MOV	R0,	#'\n'
	@ BL	CTX

	POP	{R4, R5, PC}
 80002fe:	bd30      	pop	{r4, r5, pc}
    .TYPE	PDEC,	%function

CTX:
	MOV	R2,	R0
WAIT_TXE:
	LRV	R3,	APB2,	USART6,	USART_SR
 8000300:	40011400 	.word	0x40011400
 8000304:	08037fb4 	.word	0x08037fb4
 8000308:	20000000 	.word	0x20000000
 800030c:	200008ec 	.word	0x200008ec
 8000310:	200008f0 	.word	0x200008f0
 8000314:	20009a80 	.word	0x20009a80

08000318 <ads1299_pwr_up_seq>:
}



void ads1299_pwr_up_seq()
{
 8000318:	b510      	push	{r4, lr}
    // WAIT 40ms
    __DELAY(MILI_S(40));
    // PULL RESET LOW
    GPIOB->BSRRH |= GPIO_Pin_7;
 800031a:	4c0b      	ldr	r4, [pc, #44]	; (8000348 <ads1299_pwr_up_seq+0x30>)


void ads1299_pwr_up_seq()
{
    // WAIT 40ms
    __DELAY(MILI_S(40));
 800031c:	480b      	ldr	r0, [pc, #44]	; (800034c <ads1299_pwr_up_seq+0x34>)
 800031e:	f000 faa9 	bl	8000874 <__DELAY>
    // PULL RESET LOW
    GPIOB->BSRRH |= GPIO_Pin_7;
 8000322:	8b63      	ldrh	r3, [r4, #26]
 8000324:	b29b      	uxth	r3, r3
 8000326:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 800032a:	8363      	strh	r3, [r4, #26]
    // WAIT 2us
    __DELAY(MICRO_S(2));
 800032c:	2054      	movs	r0, #84	; 0x54
 800032e:	f000 faa1 	bl	8000874 <__DELAY>
    // PULL RESET HIGH
    GPIOB->BSRRL |= GPIO_Pin_7;
 8000332:	8b23      	ldrh	r3, [r4, #24]
 8000334:	b29b      	uxth	r3, r3
 8000336:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 800033a:	8323      	strh	r3, [r4, #24]
    // WAIT 10us
    __DELAY(MICRO_S(10));
 800033c:	f44f 70d2 	mov.w	r0, #420	; 0x1a4
}
 8000340:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
    // WAIT 2us
    __DELAY(MICRO_S(2));
    // PULL RESET HIGH
    GPIOB->BSRRL |= GPIO_Pin_7;
    // WAIT 10us
    __DELAY(MICRO_S(10));
 8000344:	f000 ba96 	b.w	8000874 <__DELAY>
 8000348:	40020400 	.word	0x40020400
 800034c:	0019a280 	.word	0x0019a280

08000350 <ads1299_stop_dataread>:
}

void ads1299_stop_dataread()
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 8000350:	4a12      	ldr	r2, [pc, #72]	; (800039c <ads1299_stop_dataread+0x4c>)
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000352:	4913      	ldr	r1, [pc, #76]	; (80003a0 <ads1299_stop_dataread+0x50>)
}

void ads1299_stop_dataread()
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 8000354:	8b53      	ldrh	r3, [r2, #26]
 8000356:	b29b      	uxth	r3, r3
 8000358:	f043 0380 	orr.w	r3, r3, #128	; 0x80
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800035c:	2011      	movs	r0, #17
}

void ads1299_stop_dataread()
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 800035e:	8353      	strh	r3, [r2, #26]
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000360:	460a      	mov	r2, r1
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000362:	8188      	strh	r0, [r1, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000364:	8913      	ldrh	r3, [r2, #8]
 8000366:	0798      	lsls	r0, r3, #30
 8000368:	d5fc      	bpl.n	8000364 <ads1299_stop_dataread+0x14>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 800036a:	4a0d      	ldr	r2, [pc, #52]	; (80003a0 <ads1299_stop_dataread+0x50>)
 800036c:	8913      	ldrh	r3, [r2, #8]
 800036e:	07d9      	lsls	r1, r3, #31
 8000370:	d5fc      	bpl.n	800036c <ads1299_stop_dataread+0x1c>
    // WAIT 10us
    __DELAY(MICRO_S(10));
}

void ads1299_stop_dataread()
{
 8000372:	b510      	push	{r4, lr}
    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000374:	4a0a      	ldr	r2, [pc, #40]	; (80003a0 <ads1299_stop_dataread+0x50>)
 8000376:	8913      	ldrh	r3, [r2, #8]
 8000378:	4c09      	ldr	r4, [pc, #36]	; (80003a0 <ads1299_stop_dataread+0x50>)
 800037a:	061b      	lsls	r3, r3, #24
 800037c:	d4fb      	bmi.n	8000376 <ads1299_stop_dataread+0x26>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 800037e:	2054      	movs	r0, #84	; 0x54
 8000380:	f000 fa78 	bl	8000874 <__DELAY>
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
    // SEND BYTE: 0x11
    SPI_TX(_SDATAC);
    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 8000384:	4a05      	ldr	r2, [pc, #20]	; (800039c <ads1299_stop_dataread+0x4c>)
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000386:	89a3      	ldrh	r3, [r4, #12]
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
    // SEND BYTE: 0x11
    SPI_TX(_SDATAC);
    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 8000388:	8b13      	ldrh	r3, [r2, #24]
 800038a:	b29b      	uxth	r3, r3
 800038c:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 8000390:	8313      	strh	r3, [r2, #24]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 8000392:	202a      	movs	r0, #42	; 0x2a
}
 8000394:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
    // SEND BYTE: 0x11
    SPI_TX(_SDATAC);
    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 8000398:	f000 ba6c 	b.w	8000874 <__DELAY>
 800039c:	40020c00 	.word	0x40020c00
 80003a0:	40013000 	.word	0x40013000

080003a4 <ads1299_read_data>:

void ads1299_read_data(uint32_t *STATUS, int32_t *DATA)
{

    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80003a4:	4a4c      	ldr	r2, [pc, #304]	; (80004d8 <ads1299_read_data+0x134>)
 80003a6:	8b53      	ldrh	r3, [r2, #26]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
}

void ads1299_read_data(uint32_t *STATUS, int32_t *DATA)
{
 80003a8:	b4f0      	push	{r4, r5, r6, r7}

    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80003aa:	b29b      	uxth	r3, r3


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80003ac:	4c4b      	ldr	r4, [pc, #300]	; (80004dc <ads1299_read_data+0x138>)

void ads1299_read_data(uint32_t *STATUS, int32_t *DATA)
{

    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80003ae:	f043 0380 	orr.w	r3, r3, #128	; 0x80


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80003b2:	2512      	movs	r5, #18

void ads1299_read_data(uint32_t *STATUS, int32_t *DATA)
{

    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80003b4:	8353      	strh	r3, [r2, #26]
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 80003b6:	4622      	mov	r2, r4


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80003b8:	81a5      	strh	r5, [r4, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 80003ba:	8913      	ldrh	r3, [r2, #8]
 80003bc:	079f      	lsls	r7, r3, #30
 80003be:	d5fc      	bpl.n	80003ba <ads1299_read_data+0x16>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 80003c0:	4a46      	ldr	r2, [pc, #280]	; (80004dc <ads1299_read_data+0x138>)
 80003c2:	8913      	ldrh	r3, [r2, #8]
 80003c4:	07de      	lsls	r6, r3, #31
 80003c6:	d5fc      	bpl.n	80003c2 <ads1299_read_data+0x1e>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 80003c8:	4c44      	ldr	r4, [pc, #272]	; (80004dc <ads1299_read_data+0x138>)
 80003ca:	8923      	ldrh	r3, [r4, #8]
 80003cc:	4a43      	ldr	r2, [pc, #268]	; (80004dc <ads1299_read_data+0x138>)
 80003ce:	f003 0380 	and.w	r3, r3, #128	; 0x80
 80003d2:	b29b      	uxth	r3, r3
 80003d4:	2b00      	cmp	r3, #0
 80003d6:	d1f8      	bne.n	80003ca <ads1299_read_data+0x26>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80003d8:	8994      	ldrh	r4, [r2, #12]


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80003da:	8193      	strh	r3, [r2, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 80003dc:	8913      	ldrh	r3, [r2, #8]
 80003de:	079d      	lsls	r5, r3, #30
 80003e0:	d5fc      	bpl.n	80003dc <ads1299_read_data+0x38>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 80003e2:	4a3e      	ldr	r2, [pc, #248]	; (80004dc <ads1299_read_data+0x138>)
 80003e4:	8913      	ldrh	r3, [r2, #8]
 80003e6:	07dc      	lsls	r4, r3, #31
 80003e8:	d5fc      	bpl.n	80003e4 <ads1299_read_data+0x40>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 80003ea:	4c3c      	ldr	r4, [pc, #240]	; (80004dc <ads1299_read_data+0x138>)
 80003ec:	8923      	ldrh	r3, [r4, #8]
 80003ee:	4a3b      	ldr	r2, [pc, #236]	; (80004dc <ads1299_read_data+0x138>)
 80003f0:	f003 0380 	and.w	r3, r3, #128	; 0x80
 80003f4:	b29b      	uxth	r3, r3
 80003f6:	2b00      	cmp	r3, #0
 80003f8:	d1f8      	bne.n	80003ec <ads1299_read_data+0x48>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80003fa:	8995      	ldrh	r5, [r2, #12]


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80003fc:	8193      	strh	r3, [r2, #12]
    GPIOD->BSRRH |= GPIO_Pin_7;

    // SEND BYTE: 0x11
    SPI_NO_DELAY_TX(_RDATA);
    // READ STATUS
    *STATUS = SPI_NO_DELAY_TX(0x0) << 16;
 80003fe:	b2ed      	uxtb	r5, r5
 8000400:	042d      	lsls	r5, r5, #16
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000402:	8913      	ldrh	r3, [r2, #8]
 8000404:	079b      	lsls	r3, r3, #30
 8000406:	d5fc      	bpl.n	8000402 <ads1299_read_data+0x5e>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000408:	4a34      	ldr	r2, [pc, #208]	; (80004dc <ads1299_read_data+0x138>)
 800040a:	8913      	ldrh	r3, [r2, #8]
 800040c:	07df      	lsls	r7, r3, #31
 800040e:	d5fc      	bpl.n	800040a <ads1299_read_data+0x66>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000410:	4c32      	ldr	r4, [pc, #200]	; (80004dc <ads1299_read_data+0x138>)
 8000412:	8923      	ldrh	r3, [r4, #8]
 8000414:	4a31      	ldr	r2, [pc, #196]	; (80004dc <ads1299_read_data+0x138>)
 8000416:	f003 0380 	and.w	r3, r3, #128	; 0x80
 800041a:	b29b      	uxth	r3, r3
 800041c:	2b00      	cmp	r3, #0
 800041e:	d1f8      	bne.n	8000412 <ads1299_read_data+0x6e>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000420:	8994      	ldrh	r4, [r2, #12]


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000422:	8193      	strh	r3, [r2, #12]

    // SEND BYTE: 0x11
    SPI_NO_DELAY_TX(_RDATA);
    // READ STATUS
    *STATUS = SPI_NO_DELAY_TX(0x0) << 16;
    *STATUS |= SPI_NO_DELAY_TX(0x0) << 8;
 8000424:	b2e3      	uxtb	r3, r4
 8000426:	ea45 2503 	orr.w	r5, r5, r3, lsl #8
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 800042a:	8913      	ldrh	r3, [r2, #8]
 800042c:	079e      	lsls	r6, r3, #30
 800042e:	d5fc      	bpl.n	800042a <ads1299_read_data+0x86>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000430:	4a2a      	ldr	r2, [pc, #168]	; (80004dc <ads1299_read_data+0x138>)
 8000432:	8913      	ldrh	r3, [r2, #8]
 8000434:	07dc      	lsls	r4, r3, #31
 8000436:	d5fc      	bpl.n	8000432 <ads1299_read_data+0x8e>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000438:	4a28      	ldr	r2, [pc, #160]	; (80004dc <ads1299_read_data+0x138>)
 800043a:	8914      	ldrh	r4, [r2, #8]
 800043c:	4b27      	ldr	r3, [pc, #156]	; (80004dc <ads1299_read_data+0x138>)
 800043e:	f004 0480 	and.w	r4, r4, #128	; 0x80
 8000442:	b2a4      	uxth	r4, r4
 8000444:	2c00      	cmp	r4, #0
 8000446:	d1f8      	bne.n	800043a <ads1299_read_data+0x96>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000448:	899a      	ldrh	r2, [r3, #12]
    // SEND BYTE: 0x11
    SPI_NO_DELAY_TX(_RDATA);
    // READ STATUS
    *STATUS = SPI_NO_DELAY_TX(0x0) << 16;
    *STATUS |= SPI_NO_DELAY_TX(0x0) << 8;
    *STATUS |= SPI_NO_DELAY_TX(0x0);
 800044a:	b2d2      	uxtb	r2, r2
 800044c:	4315      	orrs	r5, r2
 800044e:	6005      	str	r5, [r0, #0]
 8000450:	f101 0520 	add.w	r5, r1, #32
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000454:	4618      	mov	r0, r3


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000456:	819c      	strh	r4, [r3, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000458:	891a      	ldrh	r2, [r3, #8]
 800045a:	0792      	lsls	r2, r2, #30
 800045c:	d5fc      	bpl.n	8000458 <ads1299_read_data+0xb4>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 800045e:	891a      	ldrh	r2, [r3, #8]
 8000460:	07d7      	lsls	r7, r2, #31
 8000462:	d5fc      	bpl.n	800045e <ads1299_read_data+0xba>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000464:	891a      	ldrh	r2, [r3, #8]
 8000466:	f002 0280 	and.w	r2, r2, #128	; 0x80
 800046a:	b292      	uxth	r2, r2
 800046c:	2a00      	cmp	r2, #0
 800046e:	d1f9      	bne.n	8000464 <ads1299_read_data+0xc0>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000470:	8986      	ldrh	r6, [r0, #12]


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000472:	8182      	strh	r2, [r0, #12]
    *STATUS |= SPI_NO_DELAY_TX(0x0);

    // READ DATA 0 - 7
    int i = 0;
    for (i = 0; i < 8; i++) {
        DATA[i] = SPI_NO_DELAY_TX(0x0) << 16;
 8000474:	b2f6      	uxtb	r6, r6
 8000476:	0437      	lsls	r7, r6, #16
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000478:	891a      	ldrh	r2, [r3, #8]
 800047a:	0796      	lsls	r6, r2, #30
 800047c:	d5fc      	bpl.n	8000478 <ads1299_read_data+0xd4>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 800047e:	891a      	ldrh	r2, [r3, #8]
 8000480:	07d2      	lsls	r2, r2, #31
 8000482:	d5fc      	bpl.n	800047e <ads1299_read_data+0xda>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000484:	891a      	ldrh	r2, [r3, #8]
 8000486:	f002 0280 	and.w	r2, r2, #128	; 0x80
 800048a:	b292      	uxth	r2, r2
 800048c:	2a00      	cmp	r2, #0
 800048e:	d1f9      	bne.n	8000484 <ads1299_read_data+0xe0>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000490:	8986      	ldrh	r6, [r0, #12]


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000492:	8182      	strh	r2, [r0, #12]

    // READ DATA 0 - 7
    int i = 0;
    for (i = 0; i < 8; i++) {
        DATA[i] = SPI_NO_DELAY_TX(0x0) << 16;
        DATA[i] |= SPI_NO_DELAY_TX(0x0) << 8;
 8000494:	b2f2      	uxtb	r2, r6
 8000496:	ea47 2602 	orr.w	r6, r7, r2, lsl #8
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 800049a:	891a      	ldrh	r2, [r3, #8]
 800049c:	0797      	lsls	r7, r2, #30
 800049e:	d5fc      	bpl.n	800049a <ads1299_read_data+0xf6>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 80004a0:	891a      	ldrh	r2, [r3, #8]
 80004a2:	07d2      	lsls	r2, r2, #31
 80004a4:	d5fc      	bpl.n	80004a0 <ads1299_read_data+0xfc>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 80004a6:	891a      	ldrh	r2, [r3, #8]
 80004a8:	0617      	lsls	r7, r2, #24
 80004aa:	d4fc      	bmi.n	80004a6 <ads1299_read_data+0x102>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80004ac:	8982      	ldrh	r2, [r0, #12]
    // READ DATA 0 - 7
    int i = 0;
    for (i = 0; i < 8; i++) {
        DATA[i] = SPI_NO_DELAY_TX(0x0) << 16;
        DATA[i] |= SPI_NO_DELAY_TX(0x0) << 8;
        DATA[i] |= SPI_NO_DELAY_TX(0x0);
 80004ae:	b2d2      	uxtb	r2, r2
 80004b0:	4332      	orrs	r2, r6

        // Take care of the sign
        if (DATA[i] & 1 << 23) {
 80004b2:	0216      	lsls	r6, r2, #8
            DATA[i] ^= 0x00FFFFFF;
            DATA[i]++;
            DATA[i] &= 0x00FFFFFF;
            DATA[i] *= -1;
 80004b4:	bf48      	it	mi
 80004b6:	f042 427f 	orrmi.w	r2, r2, #4278190080	; 0xff000000
 80004ba:	600a      	str	r2, [r1, #0]
 80004bc:	3104      	adds	r1, #4
    *STATUS |= SPI_NO_DELAY_TX(0x0) << 8;
    *STATUS |= SPI_NO_DELAY_TX(0x0);

    // READ DATA 0 - 7
    int i = 0;
    for (i = 0; i < 8; i++) {
 80004be:	42a9      	cmp	r1, r5
 80004c0:	d1c9      	bne.n	8000456 <ads1299_read_data+0xb2>
            DATA[i] *= -1;
        }
    }

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 80004c2:	4a05      	ldr	r2, [pc, #20]	; (80004d8 <ads1299_read_data+0x134>)
 80004c4:	8b13      	ldrh	r3, [r2, #24]
 80004c6:	b29b      	uxth	r3, r3
 80004c8:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 80004cc:	8313      	strh	r3, [r2, #24]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 80004ce:	202a      	movs	r0, #42	; 0x2a
}
 80004d0:	bcf0      	pop	{r4, r5, r6, r7}
    }

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 80004d2:	f000 b9cf 	b.w	8000874 <__DELAY>
 80004d6:	bf00      	nop
 80004d8:	40020c00 	.word	0x40020c00
 80004dc:	40013000 	.word	0x40013000

080004e0 <ads1299_write_reg>:
}

void ads1299_write_reg(uint8_t ADDR, uint8_t VAL)
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80004e0:	4a26      	ldr	r2, [pc, #152]	; (800057c <ads1299_write_reg+0x9c>)
 80004e2:	8b53      	ldrh	r3, [r2, #26]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
}

void ads1299_write_reg(uint8_t ADDR, uint8_t VAL)
{
 80004e4:	b570      	push	{r4, r5, r6, lr}
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80004e6:	b29b      	uxth	r3, r3
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80004e8:	4c25      	ldr	r4, [pc, #148]	; (8000580 <ads1299_write_reg+0xa0>)
 80004ea:	f040 0040 	orr.w	r0, r0, #64	; 0x40
}

void ads1299_write_reg(uint8_t ADDR, uint8_t VAL)
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80004ee:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 80004f2:	8353      	strh	r3, [r2, #26]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
}

void ads1299_write_reg(uint8_t ADDR, uint8_t VAL)
{
 80004f4:	460e      	mov	r6, r1
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 80004f6:	4622      	mov	r2, r4
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80004f8:	81a0      	strh	r0, [r4, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 80004fa:	8913      	ldrh	r3, [r2, #8]
 80004fc:	0798      	lsls	r0, r3, #30
 80004fe:	d5fc      	bpl.n	80004fa <ads1299_write_reg+0x1a>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000500:	4a1f      	ldr	r2, [pc, #124]	; (8000580 <ads1299_write_reg+0xa0>)
 8000502:	8913      	ldrh	r3, [r2, #8]
 8000504:	07d9      	lsls	r1, r3, #31
 8000506:	d5fc      	bpl.n	8000502 <ads1299_write_reg+0x22>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000508:	4b1d      	ldr	r3, [pc, #116]	; (8000580 <ads1299_write_reg+0xa0>)
 800050a:	891c      	ldrh	r4, [r3, #8]
 800050c:	4d1c      	ldr	r5, [pc, #112]	; (8000580 <ads1299_write_reg+0xa0>)
 800050e:	f004 0480 	and.w	r4, r4, #128	; 0x80
 8000512:	b2a4      	uxth	r4, r4
 8000514:	2c00      	cmp	r4, #0
 8000516:	d1f8      	bne.n	800050a <ads1299_write_reg+0x2a>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 8000518:	2054      	movs	r0, #84	; 0x54
 800051a:	f000 f9ab 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 800051e:	89ab      	ldrh	r3, [r5, #12]
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000520:	81ac      	strh	r4, [r5, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000522:	892b      	ldrh	r3, [r5, #8]
 8000524:	079a      	lsls	r2, r3, #30
 8000526:	d5fc      	bpl.n	8000522 <ads1299_write_reg+0x42>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000528:	4a15      	ldr	r2, [pc, #84]	; (8000580 <ads1299_write_reg+0xa0>)
 800052a:	8913      	ldrh	r3, [r2, #8]
 800052c:	07db      	lsls	r3, r3, #31
 800052e:	d5fc      	bpl.n	800052a <ads1299_write_reg+0x4a>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000530:	4a13      	ldr	r2, [pc, #76]	; (8000580 <ads1299_write_reg+0xa0>)
 8000532:	8913      	ldrh	r3, [r2, #8]
 8000534:	4c12      	ldr	r4, [pc, #72]	; (8000580 <ads1299_write_reg+0xa0>)
 8000536:	061d      	lsls	r5, r3, #24
 8000538:	d4fb      	bmi.n	8000532 <ads1299_write_reg+0x52>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 800053a:	2054      	movs	r0, #84	; 0x54
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800053c:	b2b6      	uxth	r6, r6
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 800053e:	f000 f999 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000542:	89a3      	ldrh	r3, [r4, #12]
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000544:	81a6      	strh	r6, [r4, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000546:	8923      	ldrh	r3, [r4, #8]
 8000548:	0798      	lsls	r0, r3, #30
 800054a:	d5fc      	bpl.n	8000546 <ads1299_write_reg+0x66>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 800054c:	4a0c      	ldr	r2, [pc, #48]	; (8000580 <ads1299_write_reg+0xa0>)
 800054e:	8913      	ldrh	r3, [r2, #8]
 8000550:	07d9      	lsls	r1, r3, #31
 8000552:	d5fc      	bpl.n	800054e <ads1299_write_reg+0x6e>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000554:	4a0a      	ldr	r2, [pc, #40]	; (8000580 <ads1299_write_reg+0xa0>)
 8000556:	8913      	ldrh	r3, [r2, #8]
 8000558:	4c09      	ldr	r4, [pc, #36]	; (8000580 <ads1299_write_reg+0xa0>)
 800055a:	061b      	lsls	r3, r3, #24
 800055c:	d4fb      	bmi.n	8000556 <ads1299_write_reg+0x76>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 800055e:	2054      	movs	r0, #84	; 0x54
 8000560:	f000 f988 	bl	8000874 <__DELAY>
    SPI_TX(0x00);
    // SEND VALUE TO WRITE
    SPI_TX(VAL);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 8000564:	4a05      	ldr	r2, [pc, #20]	; (800057c <ads1299_write_reg+0x9c>)
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000566:	89a3      	ldrh	r3, [r4, #12]
    SPI_TX(0x00);
    // SEND VALUE TO WRITE
    SPI_TX(VAL);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 8000568:	8b13      	ldrh	r3, [r2, #24]
 800056a:	b29b      	uxth	r3, r3
 800056c:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 8000570:	8313      	strh	r3, [r2, #24]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 8000572:	202a      	movs	r0, #42	; 0x2a
}
 8000574:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
    SPI_TX(VAL);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 8000578:	f000 b97c 	b.w	8000874 <__DELAY>
 800057c:	40020c00 	.word	0x40020c00
 8000580:	40013000 	.word	0x40013000

08000584 <ads1299_init>:
   Finalized on 6/29/2014

*/

void ads1299_init()
{
 8000584:	b538      	push	{r3, r4, r5, lr}
    // ADS1299 Power up
    STX("Starting Power up sequence...\n");
 8000586:	4871      	ldr	r0, [pc, #452]	; (800074c <ads1299_init+0x1c8>)
void ads1299_pwr_up_seq()
{
    // WAIT 40ms
    __DELAY(MILI_S(40));
    // PULL RESET LOW
    GPIOB->BSRRH |= GPIO_Pin_7;
 8000588:	4c71      	ldr	r4, [pc, #452]	; (8000750 <ads1299_init+0x1cc>)
*/

void ads1299_init()
{
    // ADS1299 Power up
    STX("Starting Power up sequence...\n");
 800058a:	f7ff fe83 	bl	8000294 <STX>


void ads1299_pwr_up_seq()
{
    // WAIT 40ms
    __DELAY(MILI_S(40));
 800058e:	4871      	ldr	r0, [pc, #452]	; (8000754 <ads1299_init+0x1d0>)
 8000590:	f000 f970 	bl	8000874 <__DELAY>
    // PULL RESET LOW
    GPIOB->BSRRH |= GPIO_Pin_7;
 8000594:	8b63      	ldrh	r3, [r4, #26]
 8000596:	b29b      	uxth	r3, r3
 8000598:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 800059c:	8363      	strh	r3, [r4, #26]
    // WAIT 2us
    __DELAY(MICRO_S(2));
 800059e:	2054      	movs	r0, #84	; 0x54
 80005a0:	f000 f968 	bl	8000874 <__DELAY>
    // PULL RESET HIGH
    GPIOB->BSRRL |= GPIO_Pin_7;
 80005a4:	8b23      	ldrh	r3, [r4, #24]
 80005a6:	b29b      	uxth	r3, r3
 80005a8:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 80005ac:	8323      	strh	r3, [r4, #24]
    // WAIT 10us
    __DELAY(MICRO_S(10));
 80005ae:	f44f 70d2 	mov.w	r0, #420	; 0x1a4
 80005b2:	f000 f95f 	bl	8000874 <__DELAY>
void ads1299_init()
{
    // ADS1299 Power up
    STX("Starting Power up sequence...\n");
    ads1299_pwr_up_seq();
    __DELAY(MILI_S(1000));
 80005b6:	4868      	ldr	r0, [pc, #416]	; (8000758 <ads1299_init+0x1d4>)
 80005b8:	f000 f95c 	bl	8000874 <__DELAY>


void ads1299_pwr_up_seq()
{
    // WAIT 40ms
    __DELAY(MILI_S(40));
 80005bc:	4865      	ldr	r0, [pc, #404]	; (8000754 <ads1299_init+0x1d0>)
 80005be:	f000 f959 	bl	8000874 <__DELAY>
    // PULL RESET LOW
    GPIOB->BSRRH |= GPIO_Pin_7;
 80005c2:	8b63      	ldrh	r3, [r4, #26]
 80005c4:	b29b      	uxth	r3, r3
 80005c6:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 80005ca:	8363      	strh	r3, [r4, #26]
    // WAIT 2us
    __DELAY(MICRO_S(2));
 80005cc:	2054      	movs	r0, #84	; 0x54
 80005ce:	f000 f951 	bl	8000874 <__DELAY>
    // PULL RESET HIGH
    GPIOB->BSRRL |= GPIO_Pin_7;
 80005d2:	8b23      	ldrh	r3, [r4, #24]
 80005d4:	b29b      	uxth	r3, r3
 80005d6:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 80005da:	8323      	strh	r3, [r4, #24]
    // WAIT 10us
    __DELAY(MICRO_S(10));
 80005dc:	f44f 70d2 	mov.w	r0, #420	; 0x1a4
 80005e0:	f000 f948 	bl	8000874 <__DELAY>
    // ADS1299 Power up
    STX("Starting Power up sequence...\n");
    ads1299_pwr_up_seq();
    __DELAY(MILI_S(1000));
    ads1299_pwr_up_seq();
    STX("Sequence completed\n");
 80005e4:	485d      	ldr	r0, [pc, #372]	; (800075c <ads1299_init+0x1d8>)
 80005e6:	f7ff fe55 	bl	8000294 <STX>

    // Stop Conversion for Configuration
    STX("Sending Stop Data CMD...\n");
 80005ea:	485d      	ldr	r0, [pc, #372]	; (8000760 <ads1299_init+0x1dc>)
 80005ec:	f7ff fe52 	bl	8000294 <STX>
}

void ads1299_stop_dataread()
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80005f0:	495c      	ldr	r1, [pc, #368]	; (8000764 <ads1299_init+0x1e0>)
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80005f2:	4a5d      	ldr	r2, [pc, #372]	; (8000768 <ads1299_init+0x1e4>)
}

void ads1299_stop_dataread()
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80005f4:	8b4b      	ldrh	r3, [r1, #26]
 80005f6:	b29b      	uxth	r3, r3
 80005f8:	f043 0380 	orr.w	r3, r3, #128	; 0x80
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80005fc:	2011      	movs	r0, #17
}

void ads1299_stop_dataread()
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 80005fe:	834b      	strh	r3, [r1, #26]
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000600:	4613      	mov	r3, r2
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000602:	8190      	strh	r0, [r2, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000604:	891a      	ldrh	r2, [r3, #8]
 8000606:	0795      	lsls	r5, r2, #30
 8000608:	d5fc      	bpl.n	8000604 <ads1299_init+0x80>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 800060a:	4b57      	ldr	r3, [pc, #348]	; (8000768 <ads1299_init+0x1e4>)
 800060c:	891a      	ldrh	r2, [r3, #8]
 800060e:	07d4      	lsls	r4, r2, #31
 8000610:	d5fc      	bpl.n	800060c <ads1299_init+0x88>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000612:	4b55      	ldr	r3, [pc, #340]	; (8000768 <ads1299_init+0x1e4>)
 8000614:	891a      	ldrh	r2, [r3, #8]
 8000616:	4d54      	ldr	r5, [pc, #336]	; (8000768 <ads1299_init+0x1e4>)
 8000618:	0610      	lsls	r0, r2, #24
 800061a:	d4fb      	bmi.n	8000614 <ads1299_init+0x90>
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
    // SEND BYTE: 0x11
    SPI_TX(_SDATAC);
    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 800061c:	4c51      	ldr	r4, [pc, #324]	; (8000764 <ads1299_init+0x1e0>)
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 800061e:	2054      	movs	r0, #84	; 0x54
 8000620:	f000 f928 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000624:	89ab      	ldrh	r3, [r5, #12]
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
    // SEND BYTE: 0x11
    SPI_TX(_SDATAC);
    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 8000626:	8b23      	ldrh	r3, [r4, #24]
 8000628:	b29b      	uxth	r3, r3
 800062a:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 800062e:	8323      	strh	r3, [r4, #24]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 8000630:	202a      	movs	r0, #42	; 0x2a
 8000632:	f000 f91f 	bl	8000874 <__DELAY>


uint8_t ads1299_read_reg(uint8_t ADDR)
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 8000636:	8b63      	ldrh	r3, [r4, #26]
 8000638:	b29b      	uxth	r3, r3
 800063a:	f043 0380 	orr.w	r3, r3, #128	; 0x80
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800063e:	2220      	movs	r2, #32


uint8_t ads1299_read_reg(uint8_t ADDR)
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 8000640:	8363      	strh	r3, [r4, #26]
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000642:	462b      	mov	r3, r5
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000644:	81aa      	strh	r2, [r5, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000646:	891a      	ldrh	r2, [r3, #8]
 8000648:	0791      	lsls	r1, r2, #30
 800064a:	d5fc      	bpl.n	8000646 <ads1299_init+0xc2>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 800064c:	4b46      	ldr	r3, [pc, #280]	; (8000768 <ads1299_init+0x1e4>)
 800064e:	891a      	ldrh	r2, [r3, #8]
 8000650:	07d2      	lsls	r2, r2, #31
 8000652:	d5fc      	bpl.n	800064e <ads1299_init+0xca>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000654:	4b44      	ldr	r3, [pc, #272]	; (8000768 <ads1299_init+0x1e4>)
 8000656:	891c      	ldrh	r4, [r3, #8]
 8000658:	4d43      	ldr	r5, [pc, #268]	; (8000768 <ads1299_init+0x1e4>)
 800065a:	f004 0480 	and.w	r4, r4, #128	; 0x80
 800065e:	b2a4      	uxth	r4, r4
 8000660:	2c00      	cmp	r4, #0
 8000662:	d1f8      	bne.n	8000656 <ads1299_init+0xd2>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 8000664:	2054      	movs	r0, #84	; 0x54
 8000666:	f000 f905 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 800066a:	89ab      	ldrh	r3, [r5, #12]
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800066c:	81ac      	strh	r4, [r5, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 800066e:	892b      	ldrh	r3, [r5, #8]
 8000670:	079b      	lsls	r3, r3, #30
 8000672:	d5fc      	bpl.n	800066e <ads1299_init+0xea>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000674:	4b3c      	ldr	r3, [pc, #240]	; (8000768 <ads1299_init+0x1e4>)
 8000676:	891a      	ldrh	r2, [r3, #8]
 8000678:	07d4      	lsls	r4, r2, #31
 800067a:	d5fc      	bpl.n	8000676 <ads1299_init+0xf2>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 800067c:	4b3a      	ldr	r3, [pc, #232]	; (8000768 <ads1299_init+0x1e4>)
 800067e:	891c      	ldrh	r4, [r3, #8]
 8000680:	4d39      	ldr	r5, [pc, #228]	; (8000768 <ads1299_init+0x1e4>)
 8000682:	f004 0480 	and.w	r4, r4, #128	; 0x80
 8000686:	b2a4      	uxth	r4, r4
 8000688:	2c00      	cmp	r4, #0
 800068a:	d1f8      	bne.n	800067e <ads1299_init+0xfa>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 800068c:	2054      	movs	r0, #84	; 0x54
 800068e:	f000 f8f1 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000692:	89ab      	ldrh	r3, [r5, #12]
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000694:	81ac      	strh	r4, [r5, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000696:	892b      	ldrh	r3, [r5, #8]
 8000698:	0798      	lsls	r0, r3, #30
 800069a:	d5fc      	bpl.n	8000696 <ads1299_init+0x112>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 800069c:	4b32      	ldr	r3, [pc, #200]	; (8000768 <ads1299_init+0x1e4>)
 800069e:	891a      	ldrh	r2, [r3, #8]
 80006a0:	07d1      	lsls	r1, r2, #31
 80006a2:	d5fc      	bpl.n	800069e <ads1299_init+0x11a>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 80006a4:	4b30      	ldr	r3, [pc, #192]	; (8000768 <ads1299_init+0x1e4>)
 80006a6:	891a      	ldrh	r2, [r3, #8]
 80006a8:	4c2f      	ldr	r4, [pc, #188]	; (8000768 <ads1299_init+0x1e4>)
 80006aa:	0612      	lsls	r2, r2, #24
 80006ac:	d4fb      	bmi.n	80006a6 <ads1299_init+0x122>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 80006ae:	2054      	movs	r0, #84	; 0x54
 80006b0:	f000 f8e0 	bl	8000874 <__DELAY>
    SPI_TX(0x00);
    // SEND A DUMMY BYTE TO RECIEVE DATA
    uint8_t RESP = SPI_TX(0x00);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 80006b4:	4a2b      	ldr	r2, [pc, #172]	; (8000764 <ads1299_init+0x1e0>)
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80006b6:	89a4      	ldrh	r4, [r4, #12]
    SPI_TX(0x00);
    // SEND A DUMMY BYTE TO RECIEVE DATA
    uint8_t RESP = SPI_TX(0x00);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 80006b8:	8b13      	ldrh	r3, [r2, #24]
 80006ba:	b29b      	uxth	r3, r3
 80006bc:	f043 0380 	orr.w	r3, r3, #128	; 0x80
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80006c0:	b2a4      	uxth	r4, r4
    SPI_TX(0x00);
    // SEND A DUMMY BYTE TO RECIEVE DATA
    uint8_t RESP = SPI_TX(0x00);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 80006c2:	8313      	strh	r3, [r2, #24]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 80006c4:	202a      	movs	r0, #42	; 0x2a
    // Stop Conversion for Configuration
    STX("Sending Stop Data CMD...\n");
    ads1299_stop_dataread();

    // WHO AM I?
    if (ads1299_read_reg(ID) == 0x3E) STX("Life is good\n");
 80006c6:	b2e4      	uxtb	r4, r4
    uint8_t RESP = SPI_TX(0x00);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 80006c8:	f000 f8d4 	bl	8000874 <__DELAY>
    // Stop Conversion for Configuration
    STX("Sending Stop Data CMD...\n");
    ads1299_stop_dataread();

    // WHO AM I?
    if (ads1299_read_reg(ID) == 0x3E) STX("Life is good\n");
 80006cc:	2c3e      	cmp	r4, #62	; 0x3e
 80006ce:	d003      	beq.n	80006d8 <ads1299_init+0x154>
    else {
        STX("SHIT. I DON'T KNOW WHO I AM\n");
 80006d0:	4826      	ldr	r0, [pc, #152]	; (800076c <ads1299_init+0x1e8>)
 80006d2:	f7ff fddf 	bl	8000294 <STX>
        while (1);
 80006d6:	e7fe      	b.n	80006d6 <ads1299_init+0x152>
    // Stop Conversion for Configuration
    STX("Sending Stop Data CMD...\n");
    ads1299_stop_dataread();

    // WHO AM I?
    if (ads1299_read_reg(ID) == 0x3E) STX("Life is good\n");
 80006d8:	4825      	ldr	r0, [pc, #148]	; (8000770 <ads1299_init+0x1ec>)
 80006da:	f7ff fddb 	bl	8000294 <STX>
    }

    // Some Configs:
    // Turn on Reference buffer and Bias buffer in CONFIG3
    // Set Test signal configs in CONFIG2
    ads1299_write_reg(CONFIG3, 0x60 | 1 << 7 | 1 << 2);
 80006de:	2003      	movs	r0, #3
 80006e0:	21e4      	movs	r1, #228	; 0xe4
 80006e2:	f7ff fefd 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CONFIG2, 0xC0 | ADS1299_TEST_INT | ADS1299_TESTSIGNAL_PULSE_FAST);
 80006e6:	2002      	movs	r0, #2
 80006e8:	21d1      	movs	r1, #209	; 0xd1
 80006ea:	f7ff fef9 	bl	80004e0 <ads1299_write_reg>

    // All Channels are PGA = 24 | Normal Input | Powered down except Channel 8
    ads1299_write_reg(CH1SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_DOWN);
 80006ee:	2005      	movs	r0, #5
 80006f0:	21e0      	movs	r1, #224	; 0xe0
 80006f2:	f7ff fef5 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CH2SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_DOWN);
 80006f6:	2006      	movs	r0, #6
 80006f8:	21e0      	movs	r1, #224	; 0xe0
 80006fa:	f7ff fef1 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CH3SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_DOWN);
 80006fe:	2007      	movs	r0, #7
 8000700:	21e0      	movs	r1, #224	; 0xe0
 8000702:	f7ff feed 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CH4SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_DOWN);
 8000706:	2008      	movs	r0, #8
 8000708:	21e0      	movs	r1, #224	; 0xe0
 800070a:	f7ff fee9 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CH5SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_DOWN);
 800070e:	2009      	movs	r0, #9
 8000710:	21e0      	movs	r1, #224	; 0xe0
 8000712:	f7ff fee5 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CH6SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_DOWN);
 8000716:	200a      	movs	r0, #10
 8000718:	21e0      	movs	r1, #224	; 0xe0
 800071a:	f7ff fee1 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CH7SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_DOWN);
 800071e:	200b      	movs	r0, #11
 8000720:	21e0      	movs	r1, #224	; 0xe0
 8000722:	f7ff fedd 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(CH8SET, ADS1299_PGA_GAIN24 | ADS1299_INPUT_NORMAL | ADS1299_INPUT_PWR_UP);
 8000726:	200c      	movs	r0, #12
 8000728:	2160      	movs	r1, #96	; 0x60
 800072a:	f7ff fed9 	bl	80004e0 <ads1299_write_reg>

    // Configure Lead Off Options in LOFF: Set AC lead-off at 62.5hz (f_DR/4)
    ads1299_write_reg(LOFF, LOFF_FREQ_FS_4);
 800072e:	2004      	movs	r0, #4
 8000730:	2103      	movs	r1, #3
 8000732:	f7ff fed5 	bl	80004e0 <ads1299_write_reg>
    ads1299_write_reg(LOFF_SENSP, 0xFF);
 8000736:	200f      	movs	r0, #15
 8000738:	21ff      	movs	r1, #255	; 0xff
 800073a:	f7ff fed1 	bl	80004e0 <ads1299_write_reg>

    // Gimme time to check settings
    //__DELAY(MILI_S(2000));

    // Start the conversion
    GPIOB->BSRRL |= GPIO_Pin_6;
 800073e:	4a04      	ldr	r2, [pc, #16]	; (8000750 <ads1299_init+0x1cc>)
 8000740:	8b13      	ldrh	r3, [r2, #24]
 8000742:	b29b      	uxth	r3, r3
 8000744:	f043 0340 	orr.w	r3, r3, #64	; 0x40
 8000748:	8313      	strh	r3, [r2, #24]
 800074a:	bd38      	pop	{r3, r4, r5, pc}
 800074c:	08037b84 	.word	0x08037b84
 8000750:	40020400 	.word	0x40020400
 8000754:	0019a280 	.word	0x0019a280
 8000758:	0280de80 	.word	0x0280de80
 800075c:	08037ba4 	.word	0x08037ba4
 8000760:	08037bb8 	.word	0x08037bb8
 8000764:	40020c00 	.word	0x40020c00
 8000768:	40013000 	.word	0x40013000
 800076c:	08037be4 	.word	0x08037be4
 8000770:	08037bd4 	.word	0x08037bd4

08000774 <ads1299_read_reg>:


uint8_t ads1299_read_reg(uint8_t ADDR)
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 8000774:	4a27      	ldr	r2, [pc, #156]	; (8000814 <ads1299_read_reg+0xa0>)
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 8000776:	4928      	ldr	r1, [pc, #160]	; (8000818 <ads1299_read_reg+0xa4>)
    __DELAY(MICRO_S(1));
}


uint8_t ads1299_read_reg(uint8_t ADDR)
{
 8000778:	b538      	push	{r3, r4, r5, lr}
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 800077a:	8b53      	ldrh	r3, [r2, #26]
 800077c:	b29b      	uxth	r3, r3
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800077e:	f040 0020 	orr.w	r0, r0, #32


uint8_t ads1299_read_reg(uint8_t ADDR)
{
    // PULL CS LOW
    GPIOD->BSRRH |= GPIO_Pin_7;
 8000782:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 8000786:	8353      	strh	r3, [r2, #26]
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000788:	460a      	mov	r2, r1
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800078a:	8188      	strh	r0, [r1, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 800078c:	8913      	ldrh	r3, [r2, #8]
 800078e:	0798      	lsls	r0, r3, #30
 8000790:	d5fc      	bpl.n	800078c <ads1299_read_reg+0x18>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000792:	4a21      	ldr	r2, [pc, #132]	; (8000818 <ads1299_read_reg+0xa4>)
 8000794:	8913      	ldrh	r3, [r2, #8]
 8000796:	07d9      	lsls	r1, r3, #31
 8000798:	d5fc      	bpl.n	8000794 <ads1299_read_reg+0x20>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 800079a:	4b1f      	ldr	r3, [pc, #124]	; (8000818 <ads1299_read_reg+0xa4>)
 800079c:	891c      	ldrh	r4, [r3, #8]
 800079e:	4d1e      	ldr	r5, [pc, #120]	; (8000818 <ads1299_read_reg+0xa4>)
 80007a0:	f004 0480 	and.w	r4, r4, #128	; 0x80
 80007a4:	b2a4      	uxth	r4, r4
 80007a6:	2c00      	cmp	r4, #0
 80007a8:	d1f8      	bne.n	800079c <ads1299_read_reg+0x28>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 80007aa:	2054      	movs	r0, #84	; 0x54
 80007ac:	f000 f862 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80007b0:	89ab      	ldrh	r3, [r5, #12]
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80007b2:	81ac      	strh	r4, [r5, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 80007b4:	892b      	ldrh	r3, [r5, #8]
 80007b6:	079b      	lsls	r3, r3, #30
 80007b8:	d5fc      	bpl.n	80007b4 <ads1299_read_reg+0x40>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 80007ba:	4a17      	ldr	r2, [pc, #92]	; (8000818 <ads1299_read_reg+0xa4>)
 80007bc:	8913      	ldrh	r3, [r2, #8]
 80007be:	07dc      	lsls	r4, r3, #31
 80007c0:	d5fc      	bpl.n	80007bc <ads1299_read_reg+0x48>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 80007c2:	4b15      	ldr	r3, [pc, #84]	; (8000818 <ads1299_read_reg+0xa4>)
 80007c4:	891c      	ldrh	r4, [r3, #8]
 80007c6:	4d14      	ldr	r5, [pc, #80]	; (8000818 <ads1299_read_reg+0xa4>)
 80007c8:	f004 0480 	and.w	r4, r4, #128	; 0x80
 80007cc:	b2a4      	uxth	r4, r4
 80007ce:	2c00      	cmp	r4, #0
 80007d0:	d1f8      	bne.n	80007c4 <ads1299_read_reg+0x50>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 80007d2:	2054      	movs	r0, #84	; 0x54
 80007d4:	f000 f84e 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80007d8:	89ab      	ldrh	r3, [r5, #12]
}

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 80007da:	81ac      	strh	r4, [r5, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 80007dc:	892b      	ldrh	r3, [r5, #8]
 80007de:	0798      	lsls	r0, r3, #30
 80007e0:	d5fc      	bpl.n	80007dc <ads1299_read_reg+0x68>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 80007e2:	4a0d      	ldr	r2, [pc, #52]	; (8000818 <ads1299_read_reg+0xa4>)
 80007e4:	8913      	ldrh	r3, [r2, #8]
 80007e6:	07d9      	lsls	r1, r3, #31
 80007e8:	d5fc      	bpl.n	80007e4 <ads1299_read_reg+0x70>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 80007ea:	4a0b      	ldr	r2, [pc, #44]	; (8000818 <ads1299_read_reg+0xa4>)
 80007ec:	8913      	ldrh	r3, [r2, #8]
 80007ee:	4c0a      	ldr	r4, [pc, #40]	; (8000818 <ads1299_read_reg+0xa4>)
 80007f0:	061b      	lsls	r3, r3, #24
 80007f2:	d4fb      	bmi.n	80007ec <ads1299_read_reg+0x78>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 80007f4:	2054      	movs	r0, #84	; 0x54
 80007f6:	f000 f83d 	bl	8000874 <__DELAY>
    SPI_TX(0x00);
    // SEND A DUMMY BYTE TO RECIEVE DATA
    uint8_t RESP = SPI_TX(0x00);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 80007fa:	4a06      	ldr	r2, [pc, #24]	; (8000814 <ads1299_read_reg+0xa0>)
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 80007fc:	89a4      	ldrh	r4, [r4, #12]
    SPI_TX(0x00);
    // SEND A DUMMY BYTE TO RECIEVE DATA
    uint8_t RESP = SPI_TX(0x00);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 80007fe:	8b13      	ldrh	r3, [r2, #24]
 8000800:	b29b      	uxth	r3, r3
 8000802:	f043 0380 	orr.w	r3, r3, #128	; 0x80
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000806:	b2a4      	uxth	r4, r4
    SPI_TX(0x00);
    // SEND A DUMMY BYTE TO RECIEVE DATA
    uint8_t RESP = SPI_TX(0x00);

    // PULL CS HIGH
    GPIOD->BSRRL |= GPIO_Pin_7;
 8000808:	8313      	strh	r3, [r2, #24]
    // WAIT 2*TCLK'S = 888ns = 1us
    __DELAY(MICRO_S(1));
 800080a:	202a      	movs	r0, #42	; 0x2a
 800080c:	f000 f832 	bl	8000874 <__DELAY>
    // Return read value
    return (RESP);
}
 8000810:	b2e0      	uxtb	r0, r4
 8000812:	bd38      	pop	{r3, r4, r5, pc}
 8000814:	40020c00 	.word	0x40020c00
 8000818:	40013000 	.word	0x40013000

0800081c <SPI_TX>:

uint8_t SPI_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800081c:	4a0a      	ldr	r2, [pc, #40]	; (8000848 <SPI_TX+0x2c>)
 800081e:	8190      	strh	r0, [r2, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000820:	8913      	ldrh	r3, [r2, #8]
 8000822:	0798      	lsls	r0, r3, #30
 8000824:	d5fc      	bpl.n	8000820 <SPI_TX+0x4>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000826:	4a08      	ldr	r2, [pc, #32]	; (8000848 <SPI_TX+0x2c>)
 8000828:	8913      	ldrh	r3, [r2, #8]
 800082a:	07d9      	lsls	r1, r3, #31
 800082c:	d5fc      	bpl.n	8000828 <SPI_TX+0xc>
    // Return read value
    return (RESP);
}

uint8_t SPI_TX(uint8_t DATA)
{
 800082e:	b510      	push	{r4, lr}
    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 8000830:	4a05      	ldr	r2, [pc, #20]	; (8000848 <SPI_TX+0x2c>)
 8000832:	8913      	ldrh	r3, [r2, #8]
 8000834:	4c04      	ldr	r4, [pc, #16]	; (8000848 <SPI_TX+0x2c>)
 8000836:	061b      	lsls	r3, r3, #24
 8000838:	d4fb      	bmi.n	8000832 <SPI_TX+0x16>
    // WAIT 2uS - Give the chip some time 2 sort things out
    __DELAY(MICRO_S(2));
 800083a:	2054      	movs	r0, #84	; 0x54
 800083c:	f000 f81a 	bl	8000874 <__DELAY>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000840:	89a0      	ldrh	r0, [r4, #12]
}
 8000842:	b2c0      	uxtb	r0, r0
 8000844:	bd10      	pop	{r4, pc}
 8000846:	bf00      	nop
 8000848:	40013000 	.word	0x40013000

0800084c <SPI_NO_DELAY_TX>:


uint8_t SPI_NO_DELAY_TX(uint8_t DATA)
{
    // SEND DATA
    SPI1->DR = (uint8_t)DATA;
 800084c:	4a08      	ldr	r2, [pc, #32]	; (8000870 <SPI_NO_DELAY_TX+0x24>)
 800084e:	8190      	strh	r0, [r2, #12]

    // WAIT UNTIL TRANSMIT IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
 8000850:	8913      	ldrh	r3, [r2, #8]
 8000852:	0798      	lsls	r0, r3, #30
 8000854:	d5fc      	bpl.n	8000850 <SPI_NO_DELAY_TX+0x4>
    // WAIT UNTIL DATA RECIEVE IS COMPLETED
    while (!(SPI1->SR & SPI_I2S_FLAG_RXNE));
 8000856:	4a06      	ldr	r2, [pc, #24]	; (8000870 <SPI_NO_DELAY_TX+0x24>)
 8000858:	8913      	ldrh	r3, [r2, #8]
 800085a:	07d9      	lsls	r1, r3, #31
 800085c:	d5fc      	bpl.n	8000858 <SPI_NO_DELAY_TX+0xc>
    // WAIT UNTIL SPI IS NOT BUSY
    while (SPI1->SR & SPI_I2S_FLAG_BSY);
 800085e:	4904      	ldr	r1, [pc, #16]	; (8000870 <SPI_NO_DELAY_TX+0x24>)
 8000860:	890b      	ldrh	r3, [r1, #8]
 8000862:	4a03      	ldr	r2, [pc, #12]	; (8000870 <SPI_NO_DELAY_TX+0x24>)
 8000864:	061b      	lsls	r3, r3, #24
 8000866:	d4fb      	bmi.n	8000860 <SPI_NO_DELAY_TX+0x14>

    // RETURN RECIEVED DATA
    return (SPI1->DR);
 8000868:	8990      	ldrh	r0, [r2, #12]
}
 800086a:	b2c0      	uxtb	r0, r0
 800086c:	4770      	bx	lr
 800086e:	bf00      	nop
 8000870:	40013000 	.word	0x40013000

08000874 <__DELAY>:
#include "main.h"

void __DELAY(uint32_t cycles){
    int i;
    for (i = cycles; i > 0; i--) __asm__("nop");
 8000874:	2800      	cmp	r0, #0
 8000876:	dd02      	ble.n	800087e <__DELAY+0xa>
 8000878:	bf00      	nop
 800087a:	3801      	subs	r0, #1
 800087c:	d1fc      	bne.n	8000878 <__DELAY+0x4>
 800087e:	4770      	bx	lr

08000880 <EXTI9_5_IRQHandler>:
// These are 2D-arrays (integer and float): Array[DATA_POINTS][CHANNELS]
float32_t data_Signal_Float[ADS1299_CHANNELS][ADS1299_SIGNAL_WINDOW];
int32_t data_Signal_Integer[ADS1299_CHANNELS][ADS1299_SIGNAL_WINDOW];

void EXTI9_5_IRQHandler()
{
 8000880:	b5f0      	push	{r4, r5, r6, r7, lr}
 8000882:	b08b      	sub	sp, #44	; 0x2c
    int32_t REALS[8];
    uint32_t RDATA_STATUS;
    uint32_t i;

    ads1299_read_data(&RDATA_STATUS, REALS);
 8000884:	a902      	add	r1, sp, #8
 8000886:	a801      	add	r0, sp, #4
 8000888:	f7ff fd8c 	bl	80003a4 <ads1299_read_data>
 800088c:	4b7a      	ldr	r3, [pc, #488]	; (8000a78 <EXTI9_5_IRQHandler+0x1f8>)
 800088e:	4e7b      	ldr	r6, [pc, #492]	; (8000a7c <EXTI9_5_IRQHandler+0x1fc>)
 8000890:	681d      	ldr	r5, [r3, #0]
 8000892:	6834      	ldr	r4, [r6, #0]


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000894:	07e9      	lsls	r1, r5, #31
 8000896:	d516      	bpl.n	80008c6 <EXTI9_5_IRQHandler+0x46>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000898:	9a02      	ldr	r2, [sp, #8]
 800089a:	4b79      	ldr	r3, [pc, #484]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 800089c:	4f79      	ldr	r7, [pc, #484]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 800089e:	f843 2024 	str.w	r2, [r3, r4, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80008a2:	ee07 2a90 	vmov	s15, r2
 80008a6:	eef8 7ae7 	vcvt.f32.s32	s15, s15
 80008aa:	eb07 0784 	add.w	r7, r7, r4, lsl #2
 80008ae:	ee17 0a90 	vmov	r0, s15
 80008b2:	f001 faeb 	bl	8001e8c <__aeabi_f2d>
 80008b6:	a36e      	add	r3, pc, #440	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 80008b8:	e9d3 2300 	ldrd	r2, r3, [r3]
 80008bc:	f001 fb3a 	bl	8001f34 <__aeabi_dmul>
 80008c0:	f001 fd4a 	bl	8002358 <__aeabi_d2f>
 80008c4:	6038      	str	r0, [r7, #0]

    ads1299_read_data(&RDATA_STATUS, REALS);


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 80008c6:	06ea      	lsls	r2, r5, #27
 80008c8:	d518      	bpl.n	80008fc <EXTI9_5_IRQHandler+0x7c>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80008ca:	eddd 7a03 	vldr	s15, [sp, #12]


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80008ce:	4a6c      	ldr	r2, [pc, #432]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)
 80008d0:	9903      	ldr	r1, [sp, #12]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80008d2:	4f6c      	ldr	r7, [pc, #432]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80008d4:	f104 03fa 	add.w	r3, r4, #250	; 0xfa

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80008d8:	eef8 7ae7 	vcvt.f32.s32	s15, s15


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80008dc:	f842 1023 	str.w	r1, [r2, r3, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80008e0:	ee17 0a90 	vmov	r0, s15
 80008e4:	eb07 0783 	add.w	r7, r7, r3, lsl #2
 80008e8:	f001 fad0 	bl	8001e8c <__aeabi_f2d>
 80008ec:	a360      	add	r3, pc, #384	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 80008ee:	e9d3 2300 	ldrd	r2, r3, [r3]
 80008f2:	f001 fb1f 	bl	8001f34 <__aeabi_dmul>
 80008f6:	f001 fd2f 	bl	8002358 <__aeabi_d2f>
 80008fa:	6038      	str	r0, [r7, #0]

    ads1299_read_data(&RDATA_STATUS, REALS);


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 80008fc:	05eb      	lsls	r3, r5, #23
 80008fe:	d518      	bpl.n	8000932 <EXTI9_5_IRQHandler+0xb2>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000900:	eddd 7a04 	vldr	s15, [sp, #16]


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000904:	4a5e      	ldr	r2, [pc, #376]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)
 8000906:	9904      	ldr	r1, [sp, #16]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000908:	4f5e      	ldr	r7, [pc, #376]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 800090a:	f504 73fa 	add.w	r3, r4, #500	; 0x1f4

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 800090e:	eef8 7ae7 	vcvt.f32.s32	s15, s15


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000912:	f842 1023 	str.w	r1, [r2, r3, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000916:	ee17 0a90 	vmov	r0, s15
 800091a:	eb07 0783 	add.w	r7, r7, r3, lsl #2
 800091e:	f001 fab5 	bl	8001e8c <__aeabi_f2d>
 8000922:	a353      	add	r3, pc, #332	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 8000924:	e9d3 2300 	ldrd	r2, r3, [r3]
 8000928:	f001 fb04 	bl	8001f34 <__aeabi_dmul>
 800092c:	f001 fd14 	bl	8002358 <__aeabi_d2f>
 8000930:	6038      	str	r0, [r7, #0]

    ads1299_read_data(&RDATA_STATUS, REALS);


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000932:	04ef      	lsls	r7, r5, #19
 8000934:	d518      	bpl.n	8000968 <EXTI9_5_IRQHandler+0xe8>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000936:	eddd 7a05 	vldr	s15, [sp, #20]


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 800093a:	4a51      	ldr	r2, [pc, #324]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)
 800093c:	9905      	ldr	r1, [sp, #20]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 800093e:	4f51      	ldr	r7, [pc, #324]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000940:	f204 23ee 	addw	r3, r4, #750	; 0x2ee

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000944:	eef8 7ae7 	vcvt.f32.s32	s15, s15


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000948:	f842 1023 	str.w	r1, [r2, r3, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 800094c:	ee17 0a90 	vmov	r0, s15
 8000950:	eb07 0783 	add.w	r7, r7, r3, lsl #2
 8000954:	f001 fa9a 	bl	8001e8c <__aeabi_f2d>
 8000958:	a345      	add	r3, pc, #276	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 800095a:	e9d3 2300 	ldrd	r2, r3, [r3]
 800095e:	f001 fae9 	bl	8001f34 <__aeabi_dmul>
 8000962:	f001 fcf9 	bl	8002358 <__aeabi_d2f>
 8000966:	6038      	str	r0, [r7, #0]

    ads1299_read_data(&RDATA_STATUS, REALS);


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000968:	03e8      	lsls	r0, r5, #15
 800096a:	d518      	bpl.n	800099e <EXTI9_5_IRQHandler+0x11e>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 800096c:	eddd 7a06 	vldr	s15, [sp, #24]


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000970:	4a43      	ldr	r2, [pc, #268]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)
 8000972:	9906      	ldr	r1, [sp, #24]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000974:	4f43      	ldr	r7, [pc, #268]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000976:	f504 737a 	add.w	r3, r4, #1000	; 0x3e8

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 800097a:	eef8 7ae7 	vcvt.f32.s32	s15, s15


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 800097e:	f842 1023 	str.w	r1, [r2, r3, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000982:	ee17 0a90 	vmov	r0, s15
 8000986:	eb07 0783 	add.w	r7, r7, r3, lsl #2
 800098a:	f001 fa7f 	bl	8001e8c <__aeabi_f2d>
 800098e:	a338      	add	r3, pc, #224	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 8000990:	e9d3 2300 	ldrd	r2, r3, [r3]
 8000994:	f001 face 	bl	8001f34 <__aeabi_dmul>
 8000998:	f001 fcde 	bl	8002358 <__aeabi_d2f>
 800099c:	6038      	str	r0, [r7, #0]

    ads1299_read_data(&RDATA_STATUS, REALS);


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 800099e:	02e9      	lsls	r1, r5, #11
 80009a0:	d518      	bpl.n	80009d4 <EXTI9_5_IRQHandler+0x154>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009a2:	eddd 7a07 	vldr	s15, [sp, #28]


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80009a6:	4a36      	ldr	r2, [pc, #216]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)
 80009a8:	9907      	ldr	r1, [sp, #28]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009aa:	4f36      	ldr	r7, [pc, #216]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80009ac:	f204 43e2 	addw	r3, r4, #1250	; 0x4e2

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009b0:	eef8 7ae7 	vcvt.f32.s32	s15, s15


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80009b4:	f842 1023 	str.w	r1, [r2, r3, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009b8:	ee17 0a90 	vmov	r0, s15
 80009bc:	eb07 0783 	add.w	r7, r7, r3, lsl #2
 80009c0:	f001 fa64 	bl	8001e8c <__aeabi_f2d>
 80009c4:	a32a      	add	r3, pc, #168	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 80009c6:	e9d3 2300 	ldrd	r2, r3, [r3]
 80009ca:	f001 fab3 	bl	8001f34 <__aeabi_dmul>
 80009ce:	f001 fcc3 	bl	8002358 <__aeabi_d2f>
 80009d2:	6038      	str	r0, [r7, #0]

    ads1299_read_data(&RDATA_STATUS, REALS);


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 80009d4:	01ea      	lsls	r2, r5, #7
 80009d6:	d518      	bpl.n	8000a0a <EXTI9_5_IRQHandler+0x18a>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009d8:	eddd 7a08 	vldr	s15, [sp, #32]


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80009dc:	4a28      	ldr	r2, [pc, #160]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)
 80009de:	9908      	ldr	r1, [sp, #32]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009e0:	4f28      	ldr	r7, [pc, #160]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80009e2:	f204 53dc 	addw	r3, r4, #1500	; 0x5dc

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009e6:	eef8 7ae7 	vcvt.f32.s32	s15, s15


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 80009ea:	f842 1023 	str.w	r1, [r2, r3, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 80009ee:	ee17 0a90 	vmov	r0, s15
 80009f2:	eb07 0783 	add.w	r7, r7, r3, lsl #2
 80009f6:	f001 fa49 	bl	8001e8c <__aeabi_f2d>
 80009fa:	a31d      	add	r3, pc, #116	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 80009fc:	e9d3 2300 	ldrd	r2, r3, [r3]
 8000a00:	f001 fa98 	bl	8001f34 <__aeabi_dmul>
 8000a04:	f001 fca8 	bl	8002358 <__aeabi_d2f>
 8000a08:	6038      	str	r0, [r7, #0]

    ads1299_read_data(&RDATA_STATUS, REALS);


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000a0a:	00eb      	lsls	r3, r5, #3
 8000a0c:	d518      	bpl.n	8000a40 <EXTI9_5_IRQHandler+0x1c0>
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000a0e:	eddd 7a09 	vldr	s15, [sp, #36]	; 0x24


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000a12:	4a1b      	ldr	r2, [pc, #108]	; (8000a80 <EXTI9_5_IRQHandler+0x200>)
 8000a14:	9909      	ldr	r1, [sp, #36]	; 0x24

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000a16:	4d1b      	ldr	r5, [pc, #108]	; (8000a84 <EXTI9_5_IRQHandler+0x204>)


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000a18:	f204 63d6 	addw	r3, r4, #1750	; 0x6d6

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000a1c:	eef8 7ae7 	vcvt.f32.s32	s15, s15


    for (i = 0; i < 8; i++) {
        if (BIOEXG_SETTINGS & 1 << i * 4) {
            // Add to uint data buffer
            data_Signal_Integer[i][Data_Count] = REALS[i];
 8000a20:	f842 1023 	str.w	r1, [r2, r3, lsl #2]

            // Converting Integer to Scaled Floating Point for Impedance checking
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
 8000a24:	ee17 0a90 	vmov	r0, s15
 8000a28:	eb05 0583 	add.w	r5, r5, r3, lsl #2
 8000a2c:	f001 fa2e 	bl	8001e8c <__aeabi_f2d>
 8000a30:	a30f      	add	r3, pc, #60	; (adr r3, 8000a70 <EXTI9_5_IRQHandler+0x1f0>)
 8000a32:	e9d3 2300 	ldrd	r2, r3, [r3]
 8000a36:	f001 fa7d 	bl	8001f34 <__aeabi_dmul>
 8000a3a:	f001 fc8d 	bl	8002358 <__aeabi_d2f>
 8000a3e:	6028      	str	r0, [r5, #0]
        }
    }

    Data_Count++;
 8000a40:	3401      	adds	r4, #1

    if (Data_Count == ADS1299_SIGNAL_WINDOW) {
 8000a42:	2cfa      	cmp	r4, #250	; 0xfa
 8000a44:	d00c      	beq.n	8000a60 <EXTI9_5_IRQHandler+0x1e0>
            // Range/(2^23 - 1) = 2.2351744...E-5
            data_Signal_Float[i][Data_Count] = ((float32_t)REALS[i]) * 2.235174445E-5;
        }
    }

    Data_Count++;
 8000a46:	6034      	str	r4, [r6, #0]
        Data_Count = 0;
        NVIC_SetPendingIRQ(EXTI0_IRQn);
    }

    // Forgot this:
    EXTI->PR |= EXTI_PR_PR6;
 8000a48:	4a0f      	ldr	r2, [pc, #60]	; (8000a88 <EXTI9_5_IRQHandler+0x208>)

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
 8000a4a:	4910      	ldr	r1, [pc, #64]	; (8000a8c <EXTI9_5_IRQHandler+0x20c>)
 8000a4c:	6953      	ldr	r3, [r2, #20]
 8000a4e:	f44f 0000 	mov.w	r0, #8388608	; 0x800000
 8000a52:	f043 0340 	orr.w	r3, r3, #64	; 0x40
 8000a56:	6153      	str	r3, [r2, #20]
 8000a58:	f8c1 0180 	str.w	r0, [r1, #384]	; 0x180
    // Clear pending bit
    NVIC_ClearPendingIRQ(EXTI9_5_IRQn);
}
 8000a5c:	b00b      	add	sp, #44	; 0x2c
 8000a5e:	bdf0      	pop	{r4, r5, r6, r7, pc}

    \param [in]      IRQn  Interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_SetPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ISPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* set interrupt pending */
 8000a60:	4b0a      	ldr	r3, [pc, #40]	; (8000a8c <EXTI9_5_IRQHandler+0x20c>)
    }

    Data_Count++;

    if (Data_Count == ADS1299_SIGNAL_WINDOW) {
        Data_Count = 0;
 8000a62:	2100      	movs	r1, #0
 8000a64:	2240      	movs	r2, #64	; 0x40
 8000a66:	6031      	str	r1, [r6, #0]
 8000a68:	f8c3 2100 	str.w	r2, [r3, #256]	; 0x100
 8000a6c:	e7ec      	b.n	8000a48 <EXTI9_5_IRQHandler+0x1c8>
 8000a6e:	bf00      	nop
 8000a70:	2ec819be 	.word	0x2ec819be
 8000a74:	3ef77000 	.word	0x3ef77000
 8000a78:	200004b4 	.word	0x200004b4
 8000a7c:	2000090c 	.word	0x2000090c
 8000a80:	200047f0 	.word	0x200047f0
 8000a84:	20000970 	.word	0x20000970
 8000a88:	40013c00 	.word	0x40013c00
 8000a8c:	e000e100 	.word	0xe000e100

08000a90 <EXTI0_IRQHandler>:
    // Clear pending bit
    NVIC_ClearPendingIRQ(EXTI9_5_IRQn);
}

void EXTI0_IRQHandler()
{
 8000a90:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8000a94:	ed2d 8b04 	vpush	{d8-d9}
    // LED Indicator - Orange for IRQ calls, Blue for Data processing duration
    GPIOE->ODR ^= GPIO_Pin_10;
 8000a98:	4bc5      	ldr	r3, [pc, #788]	; (8000db0 <EXTI0_IRQHandler+0x320>)
 8000a9a:	49c6      	ldr	r1, [pc, #792]	; (8000db4 <EXTI0_IRQHandler+0x324>)
 8000a9c:	695a      	ldr	r2, [r3, #20]
 8000a9e:	4cc6      	ldr	r4, [pc, #792]	; (8000db8 <EXTI0_IRQHandler+0x328>)
 8000aa0:	f8df 8348 	ldr.w	r8, [pc, #840]	; 8000dec <EXTI0_IRQHandler+0x35c>
 8000aa4:	f8df a348 	ldr.w	sl, [pc, #840]	; 8000df0 <EXTI0_IRQHandler+0x360>
 8000aa8:	f8df b348 	ldr.w	fp, [pc, #840]	; 8000df4 <EXTI0_IRQHandler+0x364>
 8000aac:	f8df 9348 	ldr.w	r9, [pc, #840]	; 8000df8 <EXTI0_IRQHandler+0x368>
 8000ab0:	4dc2      	ldr	r5, [pc, #776]	; (8000dbc <EXTI0_IRQHandler+0x32c>)
            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) output_IIR_IMPEDANCE[i][j] *= biquad_HP_Output_Gain;

            arm_biquad_cascade_df2T_f32(&biquad_BP_Struct[i], output_IIR_IMPEDANCE[i], output_IIR[i], ADS1299_SIGNAL_WINDOW);
            // Mutiply biquad SOS output by output coeff, also compute amplitude

            sum_Impedance[i] = 0;
 8000ab2:	ed9f 8ac3 	vldr	s16, [pc, #780]	; 8000dc0 <EXTI0_IRQHandler+0x330>
                output_IIR[i][j] *= biquad_BP_Output_Gain;
                sum_Impedance[i] += output_IIR[i][j] * output_IIR[i][j];
            }

            // Calculate Amplitude
            sum_Impedance[i] = (float)sum_Impedance[i] / (float)ADS1299_SIGNAL_WINDOW;
 8000ab6:	eddf 8ac3 	vldr	s17, [pc, #780]	; 8000dc4 <EXTI0_IRQHandler+0x334>
 8000aba:	ed9f 9ac3 	vldr	s18, [pc, #780]	; 8000dc8 <EXTI0_IRQHandler+0x338>
    // Clear pending bit
    NVIC_ClearPendingIRQ(EXTI9_5_IRQn);
}

void EXTI0_IRQHandler()
{
 8000abe:	b083      	sub	sp, #12
    // LED Indicator - Orange for IRQ calls, Blue for Data processing duration
    GPIOE->ODR ^= GPIO_Pin_10;
 8000ac0:	f482 6280 	eor.w	r2, r2, #1024	; 0x400
 8000ac4:	9100      	str	r1, [sp, #0]
 8000ac6:	49c1      	ldr	r1, [pc, #772]	; (8000dcc <EXTI0_IRQHandler+0x33c>)
 8000ac8:	9101      	str	r1, [sp, #4]
 8000aca:	615a      	str	r2, [r3, #20]
    GPIOE->ODR |= GPIO_Pin_7;
 8000acc:	695a      	ldr	r2, [r3, #20]
 8000ace:	f042 0280 	orr.w	r2, r2, #128	; 0x80
 8000ad2:	615a      	str	r2, [r3, #20]
    GPIOE->ODR &= ~GPIO_Pin_8;
 8000ad4:	695a      	ldr	r2, [r3, #20]
 8000ad6:	f422 7280 	bic.w	r2, r2, #256	; 0x100
 8000ada:	615a      	str	r2, [r3, #20]
 8000adc:	2701      	movs	r7, #1
 8000ade:	e01a      	b.n	8000b16 <EXTI0_IRQHandler+0x86>
 8000ae0:	4620      	mov	r0, r4
 8000ae2:	4641      	mov	r1, r8
 8000ae4:	f44f 727a 	mov.w	r2, #1000	; 0x3e8
 8000ae8:	f001 fc86 	bl	80023f8 <memcpy>
 8000aec:	f509 767a 	add.w	r6, r9, #1000	; 0x3e8
 8000af0:	f504 797a 	add.w	r9, r4, #1000	; 0x3e8
 8000af4:	9b00      	ldr	r3, [sp, #0]
 8000af6:	330c      	adds	r3, #12
 8000af8:	9300      	str	r3, [sp, #0]
 8000afa:	9b01      	ldr	r3, [sp, #4]
 8000afc:	3704      	adds	r7, #4
 8000afe:	330c      	adds	r3, #12
        }
    }
    CTX('\n'); // END LINE
    }*/

    for (i = 0; i < 8; i++) {
 8000b00:	2f21      	cmp	r7, #33	; 0x21
 8000b02:	464c      	mov	r4, r9
 8000b04:	f508 787a 	add.w	r8, r8, #1000	; 0x3e8
 8000b08:	f10a 0a04 	add.w	sl, sl, #4
 8000b0c:	f10b 0b04 	add.w	fp, fp, #4
 8000b10:	46b1      	mov	r9, r6
 8000b12:	9301      	str	r3, [sp, #4]
 8000b14:	d055      	beq.n	8000bc2 <EXTI0_IRQHandler+0x132>
        if (BIOEXG_SETTINGS & 1 << (i * 4 + 1)) {
 8000b16:	2301      	movs	r3, #1
 8000b18:	682a      	ldr	r2, [r5, #0]
 8000b1a:	40bb      	lsls	r3, r7
 8000b1c:	4213      	tst	r3, r2
 8000b1e:	d0df      	beq.n	8000ae0 <EXTI0_IRQHandler+0x50>
            * IIR DC REMOVAL HP FILTER *
            ***************************/
            /************************************
            * IIR IMPEDENCE WAVEFORM EXTRACTION *
            *************************************/
            arm_biquad_cascade_df2T_f32(&biquad_HP_Struct[i], data_Signal_Float[i], output_IIR_IMPEDANCE[i], ADS1299_SIGNAL_WINDOW);
 8000b20:	23fa      	movs	r3, #250	; 0xfa
 8000b22:	9801      	ldr	r0, [sp, #4]
 8000b24:	4641      	mov	r1, r8
 8000b26:	464a      	mov	r2, r9
 8000b28:	f000 ff5c 	bl	80019e4 <arm_biquad_cascade_df2T_f32>
 8000b2c:	4ba8      	ldr	r3, [pc, #672]	; (8000dd0 <EXTI0_IRQHandler+0x340>)
 8000b2e:	f509 767a 	add.w	r6, r9, #1000	; 0x3e8
 8000b32:	ed93 7a00 	vldr	s14, [r3]
 8000b36:	464b      	mov	r3, r9
            // Mutiply biquad SOS output by output coeff
            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) output_IIR_IMPEDANCE[i][j] *= biquad_HP_Output_Gain;
 8000b38:	edd3 7a00 	vldr	s15, [r3]
 8000b3c:	ee67 7a87 	vmul.f32	s15, s15, s14
 8000b40:	ece3 7a01 	vstmia	r3!, {s15}
 8000b44:	42b3      	cmp	r3, r6
 8000b46:	d1f7      	bne.n	8000b38 <EXTI0_IRQHandler+0xa8>

            arm_biquad_cascade_df2T_f32(&biquad_BP_Struct[i], output_IIR_IMPEDANCE[i], output_IIR[i], ADS1299_SIGNAL_WINDOW);
 8000b48:	4649      	mov	r1, r9
 8000b4a:	23fa      	movs	r3, #250	; 0xfa
 8000b4c:	9800      	ldr	r0, [sp, #0]
 8000b4e:	4622      	mov	r2, r4
 8000b50:	f000 ff48 	bl	80019e4 <arm_biquad_cascade_df2T_f32>
 8000b54:	4b9f      	ldr	r3, [pc, #636]	; (8000dd4 <EXTI0_IRQHandler+0x344>)
            // Mutiply biquad SOS output by output coeff, also compute amplitude

            sum_Impedance[i] = 0;
            rms_Impedance[i] = 0;
 8000b56:	ed9f 7a9a 	vldr	s14, [pc, #616]	; 8000dc0 <EXTI0_IRQHandler+0x330>
 8000b5a:	ed93 6a00 	vldr	s12, [r3]
            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) output_IIR_IMPEDANCE[i][j] *= biquad_HP_Output_Gain;

            arm_biquad_cascade_df2T_f32(&biquad_BP_Struct[i], output_IIR_IMPEDANCE[i], output_IIR[i], ADS1299_SIGNAL_WINDOW);
            // Mutiply biquad SOS output by output coeff, also compute amplitude

            sum_Impedance[i] = 0;
 8000b5e:	ed8b 8a00 	vstr	s16, [fp]
            rms_Impedance[i] = 0;
 8000b62:	ed8a 8a00 	vstr	s16, [sl]
 8000b66:	f504 797a 	add.w	r9, r4, #1000	; 0x3e8

            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
                output_IIR[i][j] *= biquad_BP_Output_Gain;
 8000b6a:	edd4 7a00 	vldr	s15, [r4]
 8000b6e:	ee66 7a27 	vmul.f32	s15, s12, s15
                sum_Impedance[i] += output_IIR[i][j] * output_IIR[i][j];
 8000b72:	ee67 6aa7 	vmul.f32	s13, s15, s15

            sum_Impedance[i] = 0;
            rms_Impedance[i] = 0;

            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
                output_IIR[i][j] *= biquad_BP_Output_Gain;
 8000b76:	ece4 7a01 	vstmia	r4!, {s15}
            // Mutiply biquad SOS output by output coeff, also compute amplitude

            sum_Impedance[i] = 0;
            rms_Impedance[i] = 0;

            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
 8000b7a:	454c      	cmp	r4, r9
                output_IIR[i][j] *= biquad_BP_Output_Gain;
                sum_Impedance[i] += output_IIR[i][j] * output_IIR[i][j];
 8000b7c:	ee37 7a26 	vadd.f32	s14, s14, s13
            // Mutiply biquad SOS output by output coeff, also compute amplitude

            sum_Impedance[i] = 0;
            rms_Impedance[i] = 0;

            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
 8000b80:	d1f3      	bne.n	8000b6a <EXTI0_IRQHandler+0xda>
                output_IIR[i][j] *= biquad_BP_Output_Gain;
                sum_Impedance[i] += output_IIR[i][j] * output_IIR[i][j];
            }

            // Calculate Amplitude
            sum_Impedance[i] = (float)sum_Impedance[i] / (float)ADS1299_SIGNAL_WINDOW;
 8000b82:	ee87 7a28 	vdiv.f32	s14, s14, s17

  static __INLINE arm_status arm_sqrt_f32(
  float32_t in,
  float32_t * pOut)
  {
    if(in > 0)
 8000b86:	eeb5 7ac0 	vcmpe.f32	s14, #0.0
 8000b8a:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8000b8e:	ed8b 7a00 	vstr	s14, [fp]
 8000b92:	dc05      	bgt.n	8000ba0 <EXTI0_IRQHandler+0x110>
 8000b94:	2200      	movs	r2, #0
 8000b96:	4b90      	ldr	r3, [pc, #576]	; (8000dd8 <EXTI0_IRQHandler+0x348>)
 8000b98:	443b      	add	r3, r7
            arm_sqrt_f32(sum_Impedance[i], &rms_Impedance[i]);
            rms_Impedance_Integer[i] = rms_Impedance[i] * 10000.0f;
 8000b9a:	f843 2c01 	str.w	r2, [r3, #-1]
 8000b9e:	e7a9      	b.n	8000af4 <EXTI0_IRQHandler+0x64>

//      #if __FPU_USED
#if (__FPU_USED == 1) && defined ( __CC_ARM   )
      *pOut = __sqrtf(in);
#else
      *pOut = sqrtf(in);
 8000ba0:	eeb1 0ac7 	vsqrt.f32	s0, s14
 8000ba4:	eeb4 0a40 	vcmp.f32	s0, s0
 8000ba8:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 8000bac:	f040 80fb 	bne.w	8000da6 <EXTI0_IRQHandler+0x316>
 8000bb0:	ee60 7a09 	vmul.f32	s15, s0, s18
 8000bb4:	ed8a 0a00 	vstr	s0, [sl]
 8000bb8:	eefd 7ae7 	vcvt.s32.f32	s15, s15
 8000bbc:	ee17 2a90 	vmov	r2, s15
 8000bc0:	e7e9      	b.n	8000b96 <EXTI0_IRQHandler+0x106>
            for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) ;
        }
    }

    // Turn off Blue LED
    GPIOE->ODR &= ~GPIO_Pin_7;
 8000bc2:	4a7b      	ldr	r2, [pc, #492]	; (8000db0 <EXTI0_IRQHandler+0x320>)
 8000bc4:	f8df 8234 	ldr.w	r8, [pc, #564]	; 8000dfc <EXTI0_IRQHandler+0x36c>
 8000bc8:	6953      	ldr	r3, [r2, #20]
 8000bca:	4f84      	ldr	r7, [pc, #528]	; (8000ddc <EXTI0_IRQHandler+0x34c>)
 8000bcc:	f5a8 767a 	sub.w	r6, r8, #1000	; 0x3e8
 8000bd0:	f023 0380 	bic.w	r3, r3, #128	; 0x80
 8000bd4:	f5a6 797a 	sub.w	r9, r6, #1000	; 0x3e8
 8000bd8:	6153      	str	r3, [r2, #20]
 8000bda:	46c2      	mov	sl, r8
 8000bdc:	f507 747a 	add.w	r4, r7, #1000	; 0x3e8
 8000be0:	e01c      	b.n	8000c1c <EXTI0_IRQHandler+0x18c>
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000be2:	06d8      	lsls	r0, r3, #27
 8000be4:	d42d      	bmi.n	8000c42 <EXTI0_IRQHandler+0x1b2>
 8000be6:	05d9      	lsls	r1, r3, #23
 8000be8:	d439      	bmi.n	8000c5e <EXTI0_IRQHandler+0x1ce>
 8000bea:	04da      	lsls	r2, r3, #19
 8000bec:	d445      	bmi.n	8000c7a <EXTI0_IRQHandler+0x1ea>
 8000bee:	03d8      	lsls	r0, r3, #15
 8000bf0:	d451      	bmi.n	8000c96 <EXTI0_IRQHandler+0x206>
 8000bf2:	02d9      	lsls	r1, r3, #11
 8000bf4:	d45d      	bmi.n	8000cb2 <EXTI0_IRQHandler+0x222>
 8000bf6:	01da      	lsls	r2, r3, #7
 8000bf8:	d469      	bmi.n	8000cce <EXTI0_IRQHandler+0x23e>
 8000bfa:	00db      	lsls	r3, r3, #3
 8000bfc:	d474      	bmi.n	8000ce8 <EXTI0_IRQHandler+0x258>
 8000bfe:	3604      	adds	r6, #4
                //CTX(' '); // SEND DATA TYPE
                //PREG(*(uint32_t*)&rms_Impedance[i]);
                //CTX(' '); // SEND DATA TYPE
            }
        }
        CTX('\n'); // SEND DATA TYPE
 8000c00:	200a      	movs	r0, #10
 8000c02:	f7ff fb29 	bl	8000258 <CTX>

    /*
     *     FOR DEBUG PURPOSES: FILTER EVALUATION
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
 8000c06:	4556      	cmp	r6, sl
 8000c08:	f108 0804 	add.w	r8, r8, #4
 8000c0c:	f107 0704 	add.w	r7, r7, #4
 8000c10:	f109 0904 	add.w	r9, r9, #4
 8000c14:	f104 0404 	add.w	r4, r4, #4
 8000c18:	f000 80b4 	beq.w	8000d84 <EXTI0_IRQHandler+0x2f4>
        CTX('D'); // SEND DATA TYPE
 8000c1c:	2044      	movs	r0, #68	; 0x44
 8000c1e:	f7ff fb1b 	bl	8000258 <CTX>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000c22:	682b      	ldr	r3, [r5, #0]
 8000c24:	07da      	lsls	r2, r3, #31
 8000c26:	d5dc      	bpl.n	8000be2 <EXTI0_IRQHandler+0x152>
                check_RN42_RTS();
 8000c28:	f000 f998 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000c2c:	6838      	ldr	r0, [r7, #0]
 8000c2e:	2800      	cmp	r0, #0
 8000c30:	db66      	blt.n	8000d00 <EXTI0_IRQHandler+0x270>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000c32:	f7ff fb4b 	bl	80002cc <PDEC>
                CTX(' ');
 8000c36:	2020      	movs	r0, #32
 8000c38:	f7ff fb0e 	bl	8000258 <CTX>
 8000c3c:	682b      	ldr	r3, [r5, #0]
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000c3e:	06d8      	lsls	r0, r3, #27
 8000c40:	d5d1      	bpl.n	8000be6 <EXTI0_IRQHandler+0x156>
                check_RN42_RTS();
 8000c42:	f000 f98b 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000c46:	6820      	ldr	r0, [r4, #0]
 8000c48:	2800      	cmp	r0, #0
 8000c4a:	f2c0 8094 	blt.w	8000d76 <EXTI0_IRQHandler+0x2e6>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000c4e:	f7ff fb3d 	bl	80002cc <PDEC>
                CTX(' ');
 8000c52:	2020      	movs	r0, #32
 8000c54:	f7ff fb00 	bl	8000258 <CTX>
 8000c58:	682b      	ldr	r3, [r5, #0]
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000c5a:	05d9      	lsls	r1, r3, #23
 8000c5c:	d5c5      	bpl.n	8000bea <EXTI0_IRQHandler+0x15a>
                check_RN42_RTS();
 8000c5e:	f000 f97d 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000c62:	f8d4 03e8 	ldr.w	r0, [r4, #1000]	; 0x3e8
 8000c66:	2800      	cmp	r0, #0
 8000c68:	db7c      	blt.n	8000d64 <EXTI0_IRQHandler+0x2d4>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000c6a:	f7ff fb2f 	bl	80002cc <PDEC>
                CTX(' ');
 8000c6e:	2020      	movs	r0, #32
 8000c70:	f7ff faf2 	bl	8000258 <CTX>
 8000c74:	682b      	ldr	r3, [r5, #0]
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000c76:	04da      	lsls	r2, r3, #19
 8000c78:	d5b9      	bpl.n	8000bee <EXTI0_IRQHandler+0x15e>
                check_RN42_RTS();
 8000c7a:	f000 f96f 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000c7e:	f8d4 07d0 	ldr.w	r0, [r4, #2000]	; 0x7d0
 8000c82:	2800      	cmp	r0, #0
 8000c84:	db5c      	blt.n	8000d40 <EXTI0_IRQHandler+0x2b0>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000c86:	f7ff fb21 	bl	80002cc <PDEC>
                CTX(' ');
 8000c8a:	2020      	movs	r0, #32
 8000c8c:	f7ff fae4 	bl	8000258 <CTX>
 8000c90:	682b      	ldr	r3, [r5, #0]
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000c92:	03d8      	lsls	r0, r3, #15
 8000c94:	d5ad      	bpl.n	8000bf2 <EXTI0_IRQHandler+0x162>
                check_RN42_RTS();
 8000c96:	f000 f961 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000c9a:	f8d4 0bb8 	ldr.w	r0, [r4, #3000]	; 0xbb8
 8000c9e:	2800      	cmp	r0, #0
 8000ca0:	db57      	blt.n	8000d52 <EXTI0_IRQHandler+0x2c2>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000ca2:	f7ff fb13 	bl	80002cc <PDEC>
                CTX(' ');
 8000ca6:	2020      	movs	r0, #32
 8000ca8:	f7ff fad6 	bl	8000258 <CTX>
 8000cac:	682b      	ldr	r3, [r5, #0]
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000cae:	02d9      	lsls	r1, r3, #11
 8000cb0:	d5a1      	bpl.n	8000bf6 <EXTI0_IRQHandler+0x166>
                check_RN42_RTS();
 8000cb2:	f000 f953 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000cb6:	f8d9 0000 	ldr.w	r0, [r9]
 8000cba:	2800      	cmp	r0, #0
 8000cbc:	db30      	blt.n	8000d20 <EXTI0_IRQHandler+0x290>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000cbe:	f7ff fb05 	bl	80002cc <PDEC>
                CTX(' ');
 8000cc2:	2020      	movs	r0, #32
 8000cc4:	f7ff fac8 	bl	8000258 <CTX>
 8000cc8:	682b      	ldr	r3, [r5, #0]
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000cca:	01da      	lsls	r2, r3, #7
 8000ccc:	d595      	bpl.n	8000bfa <EXTI0_IRQHandler+0x16a>
                check_RN42_RTS();
 8000cce:	f000 f945 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000cd2:	6830      	ldr	r0, [r6, #0]
 8000cd4:	2800      	cmp	r0, #0
 8000cd6:	db2c      	blt.n	8000d32 <EXTI0_IRQHandler+0x2a2>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000cd8:	f7ff faf8 	bl	80002cc <PDEC>
                CTX(' ');
 8000cdc:	2020      	movs	r0, #32
 8000cde:	f7ff fabb 	bl	8000258 <CTX>
 8000ce2:	682b      	ldr	r3, [r5, #0]
    */

    for (j = 0; j < ADS1299_SIGNAL_WINDOW; j++) {
        CTX('D'); // SEND DATA TYPE
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
 8000ce4:	00db      	lsls	r3, r3, #3
 8000ce6:	d58a      	bpl.n	8000bfe <EXTI0_IRQHandler+0x16e>
                check_RN42_RTS();
 8000ce8:	f000 f938 	bl	8000f5c <check_RN42_RTS>

                if (data_Signal_Integer[i][j] < 0) {
 8000cec:	f8d8 0000 	ldr.w	r0, [r8]
 8000cf0:	2800      	cmp	r0, #0
 8000cf2:	db0c      	blt.n	8000d0e <EXTI0_IRQHandler+0x27e>
                    CTX('-');
                    data_Signal_Integer[i][j] *= -1;
                }
                PDEC((uint32_t)data_Signal_Integer[i][j]);
 8000cf4:	f7ff faea 	bl	80002cc <PDEC>
                CTX(' ');
 8000cf8:	2020      	movs	r0, #32
 8000cfa:	f7ff faad 	bl	8000258 <CTX>
 8000cfe:	e77e      	b.n	8000bfe <EXTI0_IRQHandler+0x16e>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d00:	202d      	movs	r0, #45	; 0x2d
 8000d02:	f7ff faa9 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d06:	6838      	ldr	r0, [r7, #0]
 8000d08:	4240      	negs	r0, r0
 8000d0a:	6038      	str	r0, [r7, #0]
 8000d0c:	e791      	b.n	8000c32 <EXTI0_IRQHandler+0x1a2>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d0e:	202d      	movs	r0, #45	; 0x2d
 8000d10:	f7ff faa2 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d14:	f8d8 0000 	ldr.w	r0, [r8]
 8000d18:	4240      	negs	r0, r0
 8000d1a:	f8c8 0000 	str.w	r0, [r8]
 8000d1e:	e7e9      	b.n	8000cf4 <EXTI0_IRQHandler+0x264>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d20:	202d      	movs	r0, #45	; 0x2d
 8000d22:	f7ff fa99 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d26:	f8d9 0000 	ldr.w	r0, [r9]
 8000d2a:	4240      	negs	r0, r0
 8000d2c:	f8c9 0000 	str.w	r0, [r9]
 8000d30:	e7c5      	b.n	8000cbe <EXTI0_IRQHandler+0x22e>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d32:	202d      	movs	r0, #45	; 0x2d
 8000d34:	f7ff fa90 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d38:	6830      	ldr	r0, [r6, #0]
 8000d3a:	4240      	negs	r0, r0
 8000d3c:	6030      	str	r0, [r6, #0]
 8000d3e:	e7cb      	b.n	8000cd8 <EXTI0_IRQHandler+0x248>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d40:	202d      	movs	r0, #45	; 0x2d
 8000d42:	f7ff fa89 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d46:	f8d4 07d0 	ldr.w	r0, [r4, #2000]	; 0x7d0
 8000d4a:	4240      	negs	r0, r0
 8000d4c:	f8c4 07d0 	str.w	r0, [r4, #2000]	; 0x7d0
 8000d50:	e799      	b.n	8000c86 <EXTI0_IRQHandler+0x1f6>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d52:	202d      	movs	r0, #45	; 0x2d
 8000d54:	f7ff fa80 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d58:	f8d4 0bb8 	ldr.w	r0, [r4, #3000]	; 0xbb8
 8000d5c:	4240      	negs	r0, r0
 8000d5e:	f8c4 0bb8 	str.w	r0, [r4, #3000]	; 0xbb8
 8000d62:	e79e      	b.n	8000ca2 <EXTI0_IRQHandler+0x212>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d64:	202d      	movs	r0, #45	; 0x2d
 8000d66:	f7ff fa77 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d6a:	f8d4 03e8 	ldr.w	r0, [r4, #1000]	; 0x3e8
 8000d6e:	4240      	negs	r0, r0
 8000d70:	f8c4 03e8 	str.w	r0, [r4, #1000]	; 0x3e8
 8000d74:	e779      	b.n	8000c6a <EXTI0_IRQHandler+0x1da>
        for (i = 0; i < 8; i++) {
            if (BIOEXG_SETTINGS & 1 << i * 4) {
                check_RN42_RTS();

                if (data_Signal_Integer[i][j] < 0) {
                    CTX('-');
 8000d76:	202d      	movs	r0, #45	; 0x2d
 8000d78:	f7ff fa6e 	bl	8000258 <CTX>
                    data_Signal_Integer[i][j] *= -1;
 8000d7c:	6820      	ldr	r0, [r4, #0]
 8000d7e:	4240      	negs	r0, r0
 8000d80:	6020      	str	r0, [r4, #0]
 8000d82:	e764      	b.n	8000c4e <EXTI0_IRQHandler+0x1be>
        }
    }


    // Append semicolon to the text to indicate end of 1 second packet
    STX("; We're good:\n");
 8000d84:	4816      	ldr	r0, [pc, #88]	; (8000de0 <EXTI0_IRQHandler+0x350>)
 8000d86:	f7ff fa85 	bl	8000294 <STX>

    // Forgot this:
    EXTI->PR |= EXTI_PR_PR0;
 8000d8a:	4a16      	ldr	r2, [pc, #88]	; (8000de4 <EXTI0_IRQHandler+0x354>)

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
 8000d8c:	4916      	ldr	r1, [pc, #88]	; (8000de8 <EXTI0_IRQHandler+0x358>)
 8000d8e:	6953      	ldr	r3, [r2, #20]
 8000d90:	2040      	movs	r0, #64	; 0x40
 8000d92:	f043 0301 	orr.w	r3, r3, #1
 8000d96:	6153      	str	r3, [r2, #20]
 8000d98:	f8c1 0180 	str.w	r0, [r1, #384]	; 0x180
    // Clear pending bit
    NVIC_ClearPendingIRQ(EXTI0_IRQn);
}
 8000d9c:	b003      	add	sp, #12
 8000d9e:	ecbd 8b04 	vpop	{d8-d9}
 8000da2:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 8000da6:	eeb0 0a47 	vmov.f32	s0, s14
 8000daa:	f001 fc5f 	bl	800266c <sqrtf>
 8000dae:	e6ff      	b.n	8000bb0 <EXTI0_IRQHandler+0x120>
 8000db0:	40021000 	.word	0x40021000
 8000db4:	2000905c 	.word	0x2000905c
 8000db8:	20006730 	.word	0x20006730
 8000dbc:	200004b4 	.word	0x200004b4
 8000dc0:	00000000 	.word	0x00000000
 8000dc4:	437a0000 	.word	0x437a0000
 8000dc8:	461c4000 	.word	0x461c4000
 8000dcc:	20008c10 	.word	0x20008c10
 8000dd0:	08002b00 	.word	0x08002b00
 8000dd4:	08002b04 	.word	0x08002b04
 8000dd8:	20000930 	.word	0x20000930
 8000ddc:	200047f0 	.word	0x200047f0
 8000de0:	08037c04 	.word	0x08037c04
 8000de4:	40013c00 	.word	0x40013c00
 8000de8:	e000e100 	.word	0xe000e100
 8000dec:	20000970 	.word	0x20000970
 8000df0:	20000910 	.word	0x20000910
 8000df4:	20000950 	.word	0x20000950
 8000df8:	200028b0 	.word	0x200028b0
 8000dfc:	20006348 	.word	0x20006348

08000e00 <USART6_IRQHandler>:

void USART6_IRQHandler()
{
 8000e00:	b538      	push	{r3, r4, r5, lr}
    if (CRX() == 'S') {
 8000e02:	f7ff fa35 	bl	8000270 <CRX>
 8000e06:	2853      	cmp	r0, #83	; 0x53
 8000e08:	d004      	beq.n	8000e14 <USART6_IRQHandler+0x14>
 8000e0a:	4b15      	ldr	r3, [pc, #84]	; (8000e60 <USART6_IRQHandler+0x60>)
 8000e0c:	2280      	movs	r2, #128	; 0x80
 8000e0e:	f8c3 2188 	str.w	r2, [r3, #392]	; 0x188
 8000e12:	bd38      	pop	{r3, r4, r5, pc}

        // Turn on Amber LED to indicate a Keypress then stop ADS1299 conversion
        GPIOE->BSRRL |= GPIO_Pin_10;
 8000e14:	4d13      	ldr	r5, [pc, #76]	; (8000e64 <USART6_IRQHandler+0x64>)
        GPIOB->BSRRH |= GPIO_Pin_6;  // STOP
 8000e16:	4c14      	ldr	r4, [pc, #80]	; (8000e68 <USART6_IRQHandler+0x68>)
void USART6_IRQHandler()
{
    if (CRX() == 'S') {

        // Turn on Amber LED to indicate a Keypress then stop ADS1299 conversion
        GPIOE->BSRRL |= GPIO_Pin_10;
 8000e18:	8b2b      	ldrh	r3, [r5, #24]
 8000e1a:	b29b      	uxth	r3, r3
 8000e1c:	f443 6380 	orr.w	r3, r3, #1024	; 0x400
 8000e20:	832b      	strh	r3, [r5, #24]
        GPIOB->BSRRH |= GPIO_Pin_6;  // STOP
 8000e22:	8b63      	ldrh	r3, [r4, #26]
 8000e24:	b29b      	uxth	r3, r3
 8000e26:	f043 0340 	orr.w	r3, r3, #64	; 0x40
 8000e2a:	8363      	strh	r3, [r4, #26]

        // If S was pressed, then enter Settings mode
        setting_mode();
 8000e2c:	f000 f8fa 	bl	8001024 <setting_mode>

        // Turn off Amber LED then restart conversion
        GPIOE->BSRRH |= GPIO_Pin_10;
 8000e30:	8b6b      	ldrh	r3, [r5, #26]
 8000e32:	4a0b      	ldr	r2, [pc, #44]	; (8000e60 <USART6_IRQHandler+0x60>)
        GPIOB->BSRRL |= GPIO_Pin_6;  // START

        // Reset EVERYTHING
        Data_Count = 0;
 8000e34:	480d      	ldr	r0, [pc, #52]	; (8000e6c <USART6_IRQHandler+0x6c>)

        // If S was pressed, then enter Settings mode
        setting_mode();

        // Turn off Amber LED then restart conversion
        GPIOE->BSRRH |= GPIO_Pin_10;
 8000e36:	b29b      	uxth	r3, r3
 8000e38:	f443 6380 	orr.w	r3, r3, #1024	; 0x400
 8000e3c:	836b      	strh	r3, [r5, #26]
        GPIOB->BSRRL |= GPIO_Pin_6;  // START
 8000e3e:	8b23      	ldrh	r3, [r4, #24]
 8000e40:	b29b      	uxth	r3, r3
 8000e42:	f043 0340 	orr.w	r3, r3, #64	; 0x40
 8000e46:	8323      	strh	r3, [r4, #24]
 8000e48:	2140      	movs	r1, #64	; 0x40

        // Reset EVERYTHING
        Data_Count = 0;
 8000e4a:	2500      	movs	r5, #0
 8000e4c:	4b04      	ldr	r3, [pc, #16]	; (8000e60 <USART6_IRQHandler+0x60>)
 8000e4e:	6005      	str	r5, [r0, #0]
 8000e50:	f8c2 1180 	str.w	r1, [r2, #384]	; 0x180

    \param [in]      IRQn  Interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_SetPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ISPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* set interrupt pending */
 8000e54:	f8c2 1100 	str.w	r1, [r2, #256]	; 0x100

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
 8000e58:	2280      	movs	r2, #128	; 0x80
 8000e5a:	f8c3 2188 	str.w	r2, [r3, #392]	; 0x188
 8000e5e:	bd38      	pop	{r3, r4, r5, pc}
 8000e60:	e000e100 	.word	0xe000e100
 8000e64:	40021000 	.word	0x40021000
 8000e68:	40020400 	.word	0x40020400
 8000e6c:	2000090c 	.word	0x2000090c

08000e70 <init_GPIO>:
#include "main.h"
#define  AFR_Pin(x) x * 4

void init_GPIO()
{
 8000e70:	b430      	push	{r4, r5}
    // Enable buses GPIOB, GPIOC, GPIOD and GPIOE
    RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN);
 8000e72:	4d23      	ldr	r5, [pc, #140]	; (8000f00 <init_GPIO+0x90>)

    // GPIOB
    // Set Alternate Function mode for ADS1299 SPI's - PB3, PB4, PB5; PB6 and PB7 as GPIO Output
    GPIOB->MODER |= (GPIO_MODER_MODER3_1 | GPIO_MODER_MODER4_1 |
 8000e74:	4b23      	ldr	r3, [pc, #140]	; (8000f04 <init_GPIO+0x94>)
#define  AFR_Pin(x) x * 4

void init_GPIO()
{
    // Enable buses GPIOB, GPIOC, GPIOD and GPIOE
    RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN);
 8000e76:	6b28      	ldr	r0, [r5, #48]	; 0x30
    GPIOB->AFR[0] |= (GPIO_AF_SPI1 << AFR_Pin(3) | GPIO_AF_SPI1 << AFR_Pin(4) |
                      GPIO_AF_SPI1 << AFR_Pin(5));

    // GPIOC
    // Set Alternate Function mode for RN42 USART's - PC6, PC7
    GPIOC->MODER   |= (GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1);
 8000e78:	4923      	ldr	r1, [pc, #140]	; (8000f08 <init_GPIO+0x98>)
    // Set USART6 for PC6, PC7
    GPIOC->AFR[0]  |= (GPIO_AF_USART6 << AFR_Pin(6) | GPIO_AF_USART6 << AFR_Pin(7));

    // GPIOD
    // Set Output mode for CS, CTS, and Input mode for RTS - PD7, PD14, PD15
    GPIOD->MODER   |= (GPIO_MODER_MODER7_0 | GPIO_MODER_MODER14_0);
 8000e7a:	4a24      	ldr	r2, [pc, #144]	; (8000f0c <init_GPIO+0x9c>)
    GPIOD->BSRRL   |= GPIO_Pin_7;
    GPIOD->BSRRH   |= GPIO_Pin_14;

    // GPIOE
    // Set Output mode for the LED's - PE7, PE8, PE10
    GPIOE->MODER |= (GPIO_MODER_MODER7_0 | GPIO_MODER_MODER8_0 | GPIO_MODER_MODER10_0);
 8000e7c:	4c24      	ldr	r4, [pc, #144]	; (8000f10 <init_GPIO+0xa0>)
#define  AFR_Pin(x) x * 4

void init_GPIO()
{
    // Enable buses GPIOB, GPIOC, GPIOD and GPIOE
    RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN);
 8000e7e:	f040 001e 	orr.w	r0, r0, #30
 8000e82:	6328      	str	r0, [r5, #48]	; 0x30

    // GPIOB
    // Set Alternate Function mode for ADS1299 SPI's - PB3, PB4, PB5; PB6 and PB7 as GPIO Output
    GPIOB->MODER |= (GPIO_MODER_MODER3_1 | GPIO_MODER_MODER4_1 |
 8000e84:	6818      	ldr	r0, [r3, #0]
 8000e86:	f440 40b5 	orr.w	r0, r0, #23168	; 0x5a80
 8000e8a:	6018      	str	r0, [r3, #0]
                     GPIO_MODER_MODER5_1 | GPIO_MODER_MODER6_0 | GPIO_MODER_MODER7_0);
    // Set fast speed for PB3, PB4, PB5
    GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR3_1 | GPIO_OSPEEDER_OSPEEDR4_1 |
 8000e8c:	6898      	ldr	r0, [r3, #8]
 8000e8e:	f440 400a 	orr.w	r0, r0, #35328	; 0x8a00
 8000e92:	f040 0080 	orr.w	r0, r0, #128	; 0x80
                       GPIO_OSPEEDER_OSPEEDR5_1 | GPIO_OSPEEDER_OSPEEDR7_1);
    // WTF PB4 IS PULLDOWN
    GPIOB->PUPDR = 0x0;
 8000e96:	2500      	movs	r5, #0
    // GPIOB
    // Set Alternate Function mode for ADS1299 SPI's - PB3, PB4, PB5; PB6 and PB7 as GPIO Output
    GPIOB->MODER |= (GPIO_MODER_MODER3_1 | GPIO_MODER_MODER4_1 |
                     GPIO_MODER_MODER5_1 | GPIO_MODER_MODER6_0 | GPIO_MODER_MODER7_0);
    // Set fast speed for PB3, PB4, PB5
    GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR3_1 | GPIO_OSPEEDER_OSPEEDR4_1 |
 8000e98:	6098      	str	r0, [r3, #8]
                       GPIO_OSPEEDER_OSPEEDR5_1 | GPIO_OSPEEDER_OSPEEDR7_1);
    // WTF PB4 IS PULLDOWN
    GPIOB->PUPDR = 0x0;
 8000e9a:	60dd      	str	r5, [r3, #12]
    // WITH THAT SAID, LETS PULL PB6 DOWN
    GPIOB->BSRRH |= GPIO_Pin_6;
 8000e9c:	8b58      	ldrh	r0, [r3, #26]
 8000e9e:	b280      	uxth	r0, r0
 8000ea0:	f040 0040 	orr.w	r0, r0, #64	; 0x40
 8000ea4:	8358      	strh	r0, [r3, #26]
    // Set SPI1 for PB3, PB4, PB5
    GPIOB->AFR[0] |= (GPIO_AF_SPI1 << AFR_Pin(3) | GPIO_AF_SPI1 << AFR_Pin(4) |
 8000ea6:	6a18      	ldr	r0, [r3, #32]
 8000ea8:	f440 00aa 	orr.w	r0, r0, #5570560	; 0x550000
 8000eac:	f440 40a0 	orr.w	r0, r0, #20480	; 0x5000
 8000eb0:	6218      	str	r0, [r3, #32]
                      GPIO_AF_SPI1 << AFR_Pin(5));

    // GPIOC
    // Set Alternate Function mode for RN42 USART's - PC6, PC7
    GPIOC->MODER   |= (GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1);
 8000eb2:	680b      	ldr	r3, [r1, #0]
 8000eb4:	f443 4320 	orr.w	r3, r3, #40960	; 0xa000
 8000eb8:	600b      	str	r3, [r1, #0]
    // Set fast speed for PC6, PC7
    GPIOC->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR6_1 | GPIO_OSPEEDER_OSPEEDR7_1);
 8000eba:	688b      	ldr	r3, [r1, #8]
 8000ebc:	f443 4320 	orr.w	r3, r3, #40960	; 0xa000
 8000ec0:	608b      	str	r3, [r1, #8]
    // Set USART6 for PC6, PC7
    GPIOC->AFR[0]  |= (GPIO_AF_USART6 << AFR_Pin(6) | GPIO_AF_USART6 << AFR_Pin(7));
 8000ec2:	6a0b      	ldr	r3, [r1, #32]
 8000ec4:	f043 4308 	orr.w	r3, r3, #2281701376	; 0x88000000
 8000ec8:	620b      	str	r3, [r1, #32]

    // GPIOD
    // Set Output mode for CS, CTS, and Input mode for RTS - PD7, PD14, PD15
    GPIOD->MODER   |= (GPIO_MODER_MODER7_0 | GPIO_MODER_MODER14_0);
 8000eca:	6813      	ldr	r3, [r2, #0]
 8000ecc:	f043 5380 	orr.w	r3, r3, #268435456	; 0x10000000
 8000ed0:	f443 4380 	orr.w	r3, r3, #16384	; 0x4000
 8000ed4:	6013      	str	r3, [r2, #0]
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR7_1;
 8000ed6:	6893      	ldr	r3, [r2, #8]
 8000ed8:	f443 4300 	orr.w	r3, r3, #32768	; 0x8000
 8000edc:	6093      	str	r3, [r2, #8]
    GPIOD->BSRRL   |= GPIO_Pin_7;
 8000ede:	8b13      	ldrh	r3, [r2, #24]
 8000ee0:	b29b      	uxth	r3, r3
 8000ee2:	f043 0380 	orr.w	r3, r3, #128	; 0x80
 8000ee6:	8313      	strh	r3, [r2, #24]
    GPIOD->BSRRH   |= GPIO_Pin_14;
 8000ee8:	8b53      	ldrh	r3, [r2, #26]
 8000eea:	b29b      	uxth	r3, r3
 8000eec:	f443 4380 	orr.w	r3, r3, #16384	; 0x4000
 8000ef0:	8353      	strh	r3, [r2, #26]

    // GPIOE
    // Set Output mode for the LED's - PE7, PE8, PE10
    GPIOE->MODER |= (GPIO_MODER_MODER7_0 | GPIO_MODER_MODER8_0 | GPIO_MODER_MODER10_0);
 8000ef2:	6823      	ldr	r3, [r4, #0]
 8000ef4:	f443 138a 	orr.w	r3, r3, #1130496	; 0x114000
 8000ef8:	6023      	str	r3, [r4, #0]
}
 8000efa:	bc30      	pop	{r4, r5}
 8000efc:	4770      	bx	lr
 8000efe:	bf00      	nop
 8000f00:	40023800 	.word	0x40023800
 8000f04:	40020400 	.word	0x40020400
 8000f08:	40020800 	.word	0x40020800
 8000f0c:	40020c00 	.word	0x40020c00
 8000f10:	40021000 	.word	0x40021000

08000f14 <init_USART6>:

void init_USART6()
{
 8000f14:	b470      	push	{r4, r5, r6}
    // Enable USART6 Bus
    RCC->APB2ENR |= RCC_APB2ENR_USART6EN;
 8000f16:	4c0e      	ldr	r4, [pc, #56]	; (8000f50 <init_USART6+0x3c>)
    // Set USART6 Baudrate
    // 115Kbps approx.
    //USART6->BRR = 0x2D9;
    // 922Kbps approx 5.6875 = 5B
    USART6->BRR = 0x5B;
 8000f18:	4b0e      	ldr	r3, [pc, #56]	; (8000f54 <init_USART6+0x40>)
}

void init_USART6()
{
    // Enable USART6 Bus
    RCC->APB2ENR |= RCC_APB2ENR_USART6EN;
 8000f1a:	6c61      	ldr	r1, [r4, #68]	; 0x44
__STATIC_INLINE void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
  if(IRQn < 0) {
    SCB->SHP[((uint32_t)(IRQn) & 0xF)-4] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff); } /* set Priority for Cortex-M  System Interrupts */
  else {
    NVIC->IP[(uint32_t)(IRQn)] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff);    }        /* set Priority for device specific Interrupts  */
 8000f1c:	4a0e      	ldr	r2, [pc, #56]	; (8000f58 <init_USART6+0x44>)
    // Set USART6 Baudrate
    // 115Kbps approx.
    //USART6->BRR = 0x2D9;
    // 922Kbps approx 5.6875 = 5B
    USART6->BRR = 0x5B;
 8000f1e:	265b      	movs	r6, #91	; 0x5b
 8000f20:	2500      	movs	r5, #0

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
 8000f22:	2080      	movs	r0, #128	; 0x80
}

void init_USART6()
{
    // Enable USART6 Bus
    RCC->APB2ENR |= RCC_APB2ENR_USART6EN;
 8000f24:	f041 0120 	orr.w	r1, r1, #32
 8000f28:	6461      	str	r1, [r4, #68]	; 0x44
    // Set USART6 Baudrate
    // 115Kbps approx.
    //USART6->BRR = 0x2D9;
    // 922Kbps approx 5.6875 = 5B
    USART6->BRR = 0x5B;
 8000f2a:	811e      	strh	r6, [r3, #8]
__STATIC_INLINE void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
  if(IRQn < 0) {
    SCB->SHP[((uint32_t)(IRQn) & 0xF)-4] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff); } /* set Priority for Cortex-M  System Interrupts */
  else {
    NVIC->IP[(uint32_t)(IRQn)] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff);    }        /* set Priority for device specific Interrupts  */
 8000f2c:	f882 5347 	strb.w	r5, [r2, #839]	; 0x347

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
 8000f30:	f8c2 0188 	str.w	r0, [r2, #392]	; 0x188
    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_EnableIRQ(IRQn_Type IRQn)
{
/*  NVIC->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  enable interrupt */
  NVIC->ISER[(uint32_t)((int32_t)IRQn) >> 5] = (uint32_t)(1 << ((uint32_t)((int32_t)IRQn) & (uint32_t)0x1F)); /* enable interrupt */
 8000f34:	6090      	str	r0, [r2, #8]
    NVIC_ClearPendingIRQ(USART6_IRQn);
    // Interrupt line is EXTI6 - PD6
    NVIC_EnableIRQ(USART6_IRQn);

    // Enable Transmit & Recieve & Recieve Interrupt then Enable USART6
    USART6->CR1 |= (USART_CR1_TE | USART_CR1_RE | USART_CR1_RXNEIE);
 8000f36:	899a      	ldrh	r2, [r3, #12]
 8000f38:	b292      	uxth	r2, r2
 8000f3a:	f042 022c 	orr.w	r2, r2, #44	; 0x2c
 8000f3e:	819a      	strh	r2, [r3, #12]
    USART6->CR1 |= USART_CR1_UE;
 8000f40:	899a      	ldrh	r2, [r3, #12]
 8000f42:	b292      	uxth	r2, r2
 8000f44:	f442 5200 	orr.w	r2, r2, #8192	; 0x2000
 8000f48:	819a      	strh	r2, [r3, #12]
}
 8000f4a:	bc70      	pop	{r4, r5, r6}
 8000f4c:	4770      	bx	lr
 8000f4e:	bf00      	nop
 8000f50:	40023800 	.word	0x40023800
 8000f54:	40011400 	.word	0x40011400
 8000f58:	e000e100 	.word	0xe000e100

08000f5c <check_RN42_RTS>:
{
    // If the RN42_RTS signal is high (RN42 is too busy)
    // then wait and turn off the green LED
    // when it is no longer high, turn green LED back on

    while (GPIOD->IDR & GPIO_IDR_IDR_15) {
 8000f5c:	4908      	ldr	r1, [pc, #32]	; (8000f80 <check_RN42_RTS+0x24>)
 8000f5e:	690b      	ldr	r3, [r1, #16]
 8000f60:	041a      	lsls	r2, r3, #16
 8000f62:	d507      	bpl.n	8000f74 <check_RN42_RTS+0x18>
        GPIOE->ODR |= GPIO_Pin_8;
 8000f64:	4a07      	ldr	r2, [pc, #28]	; (8000f84 <check_RN42_RTS+0x28>)
 8000f66:	6953      	ldr	r3, [r2, #20]
 8000f68:	f443 7380 	orr.w	r3, r3, #256	; 0x100
 8000f6c:	6153      	str	r3, [r2, #20]
{
    // If the RN42_RTS signal is high (RN42 is too busy)
    // then wait and turn off the green LED
    // when it is no longer high, turn green LED back on

    while (GPIOD->IDR & GPIO_IDR_IDR_15) {
 8000f6e:	690b      	ldr	r3, [r1, #16]
 8000f70:	041b      	lsls	r3, r3, #16
 8000f72:	d4f8      	bmi.n	8000f66 <check_RN42_RTS+0xa>
        GPIOE->ODR |= GPIO_Pin_8;
    }

    GPIOE->ODR &= ~GPIO_Pin_8;
 8000f74:	4a03      	ldr	r2, [pc, #12]	; (8000f84 <check_RN42_RTS+0x28>)
 8000f76:	6953      	ldr	r3, [r2, #20]
 8000f78:	f423 7380 	bic.w	r3, r3, #256	; 0x100
 8000f7c:	6153      	str	r3, [r2, #20]
 8000f7e:	4770      	bx	lr
 8000f80:	40020c00 	.word	0x40020c00
 8000f84:	40021000 	.word	0x40021000

08000f88 <init_SPI1>:
}

void init_SPI1()
{
    // Enable SPI1 Bus
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
 8000f88:	4a04      	ldr	r2, [pc, #16]	; (8000f9c <init_SPI1+0x14>)
    // Configure & Enable SPI1
    // CPHA = 1; CPOL = 0; MASTER; BR = 84Mhz/8 = 10.5Mhz
    SPI1->CR1 = (SPI_CR1_CPHA | SPI_CR1_MSTR | SPI_CR1_BR_1 | SPI_CR1_BR_0 |
 8000f8a:	4905      	ldr	r1, [pc, #20]	; (8000fa0 <init_SPI1+0x18>)
}

void init_SPI1()
{
    // Enable SPI1 Bus
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
 8000f8c:	6c53      	ldr	r3, [r2, #68]	; 0x44
    // Configure & Enable SPI1
    // CPHA = 1; CPOL = 0; MASTER; BR = 84Mhz/8 = 10.5Mhz
    SPI1->CR1 = (SPI_CR1_CPHA | SPI_CR1_MSTR | SPI_CR1_BR_1 | SPI_CR1_BR_0 |
 8000f8e:	f240 305d 	movw	r0, #861	; 0x35d
}

void init_SPI1()
{
    // Enable SPI1 Bus
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
 8000f92:	f443 5380 	orr.w	r3, r3, #4096	; 0x1000
 8000f96:	6453      	str	r3, [r2, #68]	; 0x44
    // Configure & Enable SPI1
    // CPHA = 1; CPOL = 0; MASTER; BR = 84Mhz/8 = 10.5Mhz
    SPI1->CR1 = (SPI_CR1_CPHA | SPI_CR1_MSTR | SPI_CR1_BR_1 | SPI_CR1_BR_0 |
 8000f98:	8008      	strh	r0, [r1, #0]
 8000f9a:	4770      	bx	lr
 8000f9c:	40023800 	.word	0x40023800
 8000fa0:	40013000 	.word	0x40013000

08000fa4 <init_EXTI>:
                 SPI_CR1_SPE | SPI_CR1_SSI  | SPI_CR1_SSM);
}

void init_EXTI()
{
 8000fa4:	b470      	push	{r4, r5, r6}
    // Enable SysCFG
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
 8000fa6:	4d1a      	ldr	r5, [pc, #104]	; (8001010 <init_EXTI+0x6c>)
    // Connect PD6 -> EXTI6
    SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI6_PD;
 8000fa8:	4c1a      	ldr	r4, [pc, #104]	; (8001014 <init_EXTI+0x70>)
}

void init_EXTI()
{
    // Enable SysCFG
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
 8000faa:	6c69      	ldr	r1, [r5, #68]	; 0x44
    // Connect PD6 -> EXTI6
    SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI6_PD;
    // Remove mask on EXTI6 line
    EXTI->IMR |= EXTI_IMR_MR6;
 8000fac:	4a1a      	ldr	r2, [pc, #104]	; (8001018 <init_EXTI+0x74>)
__STATIC_INLINE void NVIC_SetPriorityGrouping(uint32_t PriorityGroup)
{
  uint32_t reg_value;
  uint32_t PriorityGroupTmp = (PriorityGroup & (uint32_t)0x07);               /* only values 0..7 are used          */

  reg_value  =  SCB->AIRCR;                                                   /* read old register configuration    */
 8000fae:	481b      	ldr	r0, [pc, #108]	; (800101c <init_EXTI+0x78>)
__STATIC_INLINE void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
  if(IRQn < 0) {
    SCB->SHP[((uint32_t)(IRQn) & 0xF)-4] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff); } /* set Priority for Cortex-M  System Interrupts */
  else {
    NVIC->IP[(uint32_t)(IRQn)] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff);    }        /* set Priority for device specific Interrupts  */
 8000fb0:	4b1b      	ldr	r3, [pc, #108]	; (8001020 <init_EXTI+0x7c>)
}

void init_EXTI()
{
    // Enable SysCFG
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
 8000fb2:	f441 4180 	orr.w	r1, r1, #16384	; 0x4000
 8000fb6:	6469      	str	r1, [r5, #68]	; 0x44
    // Connect PD6 -> EXTI6
    SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI6_PD;
 8000fb8:	68e1      	ldr	r1, [r4, #12]
 8000fba:	f441 7140 	orr.w	r1, r1, #768	; 0x300
 8000fbe:	60e1      	str	r1, [r4, #12]
    // Remove mask on EXTI6 line
    EXTI->IMR |= EXTI_IMR_MR6;
 8000fc0:	6811      	ldr	r1, [r2, #0]
 8000fc2:	f041 0140 	orr.w	r1, r1, #64	; 0x40
 8000fc6:	6011      	str	r1, [r2, #0]
    // Remove mask on EXTI0 line
    EXTI->IMR |= EXTI_IMR_MR0;
 8000fc8:	6811      	ldr	r1, [r2, #0]
 8000fca:	f041 0101 	orr.w	r1, r1, #1
 8000fce:	6011      	str	r1, [r2, #0]
    // Trigger EXTI6 on Falling Edge
    EXTI->FTSR |= EXTI_FTSR_TR6;
 8000fd0:	68d1      	ldr	r1, [r2, #12]
 8000fd2:	f041 0140 	orr.w	r1, r1, #64	; 0x40
 8000fd6:	60d1      	str	r1, [r2, #12]
__STATIC_INLINE void NVIC_SetPriorityGrouping(uint32_t PriorityGroup)
{
  uint32_t reg_value;
  uint32_t PriorityGroupTmp = (PriorityGroup & (uint32_t)0x07);               /* only values 0..7 are used          */

  reg_value  =  SCB->AIRCR;                                                   /* read old register configuration    */
 8000fd8:	68c1      	ldr	r1, [r0, #12]
  reg_value &= ~(SCB_AIRCR_VECTKEY_Msk | SCB_AIRCR_PRIGROUP_Msk);             /* clear bits to change               */
 8000fda:	f64f 02ff 	movw	r2, #63743	; 0xf8ff
 8000fde:	400a      	ands	r2, r1
  reg_value  =  (reg_value                                 |
 8000fe0:	f042 62bf 	orr.w	r2, r2, #100139008	; 0x5f80000
__STATIC_INLINE void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
  if(IRQn < 0) {
    SCB->SHP[((uint32_t)(IRQn) & 0xF)-4] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff); } /* set Priority for Cortex-M  System Interrupts */
  else {
    NVIC->IP[(uint32_t)(IRQn)] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff);    }        /* set Priority for device specific Interrupts  */
 8000fe4:	2140      	movs	r1, #64	; 0x40
 8000fe6:	26a0      	movs	r6, #160	; 0xa0
 8000fe8:	2550      	movs	r5, #80	; 0x50
  uint32_t reg_value;
  uint32_t PriorityGroupTmp = (PriorityGroup & (uint32_t)0x07);               /* only values 0..7 are used          */

  reg_value  =  SCB->AIRCR;                                                   /* read old register configuration    */
  reg_value &= ~(SCB_AIRCR_VECTKEY_Msk | SCB_AIRCR_PRIGROUP_Msk);             /* clear bits to change               */
  reg_value  =  (reg_value                                 |
 8000fea:	f442 3200 	orr.w	r2, r2, #131072	; 0x20000
                ((uint32_t)0x5FA << SCB_AIRCR_VECTKEY_Pos) |
                (PriorityGroupTmp << 8));                                     /* Insert write key and priorty group */
  SCB->AIRCR =  reg_value;
 8000fee:	60c2      	str	r2, [r0, #12]

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
 8000ff0:	f44f 0400 	mov.w	r4, #8388608	; 0x800000
__STATIC_INLINE void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
  if(IRQn < 0) {
    SCB->SHP[((uint32_t)(IRQn) & 0xF)-4] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff); } /* set Priority for Cortex-M  System Interrupts */
  else {
    NVIC->IP[(uint32_t)(IRQn)] = ((priority << (8 - __NVIC_PRIO_BITS)) & 0xff);    }        /* set Priority for device specific Interrupts  */
 8000ff4:	f883 1347 	strb.w	r1, [r3, #839]	; 0x347
 8000ff8:	f883 6306 	strb.w	r6, [r3, #774]	; 0x306
 8000ffc:	f883 5317 	strb.w	r5, [r3, #791]	; 0x317

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
 8001000:	f8c3 1180 	str.w	r1, [r3, #384]	; 0x180
 8001004:	f8c3 4180 	str.w	r4, [r3, #384]	; 0x180
    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_EnableIRQ(IRQn_Type IRQn)
{
/*  NVIC->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  enable interrupt */
  NVIC->ISER[(uint32_t)((int32_t)IRQn) >> 5] = (uint32_t)(1 << ((uint32_t)((int32_t)IRQn) & (uint32_t)0x1F)); /* enable interrupt */
 8001008:	6019      	str	r1, [r3, #0]
 800100a:	601c      	str	r4, [r3, #0]
    NVIC_ClearPendingIRQ(EXTI9_5_IRQn);

    // Interrupt line is EXTI6 - PD6
    NVIC_EnableIRQ(EXTI0_IRQn);
    NVIC_EnableIRQ(EXTI9_5_IRQn);
 800100c:	bc70      	pop	{r4, r5, r6}
 800100e:	4770      	bx	lr
 8001010:	40023800 	.word	0x40023800
 8001014:	40013800 	.word	0x40013800
 8001018:	40013c00 	.word	0x40013c00
 800101c:	e000ed00 	.word	0xe000ed00
 8001020:	e000e100 	.word	0xe000e100

08001024 <setting_mode>:
#define SETTINGS_BIT_HPFILTER(x) 1 << (4 * (x - '0' - 1) + 1)
#define SETTINGS_BIT_LPFILTER(x) 1 << (4 * (x - '0' - 1) + 2)
#define SETTINGS_BIT_IMP(x)      1 << (4 * (x - '0' - 1) + 3)

void setting_mode()
{
 8001024:	b5f0      	push	{r4, r5, r6, r7, lr}

    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_DisableIRQ(IRQn_Type IRQn)
{
  NVIC->ICER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* disable interrupt */
 8001026:	4d9a      	ldr	r5, [pc, #616]	; (8001290 <setting_mode+0x26c>)
    // Disable IRQ to prevent idiotic stuff from happening
    NVIC_DisableIRQ(USART6_IRQn);

    STX("*********************************************\n");
 8001028:	489a      	ldr	r0, [pc, #616]	; (8001294 <setting_mode+0x270>)
 800102a:	2380      	movs	r3, #128	; 0x80
#define SETTINGS_BIT_HPFILTER(x) 1 << (4 * (x - '0' - 1) + 1)
#define SETTINGS_BIT_LPFILTER(x) 1 << (4 * (x - '0' - 1) + 2)
#define SETTINGS_BIT_IMP(x)      1 << (4 * (x - '0' - 1) + 3)

void setting_mode()
{
 800102c:	b08f      	sub	sp, #60	; 0x3c
 800102e:	f8c5 3088 	str.w	r3, [r5, #136]	; 0x88
    // Disable IRQ to prevent idiotic stuff from happening
    NVIC_DisableIRQ(USART6_IRQn);

    STX("*********************************************\n");
 8001032:	f7ff f92f 	bl	8000294 <STX>
    STX("*          SETTINGS MODE                    *\n");
 8001036:	4898      	ldr	r0, [pc, #608]	; (8001298 <setting_mode+0x274>)
 8001038:	f7ff f92c 	bl	8000294 <STX>
    STX("*===========================================*\n");
 800103c:	4897      	ldr	r0, [pc, #604]	; (800129c <setting_mode+0x278>)
 800103e:	f7ff f929 	bl	8000294 <STX>
    STX("* OPTIONS:                                  *\n");
 8001042:	4897      	ldr	r0, [pc, #604]	; (80012a0 <setting_mode+0x27c>)
 8001044:	f7ff f926 	bl	8000294 <STX>
    STX("*  - C_CHANNEL + OPT: SET CHANNEL           *\n");
 8001048:	4896      	ldr	r0, [pc, #600]	; (80012a4 <setting_mode+0x280>)
 800104a:	f7ff f923 	bl	8000294 <STX>
    STX("*  > OPT: NO, T-EST, S-HORT, TE-MP, SU-PPLY *\n");
 800104e:	4896      	ldr	r0, [pc, #600]	; (80012a8 <setting_mode+0x284>)
 8001050:	f7ff f920 	bl	8000294 <STX>
    STX("*  - I: ENABLE IMPEDENCE CHECKING  - NOPE   *\n");
 8001054:	4895      	ldr	r0, [pc, #596]	; (80012ac <setting_mode+0x288>)
 8001056:	f7ff f91d 	bl	8000294 <STX>
    STX("*  - H: ENABLE DC HP FILTER                 *\n");
 800105a:	4895      	ldr	r0, [pc, #596]	; (80012b0 <setting_mode+0x28c>)
 800105c:	f7ff f91a 	bl	8000294 <STX>
    STX("*  - L: ENABLE LP 0-40Hz FILTER             *\n");
 8001060:	4894      	ldr	r0, [pc, #592]	; (80012b4 <setting_mode+0x290>)
 8001062:	f7ff f917 	bl	8000294 <STX>
    STX("*  - S: PRINT ALL SETTINGS                  *\n");
 8001066:	4894      	ldr	r0, [pc, #592]	; (80012b8 <setting_mode+0x294>)
 8001068:	f7ff f914 	bl	8000294 <STX>
    STX("*  - P: PRINT CHANNEL CONFIGS               *\n");
 800106c:	4893      	ldr	r0, [pc, #588]	; (80012bc <setting_mode+0x298>)
 800106e:	f7ff f911 	bl	8000294 <STX>
    STX("*  - E: Quit                                *\n");
 8001072:	4893      	ldr	r0, [pc, #588]	; (80012c0 <setting_mode+0x29c>)
 8001074:	f7ff f90e 	bl	8000294 <STX>
    STX("*********************************************\n");
 8001078:	4886      	ldr	r0, [pc, #536]	; (8001294 <setting_mode+0x270>)
 800107a:	f7ff f90b 	bl	8000294 <STX>

    while (1) {

        // Signal end of transmission: 0x17 + \n:
        CTX(0x17);
 800107e:	2017      	movs	r0, #23
 8001080:	f7ff f8ea 	bl	8000258 <CTX>
        CTX('\n');
 8001084:	200a      	movs	r0, #10
 8001086:	f7ff f8e7 	bl	8000258 <CTX>
 800108a:	2300      	movs	r3, #0
    \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void NVIC_EnableIRQ(IRQn_Type IRQn)
{
/*  NVIC->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  enable interrupt */
  NVIC->ISER[(uint32_t)((int32_t)IRQn) >> 5] = (uint32_t)(1 << ((uint32_t)((int32_t)IRQn) & (uint32_t)0x1F)); /* enable interrupt */
 800108c:	2280      	movs	r2, #128	; 0x80
 800108e:	9301      	str	r3, [sp, #4]
 8001090:	9302      	str	r3, [sp, #8]
 8001092:	9303      	str	r3, [sp, #12]
 8001094:	9304      	str	r3, [sp, #16]
 8001096:	9305      	str	r3, [sp, #20]
 8001098:	9306      	str	r3, [sp, #24]
 800109a:	9307      	str	r3, [sp, #28]
 800109c:	9308      	str	r3, [sp, #32]
 800109e:	9309      	str	r3, [sp, #36]	; 0x24
 80010a0:	930a      	str	r3, [sp, #40]	; 0x28
 80010a2:	930b      	str	r3, [sp, #44]	; 0x2c
 80010a4:	930c      	str	r3, [sp, #48]	; 0x30
 80010a6:	f8ad 3034 	strh.w	r3, [sp, #52]	; 0x34
        // Initialize and clear input_str_cmd contents
        char input_str_cmd[50];
        for (int i = 0; i < 50; i++) input_str_cmd[i] = 0x0;

        NVIC_EnableIRQ(USART6_IRQn);
        SRX(input_str_cmd);
 80010aa:	a801      	add	r0, sp, #4
 80010ac:	60aa      	str	r2, [r5, #8]
 80010ae:	f7ff f8e6 	bl	800027e <SRX>

        // Echo the input string
        STX(input_str_cmd);
 80010b2:	a801      	add	r0, sp, #4
 80010b4:	f7ff f8ee 	bl	8000294 <STX>
        CTX('\n');
 80010b8:	200a      	movs	r0, #10
 80010ba:	f7ff f8cd 	bl	8000258 <CTX>


        // Check if the second argument/character is a valid number 1 - 9
        if (input_str_cmd[1] > '0' && input_str_cmd[1] < '9') {
 80010be:	f89d 3005 	ldrb.w	r3, [sp, #5]
 80010c2:	3b31      	subs	r3, #49	; 0x31
 80010c4:	2b07      	cmp	r3, #7

            /*************************
             *  SET UP A NEW CHANNEL *
             *************************/
            if (input_str_cmd[0] == 'C') {
 80010c6:	f89d 3004 	ldrb.w	r3, [sp, #4]
        STX(input_str_cmd);
        CTX('\n');


        // Check if the second argument/character is a valid number 1 - 9
        if (input_str_cmd[1] > '0' && input_str_cmd[1] < '9') {
 80010ca:	d81f      	bhi.n	800110c <setting_mode+0xe8>

            /*************************
             *  SET UP A NEW CHANNEL *
             *************************/
            if (input_str_cmd[0] == 'C') {
 80010cc:	2b43      	cmp	r3, #67	; 0x43
 80010ce:	d041      	beq.n	8001154 <setting_mode+0x130>
                */
                /**********************************
                 *  SET UP HP FILTER ON A CHANNEL *
                 **********************************/

            } else if (input_str_cmd[0] == 'H') {
 80010d0:	2b48      	cmp	r3, #72	; 0x48
 80010d2:	d070      	beq.n	80011b6 <setting_mode+0x192>

                /**********************************
                 *  SET UP LP FILTER ON A CHANNEL *
                 **********************************/

            } else if (input_str_cmd[0] == 'L') {
 80010d4:	2b4c      	cmp	r3, #76	; 0x4c
 80010d6:	d1d2      	bne.n	800107e <setting_mode+0x5a>

                STX("Low Pass Filter (40hz - 50hz Stop band) - CHANNEL_");
 80010d8:	487a      	ldr	r0, [pc, #488]	; (80012c4 <setting_mode+0x2a0>)
 80010da:	f7ff f8db 	bl	8000294 <STX>
                CTX(input_str_cmd[1]);
 80010de:	f89d 0005 	ldrb.w	r0, [sp, #5]
 80010e2:	f7ff f8b9 	bl	8000258 <CTX>

                // BIOEXG_SETTINGS ^= 1 << (4 * (input_str_cmd[1] - '0' - 1) + 2);
                BIOEXG_SETTINGS ^= SETTINGS_BIT_LPFILTER(input_str_cmd[1]);
 80010e6:	f89d 3005 	ldrb.w	r3, [sp, #5]
 80010ea:	4877      	ldr	r0, [pc, #476]	; (80012c8 <setting_mode+0x2a4>)
 80010ec:	009b      	lsls	r3, r3, #2
 80010ee:	3bc2      	subs	r3, #194	; 0xc2

                STX("High Pass Filter (DC Filter) - CHANNEL_");
                CTX(input_str_cmd[1]);

                // BIOEXG_SETTINGS ^= 1 << (4 * (input_str_cmd[1] - '0' - 1) + 1);
                BIOEXG_SETTINGS ^= SETTINGS_BIT_HPFILTER(input_str_cmd[1]);
 80010f0:	6801      	ldr	r1, [r0, #0]
 80010f2:	2201      	movs	r2, #1
 80010f4:	fa02 f303 	lsl.w	r3, r2, r3
 80010f8:	ea83 0201 	eor.w	r2, r3, r1
                if (BIOEXG_SETTINGS & SETTINGS_BIT_HPFILTER(input_str_cmd[1])) STX(": ON\n");
 80010fc:	421a      	tst	r2, r3

                STX("High Pass Filter (DC Filter) - CHANNEL_");
                CTX(input_str_cmd[1]);

                // BIOEXG_SETTINGS ^= 1 << (4 * (input_str_cmd[1] - '0' - 1) + 1);
                BIOEXG_SETTINGS ^= SETTINGS_BIT_HPFILTER(input_str_cmd[1]);
 80010fe:	6002      	str	r2, [r0, #0]
                if (BIOEXG_SETTINGS & SETTINGS_BIT_HPFILTER(input_str_cmd[1])) STX(": ON\n");
 8001100:	bf14      	ite	ne
 8001102:	4872      	ldrne	r0, [pc, #456]	; (80012cc <setting_mode+0x2a8>)
                else                                                           STX(": OFF\n");
 8001104:	4872      	ldreq	r0, [pc, #456]	; (80012d0 <setting_mode+0x2ac>)
 8001106:	f7ff f8c5 	bl	8000294 <STX>
 800110a:	e7b8      	b.n	800107e <setting_mode+0x5a>
            }
            /**************************
             *  PRINT CHANNEL CONFIGS *
             **************************/

        } else if (input_str_cmd[0] == 'P') {
 800110c:	2b50      	cmp	r3, #80	; 0x50
 800110e:	d008      	beq.n	8001122 <setting_mode+0xfe>

            /**********************
             *  PRINT ALL CONFIGS *
             **********************/

        } else if (input_str_cmd[0] == 'S') {
 8001110:	2b53      	cmp	r3, #83	; 0x53
 8001112:	d05d      	beq.n	80011d0 <setting_mode+0x1ac>
            CTX('\n');
            /*********
             *  QUIT *
             *********/

        } else if (input_str_cmd[0] == 'E') break;
 8001114:	2b45      	cmp	r3, #69	; 0x45
 8001116:	f000 80a5 	beq.w	8001264 <setting_mode+0x240>

        else  STX("WTF\n");
 800111a:	486e      	ldr	r0, [pc, #440]	; (80012d4 <setting_mode+0x2b0>)
 800111c:	f7ff f8ba 	bl	8000294 <STX>
    }
 8001120:	e7ad      	b.n	800107e <setting_mode+0x5a>
 8001122:	2431      	movs	r4, #49	; 0x31
             *  PRINT CHANNEL CONFIGS *
             **************************/

        } else if (input_str_cmd[0] == 'P') {
            for (int i = 0; i < 8; i++) {
                STX("Channel ");
 8001124:	486c      	ldr	r0, [pc, #432]	; (80012d8 <setting_mode+0x2b4>)
 8001126:	f7ff f8b5 	bl	8000294 <STX>
                CTX(i + '1');
 800112a:	4620      	mov	r0, r4
 800112c:	f7ff f894 	bl	8000258 <CTX>
                STX(": ");
 8001130:	486a      	ldr	r0, [pc, #424]	; (80012dc <setting_mode+0x2b8>)
 8001132:	f7ff f8af 	bl	8000294 <STX>
 8001136:	f1a4 002c 	sub.w	r0, r4, #44	; 0x2c
                PREG(ads1299_read_reg(CH1SET + i));
 800113a:	b2c0      	uxtb	r0, r0
 800113c:	f7ff fb1a 	bl	8000774 <ads1299_read_reg>
 8001140:	3401      	adds	r4, #1
 8001142:	f7ff f8b2 	bl	80002aa <PREG>
 8001146:	b2e4      	uxtb	r4, r4
                CTX('\n');
 8001148:	200a      	movs	r0, #10
 800114a:	f7ff f885 	bl	8000258 <CTX>
            /**************************
             *  PRINT CHANNEL CONFIGS *
             **************************/

        } else if (input_str_cmd[0] == 'P') {
            for (int i = 0; i < 8; i++) {
 800114e:	2c39      	cmp	r4, #57	; 0x39
 8001150:	d1e8      	bne.n	8001124 <setting_mode+0x100>
 8001152:	e794      	b.n	800107e <setting_mode+0x5a>
            /*************************
             *  SET UP A NEW CHANNEL *
             *************************/
            if (input_str_cmd[0] == 'C') {

                STX("CHANNEL_");
 8001154:	4862      	ldr	r0, [pc, #392]	; (80012e0 <setting_mode+0x2bc>)
 8001156:	f7ff f89d 	bl	8000294 <STX>
                // Check if the input is a number[1:8]

                // Echo channel number
                CTX(input_str_cmd[1]);
 800115a:	f89d 0005 	ldrb.w	r0, [sp, #5]
 800115e:	f7ff f87b 	bl	8000258 <CTX>

                uint8_t input_opt = 0x0;

                if      (input_str_cmd[2] == 'N') {
 8001162:	f89d 0006 	ldrb.w	r0, [sp, #6]
 8001166:	284e      	cmp	r0, #78	; 0x4e
 8001168:	d00a      	beq.n	8001180 <setting_mode+0x15c>
                    input_opt = ADS1299_INPUT_NORMAL;
                    CTX('N');
                } else if (input_str_cmd[2] == 'T') {
 800116a:	2854      	cmp	r0, #84	; 0x54
 800116c:	f000 8081 	beq.w	8001272 <setting_mode+0x24e>
                    input_opt = ADS1299_INPUT_TESTSIGNAL;
                    CTX('T');
                } else if (input_str_cmd[2] == 'S') {
 8001170:	2853      	cmp	r0, #83	; 0x53
 8001172:	f000 8083 	beq.w	800127c <setting_mode+0x258>
                    input_opt = ADS1299_INPUT_SHORTED;
                    CTX('S');
                } else if (input_str_cmd[2] == 'E') {
 8001176:	2845      	cmp	r0, #69	; 0x45
 8001178:	f000 8085 	beq.w	8001286 <setting_mode+0x262>
                    input_opt = ADS1299_INPUT_TEMP;
                    CTX('E');
                } else if (input_str_cmd[2] == 'U') {
 800117c:	2855      	cmp	r0, #85	; 0x55
 800117e:	d073      	beq.n	8001268 <setting_mode+0x244>

                uint8_t input_opt = 0x0;

                if      (input_str_cmd[2] == 'N') {
                    input_opt = ADS1299_INPUT_NORMAL;
                    CTX('N');
 8001180:	204e      	movs	r0, #78	; 0x4e
 8001182:	f7ff f869 	bl	8000258 <CTX>
 8001186:	2160      	movs	r1, #96	; 0x60
 8001188:	22e0      	movs	r2, #224	; 0xe0
                    input_opt = ADS1299_INPUT_SUPPLY;
                    CTX('U');
                } else
                    CTX('N');

                BIOEXG_SETTINGS ^= SETTINGS_BIT_CHANNEL(input_str_cmd[1]);
 800118a:	f89d 3005 	ldrb.w	r3, [sp, #5]
 800118e:	4f4e      	ldr	r7, [pc, #312]	; (80012c8 <setting_mode+0x2a4>)
 8001190:	f1a3 0631 	sub.w	r6, r3, #49	; 0x31
 8001194:	683c      	ldr	r4, [r7, #0]
 8001196:	00b6      	lsls	r6, r6, #2
 8001198:	2001      	movs	r0, #1
 800119a:	40b0      	lsls	r0, r6
 800119c:	4044      	eors	r4, r0

                // Determine whether or not the Channel is ON or OFF
                if (BIOEXG_SETTINGS & SETTINGS_BIT_CHANNEL(input_str_cmd[1])) {
 800119e:	4204      	tst	r4, r0
                    // Magic happens underneath (input_str_cmd[1] - '1' + CH1SET), don't touch
                    ads1299_write_reg((uint8_t) input_str_cmd[1] - '1' + CH1SET, ADS1299_INPUT_PWR_UP
 80011a0:	f1a3 002c 	sub.w	r0, r3, #44	; 0x2c
                    input_opt = ADS1299_INPUT_SUPPLY;
                    CTX('U');
                } else
                    CTX('N');

                BIOEXG_SETTINGS ^= SETTINGS_BIT_CHANNEL(input_str_cmd[1]);
 80011a4:	603c      	str	r4, [r7, #0]

                // Determine whether or not the Channel is ON or OFF
                if (BIOEXG_SETTINGS & SETTINGS_BIT_CHANNEL(input_str_cmd[1])) {
                    // Magic happens underneath (input_str_cmd[1] - '1' + CH1SET), don't touch
                    ads1299_write_reg((uint8_t) input_str_cmd[1] - '1' + CH1SET, ADS1299_INPUT_PWR_UP
 80011a6:	b2c0      	uxtb	r0, r0
                    CTX('N');

                BIOEXG_SETTINGS ^= SETTINGS_BIT_CHANNEL(input_str_cmd[1]);

                // Determine whether or not the Channel is ON or OFF
                if (BIOEXG_SETTINGS & SETTINGS_BIT_CHANNEL(input_str_cmd[1])) {
 80011a8:	d055      	beq.n	8001256 <setting_mode+0x232>
                    // Magic happens underneath (input_str_cmd[1] - '1' + CH1SET), don't touch
                    ads1299_write_reg((uint8_t) input_str_cmd[1] - '1' + CH1SET, ADS1299_INPUT_PWR_UP
 80011aa:	f7ff f999 	bl	80004e0 <ads1299_write_reg>
                                      | ADS1299_PGA_GAIN24 | input_opt);
                    STX(": ON\n");
 80011ae:	4847      	ldr	r0, [pc, #284]	; (80012cc <setting_mode+0x2a8>)
 80011b0:	f7ff f870 	bl	8000294 <STX>
 80011b4:	e763      	b.n	800107e <setting_mode+0x5a>
                 *  SET UP HP FILTER ON A CHANNEL *
                 **********************************/

            } else if (input_str_cmd[0] == 'H') {

                STX("High Pass Filter (DC Filter) - CHANNEL_");
 80011b6:	484b      	ldr	r0, [pc, #300]	; (80012e4 <setting_mode+0x2c0>)
 80011b8:	f7ff f86c 	bl	8000294 <STX>
                CTX(input_str_cmd[1]);
 80011bc:	f89d 0005 	ldrb.w	r0, [sp, #5]
 80011c0:	f7ff f84a 	bl	8000258 <CTX>

                // BIOEXG_SETTINGS ^= 1 << (4 * (input_str_cmd[1] - '0' - 1) + 1);
                BIOEXG_SETTINGS ^= SETTINGS_BIT_HPFILTER(input_str_cmd[1]);
 80011c4:	f89d 3005 	ldrb.w	r3, [sp, #5]
 80011c8:	483f      	ldr	r0, [pc, #252]	; (80012c8 <setting_mode+0x2a4>)
 80011ca:	009b      	lsls	r3, r3, #2
 80011cc:	3bc3      	subs	r3, #195	; 0xc3
 80011ce:	e78f      	b.n	80010f0 <setting_mode+0xcc>
            /**********************
             *  PRINT ALL CONFIGS *
             **********************/

        } else if (input_str_cmd[0] == 'S') {
            STX("Channel (FFT) Status: ");
 80011d0:	4845      	ldr	r0, [pc, #276]	; (80012e8 <setting_mode+0x2c4>)
 80011d2:	f7ff f85f 	bl	8000294 <STX>
            PREG(BIOEXG_SETTINGS);
 80011d6:	4b3c      	ldr	r3, [pc, #240]	; (80012c8 <setting_mode+0x2a4>)
 80011d8:	6818      	ldr	r0, [r3, #0]
 80011da:	f7ff f866 	bl	80002aa <PREG>

            STX("\nConfiguration Register 1: ");
 80011de:	4843      	ldr	r0, [pc, #268]	; (80012ec <setting_mode+0x2c8>)
 80011e0:	f7ff f858 	bl	8000294 <STX>
            PREG(ads1299_read_reg(CONFIG1));
 80011e4:	2001      	movs	r0, #1
 80011e6:	f7ff fac5 	bl	8000774 <ads1299_read_reg>
 80011ea:	f7ff f85e 	bl	80002aa <PREG>

            STX("\nConfiguration Register 2: ");
 80011ee:	4840      	ldr	r0, [pc, #256]	; (80012f0 <setting_mode+0x2cc>)
 80011f0:	f7ff f850 	bl	8000294 <STX>
            PREG(ads1299_read_reg(CONFIG2));
 80011f4:	2002      	movs	r0, #2
 80011f6:	f7ff fabd 	bl	8000774 <ads1299_read_reg>
 80011fa:	f7ff f856 	bl	80002aa <PREG>

            STX("\nConfiguration Register 3: ");
 80011fe:	483d      	ldr	r0, [pc, #244]	; (80012f4 <setting_mode+0x2d0>)
 8001200:	f7ff f848 	bl	8000294 <STX>
            PREG(ads1299_read_reg(CONFIG3));
 8001204:	2003      	movs	r0, #3
 8001206:	f7ff fab5 	bl	8000774 <ads1299_read_reg>
 800120a:	f7ff f84e 	bl	80002aa <PREG>

            STX("\nConfiguration Register 4: ");
 800120e:	483a      	ldr	r0, [pc, #232]	; (80012f8 <setting_mode+0x2d4>)
 8001210:	f7ff f840 	bl	8000294 <STX>
            PREG(ads1299_read_reg(CONFIG4));
 8001214:	2017      	movs	r0, #23
 8001216:	f7ff faad 	bl	8000774 <ads1299_read_reg>
 800121a:	f7ff f846 	bl	80002aa <PREG>

            STX("\nLOFF: ");
 800121e:	4837      	ldr	r0, [pc, #220]	; (80012fc <setting_mode+0x2d8>)
 8001220:	f7ff f838 	bl	8000294 <STX>
            PREG(ads1299_read_reg(LOFF));
 8001224:	2004      	movs	r0, #4
 8001226:	f7ff faa5 	bl	8000774 <ads1299_read_reg>
 800122a:	f7ff f83e 	bl	80002aa <PREG>

            STX("\nLOFF_SENSEP: ");
 800122e:	4834      	ldr	r0, [pc, #208]	; (8001300 <setting_mode+0x2dc>)
 8001230:	f7ff f830 	bl	8000294 <STX>
            PREG(ads1299_read_reg(LOFF_SENSP));
 8001234:	200f      	movs	r0, #15
 8001236:	f7ff fa9d 	bl	8000774 <ads1299_read_reg>
 800123a:	f7ff f836 	bl	80002aa <PREG>

            STX("\nLOFF_SENSEN: ");
 800123e:	4831      	ldr	r0, [pc, #196]	; (8001304 <setting_mode+0x2e0>)
 8001240:	f7ff f828 	bl	8000294 <STX>
            PREG(ads1299_read_reg(LOFF_SENSN));
 8001244:	2010      	movs	r0, #16
 8001246:	f7ff fa95 	bl	8000774 <ads1299_read_reg>
 800124a:	f7ff f82e 	bl	80002aa <PREG>

            CTX('\n');
 800124e:	200a      	movs	r0, #10
 8001250:	f7ff f802 	bl	8000258 <CTX>
 8001254:	e713      	b.n	800107e <setting_mode+0x5a>
                    ads1299_write_reg((uint8_t) input_str_cmd[1] - '1' + CH1SET, ADS1299_INPUT_PWR_UP
                                      | ADS1299_PGA_GAIN24 | input_opt);
                    STX(": ON\n");

                } else {
                    ads1299_write_reg((uint8_t) input_str_cmd[1] - '1' + CH1SET, ADS1299_INPUT_PWR_DOWN
 8001256:	4611      	mov	r1, r2
 8001258:	f7ff f942 	bl	80004e0 <ads1299_write_reg>
                                      | ADS1299_PGA_GAIN24 | input_opt);
                    STX(": OFF\n");
 800125c:	481c      	ldr	r0, [pc, #112]	; (80012d0 <setting_mode+0x2ac>)
 800125e:	f7ff f819 	bl	8000294 <STX>
 8001262:	e70c      	b.n	800107e <setting_mode+0x5a>

        } else if (input_str_cmd[0] == 'E') break;

        else  STX("WTF\n");
    }
 8001264:	b00f      	add	sp, #60	; 0x3c
 8001266:	bdf0      	pop	{r4, r5, r6, r7, pc}
                } else if (input_str_cmd[2] == 'E') {
                    input_opt = ADS1299_INPUT_TEMP;
                    CTX('E');
                } else if (input_str_cmd[2] == 'U') {
                    input_opt = ADS1299_INPUT_SUPPLY;
                    CTX('U');
 8001268:	f7fe fff6 	bl	8000258 <CTX>
 800126c:	2163      	movs	r1, #99	; 0x63
 800126e:	22e3      	movs	r2, #227	; 0xe3
 8001270:	e78b      	b.n	800118a <setting_mode+0x166>
                if      (input_str_cmd[2] == 'N') {
                    input_opt = ADS1299_INPUT_NORMAL;
                    CTX('N');
                } else if (input_str_cmd[2] == 'T') {
                    input_opt = ADS1299_INPUT_TESTSIGNAL;
                    CTX('T');
 8001272:	f7fe fff1 	bl	8000258 <CTX>
 8001276:	2165      	movs	r1, #101	; 0x65
 8001278:	22e5      	movs	r2, #229	; 0xe5
 800127a:	e786      	b.n	800118a <setting_mode+0x166>
                } else if (input_str_cmd[2] == 'S') {
                    input_opt = ADS1299_INPUT_SHORTED;
                    CTX('S');
 800127c:	f7fe ffec 	bl	8000258 <CTX>
 8001280:	2161      	movs	r1, #97	; 0x61
 8001282:	22e1      	movs	r2, #225	; 0xe1
 8001284:	e781      	b.n	800118a <setting_mode+0x166>
                } else if (input_str_cmd[2] == 'E') {
                    input_opt = ADS1299_INPUT_TEMP;
                    CTX('E');
 8001286:	f7fe ffe7 	bl	8000258 <CTX>
 800128a:	2164      	movs	r1, #100	; 0x64
 800128c:	22e4      	movs	r2, #228	; 0xe4
 800128e:	e77c      	b.n	800118a <setting_mode+0x166>
 8001290:	e000e100 	.word	0xe000e100
 8001294:	08037c14 	.word	0x08037c14
 8001298:	08037c44 	.word	0x08037c44
 800129c:	08037c74 	.word	0x08037c74
 80012a0:	08037ca4 	.word	0x08037ca4
 80012a4:	08037cd4 	.word	0x08037cd4
 80012a8:	08037d04 	.word	0x08037d04
 80012ac:	08037d34 	.word	0x08037d34
 80012b0:	08037d64 	.word	0x08037d64
 80012b4:	08037d94 	.word	0x08037d94
 80012b8:	08037dc4 	.word	0x08037dc4
 80012bc:	08037df4 	.word	0x08037df4
 80012c0:	08037e24 	.word	0x08037e24
 80012c4:	08037e98 	.word	0x08037e98
 80012c8:	200004b4 	.word	0x200004b4
 80012cc:	08037e60 	.word	0x08037e60
 80012d0:	08037e68 	.word	0x08037e68
 80012d4:	08037f88 	.word	0x08037f88
 80012d8:	08037ecc 	.word	0x08037ecc
 80012dc:	08037f84 	.word	0x08037f84
 80012e0:	08037e54 	.word	0x08037e54
 80012e4:	08037e70 	.word	0x08037e70
 80012e8:	08037ed8 	.word	0x08037ed8
 80012ec:	08037ef0 	.word	0x08037ef0
 80012f0:	08037f0c 	.word	0x08037f0c
 80012f4:	08037f28 	.word	0x08037f28
 80012f8:	08037f44 	.word	0x08037f44
 80012fc:	08037f60 	.word	0x08037f60
 8001300:	08037f68 	.word	0x08037f68
 8001304:	08037f78 	.word	0x08037f78

08001308 <arm_sin_f32>:
  int32_t n;
  float32_t findex;

  /* input x is in radians */
  /* Scale the input to [0 1] range from [0 2*PI] , divide input by 2*pi */
  in = x * 0.159154943092f;
 8001308:	ed9f 7a1a 	vldr	s14, [pc, #104]	; 8001374 <arm_sin_f32+0x6c>

  /* Calculation of floor value of input */
  n = (int32_t) in;

  /* Make negative values towards -infinity */
  if(x < 0.0f)
 800130c:	eeb5 0ac0 	vcmpe.f32	s0, #0.0
  int32_t n;
  float32_t findex;

  /* input x is in radians */
  /* Scale the input to [0 1] range from [0 2*PI] , divide input by 2*pi */
  in = x * 0.159154943092f;
 8001310:	ee20 7a07 	vmul.f32	s14, s0, s14

  /* Calculation of floor value of input */
  n = (int32_t) in;

  /* Make negative values towards -infinity */
  if(x < 0.0f)
 8001314:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
  /* input x is in radians */
  /* Scale the input to [0 1] range from [0 2*PI] , divide input by 2*pi */
  in = x * 0.159154943092f;

  /* Calculation of floor value of input */
  n = (int32_t) in;
 8001318:	eefd 7ac7 	vcvt.s32.f32	s15, s14

  /* Make negative values towards -infinity */
  if(x < 0.0f)
 800131c:	d504      	bpl.n	8001328 <arm_sin_f32+0x20>
  {
    n--;
 800131e:	ee17 3a90 	vmov	r3, s15
 8001322:	3b01      	subs	r3, #1
 8001324:	ee07 3a90 	vmov	s15, r3
  }

  /* Map input value to [0 1] */
  in = in - (float32_t) n;
 8001328:	eef8 7ae7 	vcvt.f32.s32	s15, s15

  /* Calculation of index of the table */
  findex = (float32_t) FAST_MATH_TABLE_SIZE * in;
 800132c:	eddf 6a12 	vldr	s13, [pc, #72]	; 8001378 <arm_sin_f32+0x70>

  /* fractional value calculation */
  fract = findex - (float32_t) index;

  /* Read two nearest values of input value from the sin table */
  a = sinTable_f32[index];
 8001330:	4a12      	ldr	r2, [pc, #72]	; (800137c <arm_sin_f32+0x74>)
  {
    n--;
  }

  /* Map input value to [0 1] */
  in = in - (float32_t) n;
 8001332:	ee37 7a67 	vsub.f32	s14, s14, s15
  /* Read two nearest values of input value from the sin table */
  a = sinTable_f32[index];
  b = sinTable_f32[index+1];

  /* Linear interpolation process */
  sinVal = (1.0f-fract)*a + fract*b;
 8001336:	eeb7 0a00 	vmov.f32	s0, #112	; 0x70

  /* Map input value to [0 1] */
  in = in - (float32_t) n;

  /* Calculation of index of the table */
  findex = (float32_t) FAST_MATH_TABLE_SIZE * in;
 800133a:	ee67 7a26 	vmul.f32	s15, s14, s13
  index = ((uint16_t)findex) & 0x1ff;
 800133e:	eebc 7ae7 	vcvt.u32.f32	s14, s15
 8001342:	ee17 3a10 	vmov	r3, s14
 8001346:	f3c3 0308 	ubfx	r3, r3, #0, #9

  /* fractional value calculation */
  fract = findex - (float32_t) index;
 800134a:	ee07 3a10 	vmov	s14, r3
 800134e:	eeb8 7a47 	vcvt.f32.u32	s14, s14

  /* Read two nearest values of input value from the sin table */
  a = sinTable_f32[index];
 8001352:	eb02 0183 	add.w	r1, r2, r3, lsl #2
  /* Calculation of index of the table */
  findex = (float32_t) FAST_MATH_TABLE_SIZE * in;
  index = ((uint16_t)findex) & 0x1ff;

  /* fractional value calculation */
  fract = findex - (float32_t) index;
 8001356:	ee77 7ac7 	vsub.f32	s15, s15, s14
  /* Read two nearest values of input value from the sin table */
  a = sinTable_f32[index];
  b = sinTable_f32[index+1];

  /* Linear interpolation process */
  sinVal = (1.0f-fract)*a + fract*b;
 800135a:	edd1 6a01 	vldr	s13, [r1, #4]
 800135e:	ee30 7a67 	vsub.f32	s14, s0, s15
 8001362:	ed91 0a00 	vldr	s0, [r1]
 8001366:	ee67 7aa6 	vmul.f32	s15, s15, s13
 800136a:	ee27 0a00 	vmul.f32	s0, s14, s0

  /* Return the output value */
  return (sinVal);
}
 800136e:	ee30 0a27 	vadd.f32	s0, s0, s15
 8001372:	4770      	bx	lr
 8001374:	3e22f983 	.word	0x3e22f983
 8001378:	44000000 	.word	0x44000000
 800137c:	08003710 	.word	0x08003710

08001380 <arm_cos_f32>:
  int32_t n;
  float32_t findex;

  /* input x is in radians */
  /* Scale the input to [0 1] range from [0 2*PI] , divide input by 2*pi, add 0.25 (pi/2) to read sine table */
  in = x * 0.159154943092f + 0.25f;
 8001380:	eddf 7a1c 	vldr	s15, [pc, #112]	; 80013f4 <arm_cos_f32+0x74>
 8001384:	ee20 0a27 	vmul.f32	s0, s0, s15
 8001388:	eef5 7a00 	vmov.f32	s15, #80	; 0x50
 800138c:	ee30 0a27 	vadd.f32	s0, s0, s15

  /* Calculation of floor value of input */
  n = (int32_t) in;

  /* Make negative values towards -infinity */
  if(in < 0.0f)
 8001390:	eeb5 0ac0 	vcmpe.f32	s0, #0.0
 8001394:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
  /* input x is in radians */
  /* Scale the input to [0 1] range from [0 2*PI] , divide input by 2*pi, add 0.25 (pi/2) to read sine table */
  in = x * 0.159154943092f + 0.25f;

  /* Calculation of floor value of input */
  n = (int32_t) in;
 8001398:	eefd 7ac0 	vcvt.s32.f32	s15, s0

  /* Make negative values towards -infinity */
  if(in < 0.0f)
 800139c:	d504      	bpl.n	80013a8 <arm_cos_f32+0x28>
  {
    n--;
 800139e:	ee17 3a90 	vmov	r3, s15
 80013a2:	3b01      	subs	r3, #1
 80013a4:	ee07 3a90 	vmov	s15, r3
  }

  /* Map input value to [0 1] */
  in = in - (float32_t) n;
 80013a8:	eef8 7ae7 	vcvt.f32.s32	s15, s15

  /* Calculation of index of the table */
  findex = (float32_t) FAST_MATH_TABLE_SIZE * in;
 80013ac:	eddf 6a12 	vldr	s13, [pc, #72]	; 80013f8 <arm_cos_f32+0x78>

  /* fractional value calculation */
  fract = findex - (float32_t) index;

  /* Read two nearest values of input value from the cos table */
  a = sinTable_f32[index];
 80013b0:	4a12      	ldr	r2, [pc, #72]	; (80013fc <arm_cos_f32+0x7c>)
  {
    n--;
  }

  /* Map input value to [0 1] */
  in = in - (float32_t) n;
 80013b2:	ee30 0a67 	vsub.f32	s0, s0, s15
  /* Read two nearest values of input value from the cos table */
  a = sinTable_f32[index];
  b = sinTable_f32[index+1];

  /* Linear interpolation process */
  cosVal = (1.0f-fract)*a + fract*b;
 80013b6:	eeb7 7a00 	vmov.f32	s14, #112	; 0x70

  /* Map input value to [0 1] */
  in = in - (float32_t) n;

  /* Calculation of index of the table */
  findex = (float32_t) FAST_MATH_TABLE_SIZE * in;
 80013ba:	ee20 0a26 	vmul.f32	s0, s0, s13
  index = ((uint16_t)findex) & 0x1ff;
 80013be:	eefc 7ac0 	vcvt.u32.f32	s15, s0
 80013c2:	ee17 3a90 	vmov	r3, s15
 80013c6:	f3c3 0308 	ubfx	r3, r3, #0, #9

  /* fractional value calculation */
  fract = findex - (float32_t) index;
 80013ca:	ee07 3a90 	vmov	s15, r3
 80013ce:	eef8 7a67 	vcvt.f32.u32	s15, s15

  /* Read two nearest values of input value from the cos table */
  a = sinTable_f32[index];
 80013d2:	eb02 0183 	add.w	r1, r2, r3, lsl #2
  /* Calculation of index of the table */
  findex = (float32_t) FAST_MATH_TABLE_SIZE * in;
  index = ((uint16_t)findex) & 0x1ff;

  /* fractional value calculation */
  fract = findex - (float32_t) index;
 80013d6:	ee70 7a67 	vsub.f32	s15, s0, s15
  /* Read two nearest values of input value from the cos table */
  a = sinTable_f32[index];
  b = sinTable_f32[index+1];

  /* Linear interpolation process */
  cosVal = (1.0f-fract)*a + fract*b;
 80013da:	edd1 6a01 	vldr	s13, [r1, #4]
 80013de:	ed91 0a00 	vldr	s0, [r1]
 80013e2:	ee37 7a67 	vsub.f32	s14, s14, s15
 80013e6:	ee67 7aa6 	vmul.f32	s15, s15, s13
 80013ea:	ee27 0a00 	vmul.f32	s0, s14, s0

  /* Return the output value */
  return (cosVal);
}
 80013ee:	ee30 0a27 	vadd.f32	s0, s0, s15
 80013f2:	4770      	bx	lr
 80013f4:	3e22f983 	.word	0x3e22f983
 80013f8:	44000000 	.word	0x44000000
 80013fc:	08003710 	.word	0x08003710

08001400 <arm_mult_f32>:
void arm_mult_f32(
  float32_t * pSrcA,
  float32_t * pSrcB,
  float32_t * pDst,
  uint32_t blockSize)
{
 8001400:	b5f0      	push	{r4, r5, r6, r7, lr}
  /* loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.        
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 8001402:	ea5f 0e93 	movs.w	lr, r3, lsr #2
 8001406:	d033      	beq.n	8001470 <arm_mult_f32+0x70>
 8001408:	f100 0610 	add.w	r6, r0, #16
 800140c:	f101 0510 	add.w	r5, r1, #16
 8001410:	f102 0410 	add.w	r4, r2, #16
 8001414:	4677      	mov	r7, lr
    inA2 = *(pSrcA + 1);
    /* read sample from sourceB */
    inB2 = *(pSrcB + 1);

    /* out = sourceA * sourceB */
    out1 = inA1 * inB1;
 8001416:	ed16 6a04 	vldr	s12, [r6, #-16]
 800141a:	ed55 4a04 	vldr	s9, [r5, #-16]

    /* read sample from sourceA */
    inA3 = *(pSrcA + 2);
 800141e:	ed56 6a02 	vldr	s13, [r6, #-8]
    /* read sample from sourceB */
    inB3 = *(pSrcB + 2);
 8001422:	ed15 5a02 	vldr	s10, [r5, #-8]

    /* out = sourceA * sourceB */
    out2 = inA2 * inB2;
 8001426:	ed16 7a03 	vldr	s14, [r6, #-12]
 800142a:	ed55 5a03 	vldr	s11, [r5, #-12]

    /* read sample from sourceA */
    inA4 = *(pSrcA + 3);
 800142e:	ed56 7a01 	vldr	s15, [r6, #-4]
    inA2 = *(pSrcA + 1);
    /* read sample from sourceB */
    inB2 = *(pSrcB + 1);

    /* out = sourceA * sourceB */
    out1 = inA1 * inB1;
 8001432:	ee26 6a24 	vmul.f32	s12, s12, s9

    /* read sample from sourceB */
    inB4 = *(pSrcB + 3);

    /* out = sourceA * sourceB */
    out3 = inA3 * inB3;
 8001436:	ee66 6a85 	vmul.f32	s13, s13, s10

    /* read sample from sourceA */
    inA4 = *(pSrcA + 3);

    /* store result to destination buffer */
    *pDst = out1;
 800143a:	ed04 6a04 	vstr	s12, [r4, #-16]

    /* read sample from sourceB */
    inB4 = *(pSrcB + 3);
 800143e:	ed15 6a01 	vldr	s12, [r5, #-4]
    *(pDst + 1) = out2;

    /* out = sourceA * sourceB */
    out4 = inA4 * inB4;
    /* store result to destination buffer */
    *(pDst + 2) = out3;
 8001442:	ed44 6a02 	vstr	s13, [r4, #-8]
    inA3 = *(pSrcA + 2);
    /* read sample from sourceB */
    inB3 = *(pSrcB + 2);

    /* out = sourceA * sourceB */
    out2 = inA2 * inB2;
 8001446:	ee27 7a25 	vmul.f32	s14, s14, s11

    /* store result to destination buffer */
    *(pDst + 1) = out2;

    /* out = sourceA * sourceB */
    out4 = inA4 * inB4;
 800144a:	ee67 7a86 	vmul.f32	s15, s15, s12
  /* loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.        
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 800144e:	3f01      	subs	r7, #1

    /* out = sourceA * sourceB */
    out3 = inA3 * inB3;

    /* store result to destination buffer */
    *(pDst + 1) = out2;
 8001450:	ed04 7a03 	vstr	s14, [r4, #-12]
    /* out = sourceA * sourceB */
    out4 = inA4 * inB4;
    /* store result to destination buffer */
    *(pDst + 2) = out3;
    /* store result to destination buffer */
    *(pDst + 3) = out4;
 8001454:	ed44 7a01 	vstr	s15, [r4, #-4]
 8001458:	f106 0610 	add.w	r6, r6, #16
 800145c:	f105 0510 	add.w	r5, r5, #16
 8001460:	f104 0410 	add.w	r4, r4, #16
  /* loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.        
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 8001464:	d1d7      	bne.n	8001416 <arm_mult_f32+0x16>
 8001466:	ea4f 140e 	mov.w	r4, lr, lsl #4
 800146a:	4420      	add	r0, r4
 800146c:	4421      	add	r1, r4
 800146e:	4422      	add	r2, r4
  /* Initialize blkCnt with number of samples */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 8001470:	f013 0303 	ands.w	r3, r3, #3
 8001474:	d009      	beq.n	800148a <arm_mult_f32+0x8a>
  {
    /* C = A * B */
    /* Multiply the inputs and store the results in output buffer */
    *pDst++ = (*pSrcA++) * (*pSrcB++);
 8001476:	ecf0 7a01 	vldmia	r0!, {s15}
 800147a:	ecb1 7a01 	vldmia	r1!, {s14}
 800147e:	ee67 7a87 	vmul.f32	s15, s15, s14
  /* Initialize blkCnt with number of samples */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 8001482:	3b01      	subs	r3, #1
  {
    /* C = A * B */
    /* Multiply the inputs and store the results in output buffer */
    *pDst++ = (*pSrcA++) * (*pSrcB++);
 8001484:	ece2 7a01 	vstmia	r2!, {s15}
  /* Initialize blkCnt with number of samples */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 8001488:	d1f5      	bne.n	8001476 <arm_mult_f32+0x76>
 800148a:	bdf0      	pop	{r4, r5, r6, r7, pc}

0800148c <arm_scale_f32>:
void arm_scale_f32(
  float32_t * pSrc,
  float32_t scale,
  float32_t * pDst,
  uint32_t blockSize)
{
 800148c:	b470      	push	{r4, r5, r6}
  /*loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.        
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 800148e:	0896      	lsrs	r6, r2, #2
 8001490:	d025      	beq.n	80014de <arm_scale_f32+0x52>
 8001492:	f100 0410 	add.w	r4, r0, #16
 8001496:	f101 0310 	add.w	r3, r1, #16
 800149a:	4635      	mov	r5, r6
  {
    /* C = A * scale */
    /* Scale the input and then store the results in the destination buffer. */
    /* read input samples from source */
    in1 = *pSrc;
 800149c:	ed14 6a04 	vldr	s12, [r4, #-16]
    in2 = *(pSrc + 1);
 80014a0:	ed54 6a03 	vldr	s13, [r4, #-12]

    /* multiply with scaling factor */
    in1 = in1 * scale;

    /* read input sample from source */
    in3 = *(pSrc + 2);
 80014a4:	ed14 7a02 	vldr	s14, [r4, #-8]

    /* multiply with scaling factor */
    in2 = in2 * scale;

    /* read input sample from source */
    in4 = *(pSrc + 3);
 80014a8:	ed54 7a01 	vldr	s15, [r4, #-4]
    /* read input samples from source */
    in1 = *pSrc;
    in2 = *(pSrc + 1);

    /* multiply with scaling factor */
    in1 = in1 * scale;
 80014ac:	ee26 6a00 	vmul.f32	s12, s12, s0

    /* read input sample from source */
    in3 = *(pSrc + 2);

    /* multiply with scaling factor */
    in2 = in2 * scale;
 80014b0:	ee66 6a80 	vmul.f32	s13, s13, s0

    /* read input sample from source */
    in4 = *(pSrc + 3);

    /* multiply with scaling factor */
    in3 = in3 * scale;
 80014b4:	ee27 7a00 	vmul.f32	s14, s14, s0
    in4 = in4 * scale;
 80014b8:	ee67 7a80 	vmul.f32	s15, s15, s0
  /*loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.        
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 80014bc:	3d01      	subs	r5, #1

    /* multiply with scaling factor */
    in3 = in3 * scale;
    in4 = in4 * scale;
    /* store the result to destination */
    *pDst = in1;
 80014be:	ed03 6a04 	vstr	s12, [r3, #-16]
    *(pDst + 1) = in2;
 80014c2:	ed43 6a03 	vstr	s13, [r3, #-12]
    *(pDst + 2) = in3;
 80014c6:	ed03 7a02 	vstr	s14, [r3, #-8]
    *(pDst + 3) = in4;
 80014ca:	ed43 7a01 	vstr	s15, [r3, #-4]
 80014ce:	f104 0410 	add.w	r4, r4, #16
 80014d2:	f103 0310 	add.w	r3, r3, #16
  /*loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.        
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 80014d6:	d1e1      	bne.n	800149c <arm_scale_f32+0x10>
 80014d8:	0136      	lsls	r6, r6, #4
 80014da:	4430      	add	r0, r6
 80014dc:	4431      	add	r1, r6
  /* Initialize blkCnt with number of samples */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 80014de:	f012 0203 	ands.w	r2, r2, #3
 80014e2:	d007      	beq.n	80014f4 <arm_scale_f32+0x68>
  {
    /* C = A * scale */
    /* Scale the input and then store the result in the destination buffer. */
    *pDst++ = (*pSrc++) * scale;
 80014e4:	ecf0 7a01 	vldmia	r0!, {s15}
 80014e8:	ee67 7a80 	vmul.f32	s15, s15, s0
  /* Initialize blkCnt with number of samples */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 80014ec:	3a01      	subs	r2, #1
  {
    /* C = A * scale */
    /* Scale the input and then store the result in the destination buffer. */
    *pDst++ = (*pSrc++) * scale;
 80014ee:	ece1 7a01 	vstmia	r1!, {s15}
  /* Initialize blkCnt with number of samples */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 80014f2:	d1f7      	bne.n	80014e4 <arm_scale_f32+0x58>
    *pDst++ = (*pSrc++) * scale;

    /* Decrement the loop counter */
    blkCnt--;
  }
}
 80014f4:	bc70      	pop	{r4, r5, r6}
 80014f6:	4770      	bx	lr

080014f8 <arm_fir_f32>:
void arm_fir_f32(
const arm_fir_instance_f32 * S,
float32_t * pSrc,
float32_t * pDst,
uint32_t blockSize)
{
 80014f8:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80014fc:	ed2d 8b10 	vpush	{d8-d15}
   float32_t *pCoeffs = S->pCoeffs;               /* Coefficient pointer */
   float32_t *pStateCurnt;                        /* Points to the current sample of the state */
   float32_t *px, *pb;                            /* Temporary pointers for state and coefficient buffers */
   float32_t acc0, acc1, acc2, acc3, acc4, acc5, acc6, acc7;     /* Accumulators */
   float32_t x0, x1, x2, x3, x4, x5, x6, x7, c0;  /* Temporary variables to hold state and coefficient values */
   uint32_t numTaps = S->numTaps;                 /* Number of filter coefficients in the filter */
 8001500:	f8b0 e000 	ldrh.w	lr, [r0]
const arm_fir_instance_f32 * S,
float32_t * pSrc,
float32_t * pDst,
uint32_t blockSize)
{
   float32_t *pState = S->pState;                 /* State pointer */
 8001504:	6844      	ldr	r4, [r0, #4]
   float32_t *pCoeffs = S->pCoeffs;               /* Coefficient pointer */
 8001506:	f8d0 8008 	ldr.w	r8, [r0, #8]
void arm_fir_f32(
const arm_fir_instance_f32 * S,
float32_t * pSrc,
float32_t * pDst,
uint32_t blockSize)
{
 800150a:	b087      	sub	sp, #28
 800150c:	461d      	mov	r5, r3
 800150e:	9304      	str	r3, [sp, #16]
   uint32_t i, tapCnt, blkCnt;                    /* Loop counters */
   float32_t p0,p1,p2,p3,p4,p5,p6,p7;             /* Temporary product values */

   /* S->pState points to state array which contains previous frame (numTaps - 1) samples */
   /* pStateCurnt points to the location where the new input data should be written */
   pStateCurnt = &(S->pState[(numTaps - 1u)]);
 8001510:	f10e 4380 	add.w	r3, lr, #1073741824	; 0x40000000
 8001514:	3b01      	subs	r3, #1
 8001516:	eb04 0083 	add.w	r0, r4, r3, lsl #2
    */
   blkCnt = blockSize >> 3;

   /* First part of the processing with loop unrolling.  Compute 8 outputs at a time.  
   ** a second loop below computes the remaining 1 to 7 samples. */
   while(blkCnt > 0u)
 800151a:	08eb      	lsrs	r3, r5, #3
const arm_fir_instance_f32 * S,
float32_t * pSrc,
float32_t * pDst,
uint32_t blockSize)
{
   float32_t *pState = S->pState;                 /* State pointer */
 800151c:	9400      	str	r4, [sp, #0]
   uint32_t i, tapCnt, blkCnt;                    /* Loop counters */
   float32_t p0,p1,p2,p3,p4,p5,p6,p7;             /* Temporary product values */

   /* S->pState points to state array which contains previous frame (numTaps - 1) samples */
   /* pStateCurnt points to the location where the new input data should be written */
   pStateCurnt = &(S->pState[(numTaps - 1u)]);
 800151e:	9001      	str	r0, [sp, #4]
    */
   blkCnt = blockSize >> 3;

   /* First part of the processing with loop unrolling.  Compute 8 outputs at a time.  
   ** a second loop below computes the remaining 1 to 7 samples. */
   while(blkCnt > 0u)
 8001520:	9302      	str	r3, [sp, #8]
 8001522:	f000 824b 	beq.w	80019bc <arm_fir_f32+0x4c4>
 8001526:	ea4f 09de 	mov.w	r9, lr, lsr #3
 800152a:	469c      	mov	ip, r3
 800152c:	ea4f 1349 	mov.w	r3, r9, lsl #5
 8001530:	eb08 0503 	add.w	r5, r8, r3
 8001534:	9503      	str	r5, [sp, #12]
 8001536:	1f1e      	subs	r6, r3, #4
 8001538:	f00e 0a07 	and.w	sl, lr, #7
 800153c:	4633      	mov	r3, r6
 800153e:	f8cd e014 	str.w	lr, [sp, #20]
 8001542:	f101 0620 	add.w	r6, r1, #32
 8001546:	f8dd e00c 	ldr.w	lr, [sp, #12]
 800154a:	9103      	str	r1, [sp, #12]
 800154c:	4611      	mov	r1, r2
 800154e:	f100 0720 	add.w	r7, r0, #32
 8001552:	f102 0520 	add.w	r5, r2, #32
 8001556:	4648      	mov	r0, r9
 8001558:	4652      	mov	r2, sl
 800155a:	3420      	adds	r4, #32
 800155c:	4699      	mov	r9, r3
 800155e:	468a      	mov	sl, r1
   {
      /* Copy four new input samples into the state buffer */
      *pStateCurnt++ = *pSrc++;
 8001560:	f856 3c20 	ldr.w	r3, [r6, #-32]
 8001564:	f847 3c20 	str.w	r3, [r7, #-32]
      *pStateCurnt++ = *pSrc++;
 8001568:	f856 3c1c 	ldr.w	r3, [r6, #-28]
 800156c:	f847 3c1c 	str.w	r3, [r7, #-28]
      *pStateCurnt++ = *pSrc++;
 8001570:	f856 3c18 	ldr.w	r3, [r6, #-24]
 8001574:	f847 3c18 	str.w	r3, [r7, #-24]
      *pStateCurnt++ = *pSrc++;
 8001578:	f856 3c14 	ldr.w	r3, [r6, #-20]
 800157c:	f847 3c14 	str.w	r3, [r7, #-20]
      pb = (pCoeffs);		
   
      /* This is separated from the others to avoid 
       * a call to __aeabi_memmove which would be slower
       */
      *pStateCurnt++ = *pSrc++;
 8001580:	f856 3c10 	ldr.w	r3, [r6, #-16]
 8001584:	f847 3c10 	str.w	r3, [r7, #-16]
      *pStateCurnt++ = *pSrc++;
 8001588:	f856 3c0c 	ldr.w	r3, [r6, #-12]
 800158c:	f847 3c0c 	str.w	r3, [r7, #-12]
      *pStateCurnt++ = *pSrc++;
 8001590:	f856 3c08 	ldr.w	r3, [r6, #-8]
 8001594:	f847 3c08 	str.w	r3, [r7, #-8]
      /* Loop unrolling.  Process 8 taps at a time. */
      tapCnt = numTaps >> 3u;
      
      /* Loop over the number of taps.  Unroll by a factor of 8.  
       ** Repeat until we've computed numTaps-8 coefficients. */
      while(tapCnt > 0u)
 8001598:	eddf 3af1 	vldr	s7, [pc, #964]	; 8001960 <arm_fir_f32+0x468>
       * a call to __aeabi_memmove which would be slower
       */
      *pStateCurnt++ = *pSrc++;
      *pStateCurnt++ = *pSrc++;
      *pStateCurnt++ = *pSrc++;
      *pStateCurnt++ = *pSrc++;
 800159c:	f856 3c04 	ldr.w	r3, [r6, #-4]
 80015a0:	f847 3c04 	str.w	r3, [r7, #-4]

      /* Read the first seven samples from the state buffer:  x[n-numTaps], x[n-numTaps-1], x[n-numTaps-2] */
      x0 = *px++;
 80015a4:	ed14 3a08 	vldr	s6, [r4, #-32]	; 0xffffffe0
      x1 = *px++;
 80015a8:	ed54 2a07 	vldr	s5, [r4, #-28]	; 0xffffffe4
      x2 = *px++;
 80015ac:	ed14 2a06 	vldr	s4, [r4, #-24]	; 0xffffffe8
      x3 = *px++;
 80015b0:	ed54 1a05 	vldr	s3, [r4, #-20]	; 0xffffffec
      x4 = *px++;
 80015b4:	ed14 1a04 	vldr	s2, [r4, #-16]
      x5 = *px++;
 80015b8:	ed54 0a03 	vldr	s1, [r4, #-12]
      x6 = *px++;
 80015bc:	ed14 0a02 	vldr	s0, [r4, #-8]
 80015c0:	1f21      	subs	r1, r4, #4
      /* Loop unrolling.  Process 8 taps at a time. */
      tapCnt = numTaps >> 3u;
      
      /* Loop over the number of taps.  Unroll by a factor of 8.  
       ** Repeat until we've computed numTaps-8 coefficients. */
      while(tapCnt > 0u)
 80015c2:	eef0 8a63 	vmov.f32	s17, s7
 80015c6:	eef0 9a63 	vmov.f32	s19, s7
 80015ca:	eef0 aa63 	vmov.f32	s21, s7
 80015ce:	eef0 ba63 	vmov.f32	s23, s7
 80015d2:	eeb0 ca63 	vmov.f32	s24, s7
 80015d6:	eef0 ca63 	vmov.f32	s25, s7
 80015da:	eeb0 da63 	vmov.f32	s26, s7
 80015de:	2800      	cmp	r0, #0
 80015e0:	f000 81ea 	beq.w	80019b8 <arm_fir_f32+0x4c0>
 80015e4:	f108 0120 	add.w	r1, r8, #32
 80015e8:	f104 031c 	add.w	r3, r4, #28
 80015ec:	4683      	mov	fp, r0
      {
         /* Read the b[numTaps-1] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-3] sample */
         x7 = *(px++);
 80015ee:	ed13 8a08 	vldr	s16, [r3, #-32]	; 0xffffffe0
      /* Loop over the number of taps.  Unroll by a factor of 8.  
       ** Repeat until we've computed numTaps-8 coefficients. */
      while(tapCnt > 0u)
      {
         /* Read the b[numTaps-1] coefficient */
         c0 = *(pb++);
 80015f2:	ed11 4a08 	vldr	s8, [r1, #-32]	; 0xffffffe0

         /* acc3 +=  b[numTaps-1] * x[n-numTaps-7] */
         p7 = x7 * c0;
         
         /* Read the b[numTaps-2] coefficient */
         c0 = *(pb++);
 80015f6:	ed51 4a07 	vldr	s9, [r1, #-28]	; 0xffffffe4
         p5 = x6 * c0;   
         p6 = x7 * c0;   
         p7 = x0 * c0;   
         
         /* Read the b[numTaps-3] coefficient */
         c0 = *(pb++);
 80015fa:	ed11 5a06 	vldr	s10, [r1, #-24]	; 0xffffffe8
         p5 = x7 * c0;   
         p6 = x0 * c0;   
         p7 = x1 * c0;   

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);
 80015fe:	ed51 5a05 	vldr	s11, [r1, #-20]	; 0xffffffec
         p5 = x0 * c0;   
         p6 = x1 * c0;   
         p7 = x2 * c0;   

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);
 8001602:	ed11 6a04 	vldr	s12, [r1, #-16]
         p5 = x1 * c0;   
         p6 = x2 * c0;   
         p7 = x3 * c0;   

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);
 8001606:	ed51 6a03 	vldr	s13, [r1, #-12]
         p5 = x2 * c0;   
         p6 = x3 * c0;   
         p7 = x4 * c0;   

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);
 800160a:	ed11 7a02 	vldr	s14, [r1, #-8]
         p5 = x3 * c0;   
         p6 = x4 * c0;   
         p7 = x5 * c0;   

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);
 800160e:	ed51 7a01 	vldr	s15, [r1, #-4]

         /* Read x[n-numTaps-3] sample */
         x7 = *(px++);

         /* acc0 +=  b[numTaps-1] * x[n-numTaps] */
         p0 = x0 * c0;
 8001612:	ee23 fa04 	vmul.f32	s30, s6, s8

         /* acc1 +=  b[numTaps-1] * x[n-numTaps-1] */
         p1 = x1 * c0;
 8001616:	ee62 ea84 	vmul.f32	s29, s5, s8
         
         /* Read the b[numTaps-2] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-4] sample */
         x0 = *(px++);
 800161a:	ed13 3a07 	vldr	s6, [r3, #-28]	; 0xffffffe4

         /* acc1 +=  b[numTaps-1] * x[n-numTaps-1] */
         p1 = x1 * c0;

         /* acc2 +=  b[numTaps-1] * x[n-numTaps-2] */
         p2 = x2 * c0;
 800161e:	ee22 ea04 	vmul.f32	s28, s4, s8

         /* acc3 +=  b[numTaps-1] * x[n-numTaps-3] */
         p3 = x3 * c0;
 8001622:	ee61 da84 	vmul.f32	s27, s3, s8

         /* acc4 +=  b[numTaps-1] * x[n-numTaps-4] */
         p4 = x4 * c0;
 8001626:	ee21 ba04 	vmul.f32	s22, s2, s8

         /* acc1 +=  b[numTaps-1] * x[n-numTaps-5] */
         p5 = x5 * c0;
 800162a:	ee20 aa84 	vmul.f32	s20, s1, s8

         /* acc2 +=  b[numTaps-1] * x[n-numTaps-6] */
         p6 = x6 * c0;
 800162e:	ee20 9a04 	vmul.f32	s18, s0, s8

         /* acc3 +=  b[numTaps-1] * x[n-numTaps-7] */
         p7 = x7 * c0;
 8001632:	ee28 4a04 	vmul.f32	s8, s16, s8
         acc7 += p7;


         /* Perform the multiply-accumulate */
         p0 = x1 * c0;
         p1 = x2 * c0;   
 8001636:	ee62 fa24 	vmul.f32	s31, s4, s9
         /* Read x[n-numTaps-4] sample */
         x0 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
 800163a:	ee3c ca0e 	vadd.f32	s24, s24, s28
         acc3 += p3;
 800163e:	ee7b baad 	vadd.f32	s23, s23, s27


         /* Perform the multiply-accumulate */
         p0 = x1 * c0;
         p1 = x2 * c0;   
         p2 = x3 * c0;   
 8001642:	ee21 eaa4 	vmul.f32	s28, s3, s9
         p3 = x4 * c0;   
 8001646:	ee61 da24 	vmul.f32	s27, s2, s9
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
 800164a:	ee7a aa8b 	vadd.f32	s21, s21, s22
         acc5 += p5;
 800164e:	ee79 9a8a 	vadd.f32	s19, s19, s20
         /* Perform the multiply-accumulate */
         p0 = x1 * c0;
         p1 = x2 * c0;   
         p2 = x3 * c0;   
         p3 = x4 * c0;   
         p4 = x5 * c0;   
 8001652:	ee20 baa4 	vmul.f32	s22, s1, s9
         p5 = x6 * c0;   
 8001656:	ee20 aa24 	vmul.f32	s20, s0, s9
         c0 = *(pb++);

         /* Read x[n-numTaps-4] sample */
         x0 = *(px++);
         
         acc0 += p0;
 800165a:	ee3d da0f 	vadd.f32	s26, s26, s30
         acc1 += p1;
 800165e:	ee7c caae 	vadd.f32	s25, s25, s29
         acc6 += p6;
         acc7 += p7;


         /* Perform the multiply-accumulate */
         p0 = x1 * c0;
 8001662:	ee22 faa4 	vmul.f32	s30, s5, s9
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
         acc6 += p6;
 8001666:	ee78 8a89 	vadd.f32	s17, s17, s18
         
         /* Read the b[numTaps-3] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-5] sample */
         x1 = *(px++);
 800166a:	ed53 2a06 	vldr	s5, [r3, #-24]	; 0xffffffe8
         p1 = x2 * c0;   
         p2 = x3 * c0;   
         p3 = x4 * c0;   
         p4 = x5 * c0;   
         p5 = x6 * c0;   
         p6 = x7 * c0;   
 800166e:	ee28 9a24 	vmul.f32	s18, s16, s9
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;
 8001672:	ee73 3a84 	vadd.f32	s7, s7, s8
         p2 = x3 * c0;   
         p3 = x4 * c0;   
         p4 = x5 * c0;   
         p5 = x6 * c0;   
         p6 = x7 * c0;   
         p7 = x0 * c0;   
 8001676:	ee63 4a24 	vmul.f32	s9, s6, s9
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x2 * c0;
 800167a:	ee22 4a05 	vmul.f32	s8, s4, s10
         p1 = x3 * c0;   
         p2 = x4 * c0;   
 800167e:	ee61 ea05 	vmul.f32	s29, s2, s10

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x2 = *(px++);
 8001682:	ed13 2a05 	vldr	s4, [r3, #-20]	; 0xffffffec
         c0 = *(pb++);

         /* Read x[n-numTaps-5] sample */
         x1 = *(px++);
         
         acc0 += p0;
 8001686:	ee3d da0f 	vadd.f32	s26, s26, s30
         acc1 += p1;
 800168a:	ee7c caaf 	vadd.f32	s25, s25, s31

         /* Perform the multiply-accumulates */      
         p0 = x2 * c0;
         p1 = x3 * c0;   
         p2 = x4 * c0;   
         p3 = x5 * c0;   
 800168e:	ee20 fa85 	vmul.f32	s30, s1, s10
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x2 * c0;
         p1 = x3 * c0;   
 8001692:	ee61 fa85 	vmul.f32	s31, s3, s10
         /* Read x[n-numTaps-5] sample */
         x1 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
 8001696:	ee3c ea0e 	vadd.f32	s28, s24, s28
         acc3 += p3;
 800169a:	ee7b daad 	vadd.f32	s27, s23, s27
         acc4 += p4;
 800169e:	ee3a ba8b 	vadd.f32	s22, s21, s22
         /* Perform the multiply-accumulates */      
         p0 = x2 * c0;
         p1 = x3 * c0;   
         p2 = x4 * c0;   
         p3 = x5 * c0;   
         p4 = x6 * c0;   
 80016a2:	ee60 ba05 	vmul.f32	s23, s0, s10
         p5 = x7 * c0;   
 80016a6:	ee68 aa05 	vmul.f32	s21, s16, s10
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
 80016aa:	ee39 aa8a 	vadd.f32	s20, s19, s20
         acc6 += p6;
 80016ae:	ee78 8a89 	vadd.f32	s17, s17, s18
         acc7 += p7;
 80016b2:	ee73 3aa4 	vadd.f32	s7, s7, s9
         p1 = x3 * c0;   
         p2 = x4 * c0;   
         p3 = x5 * c0;   
         p4 = x6 * c0;   
         p5 = x7 * c0;   
         p6 = x0 * c0;   
 80016b6:	ee63 4a05 	vmul.f32	s9, s6, s10
         p7 = x1 * c0;   
 80016ba:	ee22 5a85 	vmul.f32	s10, s5, s10
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x3 * c0;
         p1 = x4 * c0;   
 80016be:	ee21 9a25 	vmul.f32	s18, s2, s11
         p2 = x5 * c0;   
         p3 = x6 * c0;   
 80016c2:	ee20 ca25 	vmul.f32	s24, s0, s11
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x2 = *(px++);
         
         acc0 += p0;
 80016c6:	ee3d da04 	vadd.f32	s26, s26, s8
         acc1 += p1;
 80016ca:	ee7c caaf 	vadd.f32	s25, s25, s31
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x3 * c0;
 80016ce:	ee21 4aa5 	vmul.f32	s8, s3, s11
         /* Read x[n-numTaps-6] sample */
         x2 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
 80016d2:	ee3e ea2e 	vadd.f32	s28, s28, s29

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x3 = *(px++);
 80016d6:	ed53 1a04 	vldr	s3, [r3, #-16]
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x3 * c0;
         p1 = x4 * c0;   
         p2 = x5 * c0;   
 80016da:	ee60 eaa5 	vmul.f32	s29, s1, s11
         x2 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
 80016de:	ee7d da8f 	vadd.f32	s27, s27, s30
         acc4 += p4;
 80016e2:	ee3b ba2b 	vadd.f32	s22, s22, s23
         acc5 += p5;
 80016e6:	ee3a aa2a 	vadd.f32	s20, s20, s21
         /* Perform the multiply-accumulates */      
         p0 = x3 * c0;
         p1 = x4 * c0;   
         p2 = x5 * c0;   
         p3 = x6 * c0;   
         p4 = x7 * c0;   
 80016ea:	ee68 ba25 	vmul.f32	s23, s16, s11
         p5 = x0 * c0;   
 80016ee:	ee63 aa25 	vmul.f32	s21, s6, s11
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
         acc6 += p6;
 80016f2:	ee78 8aa4 	vadd.f32	s17, s17, s9
         acc7 += p7;
 80016f6:	ee73 3a85 	vadd.f32	s7, s7, s10
         p1 = x4 * c0;   
         p2 = x5 * c0;   
         p3 = x6 * c0;   
         p4 = x7 * c0;   
         p5 = x0 * c0;   
         p6 = x1 * c0;   
 80016fa:	ee22 5aa5 	vmul.f32	s10, s5, s11
         p7 = x2 * c0;   
 80016fe:	ee62 5a25 	vmul.f32	s11, s4, s11
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x4 * c0;
 8001702:	ee61 4a06 	vmul.f32	s9, s2, s12
         p1 = x5 * c0;   
 8001706:	ee60 9a86 	vmul.f32	s19, s1, s12
         p2 = x6 * c0;   
 800170a:	ee60 fa06 	vmul.f32	s31, s0, s12

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x4 = *(px++);
 800170e:	ed13 1a03 	vldr	s2, [r3, #-12]
         /* Perform the multiply-accumulates */      
         p0 = x4 * c0;
         p1 = x5 * c0;   
         p2 = x6 * c0;   
         p3 = x7 * c0;   
         p4 = x0 * c0;   
 8001712:	ee23 fa06 	vmul.f32	s30, s6, s12
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x3 = *(px++);
         
         acc0 += p0;
 8001716:	ee3d da04 	vadd.f32	s26, s26, s8
         acc1 += p1;
 800171a:	ee7c ca89 	vadd.f32	s25, s25, s18
         acc2 += p2;
 800171e:	ee3e ea2e 	vadd.f32	s28, s28, s29

         /* Perform the multiply-accumulates */      
         p0 = x4 * c0;
         p1 = x5 * c0;   
         p2 = x6 * c0;   
         p3 = x7 * c0;   
 8001722:	ee28 9a06 	vmul.f32	s18, s16, s12
         x3 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
 8001726:	ee7d da8c 	vadd.f32	s27, s27, s24
         acc4 += p4;
 800172a:	ee3b ba2b 	vadd.f32	s22, s22, s23
         p0 = x4 * c0;
         p1 = x5 * c0;   
         p2 = x6 * c0;   
         p3 = x7 * c0;   
         p4 = x0 * c0;   
         p5 = x1 * c0;   
 800172e:	ee22 ca86 	vmul.f32	s24, s5, s12
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
 8001732:	ee3a aa2a 	vadd.f32	s20, s20, s21
         acc6 += p6;
 8001736:	ee78 8a85 	vadd.f32	s17, s17, s10
         acc7 += p7;
 800173a:	ee73 3aa5 	vadd.f32	s7, s7, s11
         p1 = x5 * c0;   
         p2 = x6 * c0;   
         p3 = x7 * c0;   
         p4 = x0 * c0;   
         p5 = x1 * c0;   
         p6 = x2 * c0;   
 800173e:	ee62 5a06 	vmul.f32	s11, s4, s12
         p7 = x3 * c0;   
 8001742:	ee21 6a86 	vmul.f32	s12, s3, s12
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x5 * c0;
         p1 = x6 * c0;   
 8001746:	ee20 4a26 	vmul.f32	s8, s0, s13
         p2 = x7 * c0;   
         p3 = x0 * c0;   
 800174a:	ee63 ea26 	vmul.f32	s29, s6, s13
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
 800174e:	ee3a aa0c 	vadd.f32	s20, s20, s24
         p0 = x5 * c0;
         p1 = x6 * c0;   
         p2 = x7 * c0;   
         p3 = x0 * c0;   
         p4 = x1 * c0;   
         p5 = x2 * c0;   
 8001752:	ee22 5a26 	vmul.f32	s10, s4, s13
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x4 = *(px++);
         
         acc0 += p0;
 8001756:	ee3d da24 	vadd.f32	s26, s26, s9
         acc1 += p1;
 800175a:	ee7c caa9 	vadd.f32	s25, s25, s19
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x5 * c0;
 800175e:	ee60 4aa6 	vmul.f32	s9, s1, s13
         /* Read x[n-numTaps-6] sample */
         x4 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
 8001762:	ee3e ea2f 	vadd.f32	s28, s28, s31

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x5 = *(px++);
 8001766:	ed53 0a02 	vldr	s1, [r3, #-8]
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x5 * c0;
         p1 = x6 * c0;   
         p2 = x7 * c0;   
 800176a:	ee68 fa26 	vmul.f32	s31, s16, s13
         x4 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
 800176e:	ee7d da89 	vadd.f32	s27, s27, s18
         acc4 += p4;
 8001772:	ee3b ba0f 	vadd.f32	s22, s22, s30
         acc5 += p5;
         acc6 += p6;
 8001776:	ee78 8aa5 	vadd.f32	s17, s17, s11
         /* Perform the multiply-accumulates */      
         p0 = x5 * c0;
         p1 = x6 * c0;   
         p2 = x7 * c0;   
         p3 = x0 * c0;   
         p4 = x1 * c0;   
 800177a:	ee22 faa6 	vmul.f32	s30, s5, s13
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;
 800177e:	ee73 3a86 	vadd.f32	s7, s7, s12
         p1 = x6 * c0;   
         p2 = x7 * c0;   
         p3 = x0 * c0;   
         p4 = x1 * c0;   
         p5 = x2 * c0;   
         p6 = x3 * c0;   
 8001782:	ee21 6aa6 	vmul.f32	s12, s3, s13
         p7 = x4 * c0;   
 8001786:	ee61 6a26 	vmul.f32	s13, s2, s13
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x6 * c0;
 800178a:	ee60 aa07 	vmul.f32	s21, s0, s14
         p1 = x7 * c0;   
 800178e:	ee28 ca07 	vmul.f32	s24, s16, s14
         p2 = x0 * c0;   
 8001792:	ee63 ba07 	vmul.f32	s23, s6, s14
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
 8001796:	ee7a 9a05 	vadd.f32	s19, s20, s10
         acc6 += p6;
 800179a:	ee78 8a86 	vadd.f32	s17, s17, s12
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x5 = *(px++);
         
         acc0 += p0;
 800179e:	ee3d da24 	vadd.f32	s26, s26, s9
         acc1 += p1;
 80017a2:	ee7c ca84 	vadd.f32	s25, s25, s8
         acc2 += p2;
         acc3 += p3;
 80017a6:	ee7d daae 	vadd.f32	s27, s27, s29
         /* Perform the multiply-accumulates */      
         p0 = x6 * c0;
         p1 = x7 * c0;   
         p2 = x0 * c0;   
         p3 = x1 * c0;   
         p4 = x2 * c0;   
 80017aa:	ee22 9a07 	vmul.f32	s18, s4, s14

         /* Perform the multiply-accumulates */      
         p0 = x6 * c0;
         p1 = x7 * c0;   
         p2 = x0 * c0;   
         p3 = x1 * c0;   
 80017ae:	ee62 ea87 	vmul.f32	s29, s5, s14
         p4 = x2 * c0;   
         p5 = x3 * c0;   
 80017b2:	ee61 5a87 	vmul.f32	s11, s3, s14
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;
 80017b6:	ee73 3aa6 	vadd.f32	s7, s7, s13

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x6 = *(px++);
 80017ba:	ed13 0a01 	vldr	s0, [r3, #-4]
         p1 = x7 * c0;   
         p2 = x0 * c0;   
         p3 = x1 * c0;   
         p4 = x2 * c0;   
         p5 = x3 * c0;   
         p6 = x4 * c0;   
 80017be:	ee61 6a07 	vmul.f32	s13, s2, s14
         /* Read x[n-numTaps-6] sample */
         x5 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
 80017c2:	ee3e ea2f 	vadd.f32	s28, s28, s31
         acc3 += p3;
         acc4 += p4;
 80017c6:	ee3b ba0f 	vadd.f32	s22, s22, s30
         p2 = x0 * c0;   
         p3 = x1 * c0;   
         p4 = x2 * c0;   
         p5 = x3 * c0;   
         p6 = x4 * c0;   
         p7 = x5 * c0;   
 80017ca:	ee20 7a87 	vmul.f32	s14, s1, s14
         acc5 += p5;
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x7 * c0;
 80017ce:	ee68 4a27 	vmul.f32	s9, s16, s15
         c0 = *(pb++);

         /* Read x[n-numTaps-6] sample */
         x6 = *(px++);
         
         acc0 += p0;
 80017d2:	ee3d da2a 	vadd.f32	s26, s26, s21
         acc1 += p1;
 80017d6:	ee7c ca8c 	vadd.f32	s25, s25, s24
         acc6 += p6;
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x7 * c0;
         p1 = x0 * c0;   
 80017da:	ee23 8a27 	vmul.f32	s16, s6, s15
         /* Read x[n-numTaps-6] sample */
         x6 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
 80017de:	ee3e ea2b 	vadd.f32	s28, s28, s23
         acc7 += p7;

         /* Perform the multiply-accumulates */      
         p0 = x7 * c0;
         p1 = x0 * c0;   
         p2 = x1 * c0;   
 80017e2:	ee22 4aa7 	vmul.f32	s8, s5, s15
         p3 = x2 * c0;   
 80017e6:	ee22 aa27 	vmul.f32	s20, s4, s15
         p4 = x3 * c0;   
 80017ea:	ee21 5aa7 	vmul.f32	s10, s3, s15
         p5 = x4 * c0;   
 80017ee:	ee21 6a27 	vmul.f32	s12, s2, s15
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
         acc5 += p5;
         acc6 += p6;
 80017f2:	ee78 6aa6 	vadd.f32	s13, s17, s13
         x6 = *(px++);
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
 80017f6:	ee7d daae 	vadd.f32	s27, s27, s29
         p1 = x0 * c0;   
         p2 = x1 * c0;   
         p3 = x2 * c0;   
         p4 = x3 * c0;   
         p5 = x4 * c0;   
         p6 = x5 * c0;   
 80017fa:	ee60 8aa7 	vmul.f32	s17, s1, s15
         
         acc0 += p0;
         acc1 += p1;
         acc2 += p2;
         acc3 += p3;
         acc4 += p4;
 80017fe:	ee3b ba09 	vadd.f32	s22, s22, s18
         acc5 += p5;
 8001802:	ee79 9aa5 	vadd.f32	s19, s19, s11
         acc6 += p6;
         acc7 += p7;
 8001806:	ee73 3a87 	vadd.f32	s7, s7, s14
         p2 = x1 * c0;   
         p3 = x2 * c0;   
         p4 = x3 * c0;   
         p5 = x4 * c0;   
         p6 = x5 * c0;   
         p7 = x6 * c0;   
 800180a:	ee60 7a27 	vmul.f32	s15, s0, s15
      /* Loop unrolling.  Process 8 taps at a time. */
      tapCnt = numTaps >> 3u;
      
      /* Loop over the number of taps.  Unroll by a factor of 8.  
       ** Repeat until we've computed numTaps-8 coefficients. */
      while(tapCnt > 0u)
 800180e:	f1bb 0b01 	subs.w	fp, fp, #1
 8001812:	f101 0120 	add.w	r1, r1, #32
         p6 = x5 * c0;   
         p7 = x6 * c0;   

         tapCnt--;
         
         acc0 += p0;
 8001816:	ee3d da24 	vadd.f32	s26, s26, s9
         acc1 += p1;
 800181a:	ee7c ca88 	vadd.f32	s25, s25, s16
         acc2 += p2;
 800181e:	ee3e ca04 	vadd.f32	s24, s28, s8
         acc3 += p3;
 8001822:	ee7d ba8a 	vadd.f32	s23, s27, s20
         acc4 += p4;
 8001826:	ee7b aa05 	vadd.f32	s21, s22, s10
         acc5 += p5;
 800182a:	ee79 9a86 	vadd.f32	s19, s19, s12
         acc6 += p6;
 800182e:	ee76 8aa8 	vadd.f32	s17, s13, s17
         acc7 += p7;
 8001832:	ee73 3aa7 	vadd.f32	s7, s7, s15
 8001836:	f103 0320 	add.w	r3, r3, #32
      /* Loop unrolling.  Process 8 taps at a time. */
      tapCnt = numTaps >> 3u;
      
      /* Loop over the number of taps.  Unroll by a factor of 8.  
       ** Repeat until we've computed numTaps-8 coefficients. */
      while(tapCnt > 0u)
 800183a:	f47f aed8 	bne.w	80015ee <arm_fir_f32+0xf6>
 800183e:	eb09 0104 	add.w	r1, r9, r4
         p5 = x3 * c0;   
         p6 = x4 * c0;   
         p7 = x5 * c0;   

         /* Read the b[numTaps-4] coefficient */
         c0 = *(pb++);
 8001842:	46f3      	mov	fp, lr
      }

      /* If the filter length is not a multiple of 8, compute the remaining filter taps */
      tapCnt = numTaps % 0x8u;

      while(tapCnt > 0u)
 8001844:	b3a2      	cbz	r2, 80018b0 <arm_fir_f32+0x3b8>
 8001846:	4613      	mov	r3, r2
      {
         /* Read coefficients */
         c0 = *(pb++);
 8001848:	ecbb 6a01 	vldmia	fp!, {s12}

         /* Fetch 1 state variable */
         x7 = *(px++);
 800184c:	ecf1 7a01 	vldmia	r1!, {s15}

         /* Perform the multiply-accumulates */      
         p0 = x0 * c0;
 8001850:	ee23 3a06 	vmul.f32	s6, s6, s12
         p1 = x1 * c0;   
 8001854:	ee22 4a86 	vmul.f32	s8, s5, s12
         p2 = x2 * c0;   
 8001858:	ee62 4a06 	vmul.f32	s9, s4, s12
         p3 = x3 * c0;   
 800185c:	ee21 5a86 	vmul.f32	s10, s3, s12
         p4 = x4 * c0;   
 8001860:	ee61 5a06 	vmul.f32	s11, s2, s12
         p5 = x5 * c0;   
 8001864:	ee60 6a86 	vmul.f32	s13, s1, s12
         p6 = x6 * c0;   
 8001868:	ee20 7a06 	vmul.f32	s14, s0, s12
         p7 = x7 * c0;   
 800186c:	ee27 6a86 	vmul.f32	s12, s15, s12
      }

      /* If the filter length is not a multiple of 8, compute the remaining filter taps */
      tapCnt = numTaps % 0x8u;

      while(tapCnt > 0u)
 8001870:	3b01      	subs	r3, #1
         x3 = x4;
         x4 = x5;
         x5 = x6;
         x6 = x7;
         
         acc0 += p0;
 8001872:	ee3d da03 	vadd.f32	s26, s26, s6
         acc1 += p1;
 8001876:	ee7c ca84 	vadd.f32	s25, s25, s8
 800187a:	eeb0 3a62 	vmov.f32	s6, s5
         acc2 += p2;
 800187e:	ee3c ca24 	vadd.f32	s24, s24, s9
 8001882:	eef0 2a42 	vmov.f32	s5, s4
         acc3 += p3;
 8001886:	ee7b ba85 	vadd.f32	s23, s23, s10
 800188a:	eeb0 2a61 	vmov.f32	s4, s3
         acc4 += p4;
 800188e:	ee7a aaa5 	vadd.f32	s21, s21, s11
 8001892:	eef0 1a41 	vmov.f32	s3, s2
         acc5 += p5;
 8001896:	ee79 9aa6 	vadd.f32	s19, s19, s13
 800189a:	eeb0 1a60 	vmov.f32	s2, s1
         acc6 += p6;
 800189e:	ee78 8a87 	vadd.f32	s17, s17, s14
 80018a2:	eef0 0a40 	vmov.f32	s1, s0
         acc7 += p7;
 80018a6:	ee73 3a86 	vadd.f32	s7, s7, s12
         x1 = x2;
         x2 = x3;
         x3 = x4;
         x4 = x5;
         x5 = x6;
         x6 = x7;
 80018aa:	eeb0 0a67 	vmov.f32	s0, s15
      }

      /* If the filter length is not a multiple of 8, compute the remaining filter taps */
      tapCnt = numTaps % 0x8u;

      while(tapCnt > 0u)
 80018ae:	d1cb      	bne.n	8001848 <arm_fir_f32+0x350>
    */
   blkCnt = blockSize >> 3;

   /* First part of the processing with loop unrolling.  Compute 8 outputs at a time.  
   ** a second loop below computes the remaining 1 to 7 samples. */
   while(blkCnt > 0u)
 80018b0:	f1bc 0c01 	subs.w	ip, ip, #1

      /* Advance the state pointer by 8 to process the next group of 8 samples */
      pState = pState + 8;

      /* The results in the 8 accumulators, store in the destination buffer. */
      *pDst++ = acc0;
 80018b4:	ed05 da08 	vstr	s26, [r5, #-32]	; 0xffffffe0
      *pDst++ = acc1;
 80018b8:	ed45 ca07 	vstr	s25, [r5, #-28]	; 0xffffffe4
      *pDst++ = acc2;
 80018bc:	ed05 ca06 	vstr	s24, [r5, #-24]	; 0xffffffe8
      *pDst++ = acc3;
 80018c0:	ed45 ba05 	vstr	s23, [r5, #-20]	; 0xffffffec
      *pDst++ = acc4;
 80018c4:	ed45 aa04 	vstr	s21, [r5, #-16]
      *pDst++ = acc5;
 80018c8:	ed45 9a03 	vstr	s19, [r5, #-12]
      *pDst++ = acc6;
 80018cc:	ed45 8a02 	vstr	s17, [r5, #-8]
      *pDst++ = acc7;
 80018d0:	ed45 3a01 	vstr	s7, [r5, #-4]
 80018d4:	f107 0720 	add.w	r7, r7, #32
 80018d8:	f106 0620 	add.w	r6, r6, #32
 80018dc:	f104 0420 	add.w	r4, r4, #32
 80018e0:	f105 0520 	add.w	r5, r5, #32
    */
   blkCnt = blockSize >> 3;

   /* First part of the processing with loop unrolling.  Compute 8 outputs at a time.  
   ** a second loop below computes the remaining 1 to 7 samples. */
   while(blkCnt > 0u)
 80018e4:	f47f ae3c 	bne.w	8001560 <arm_fir_f32+0x68>
 80018e8:	9b02      	ldr	r3, [sp, #8]
 80018ea:	9903      	ldr	r1, [sp, #12]
 80018ec:	f8dd e014 	ldr.w	lr, [sp, #20]
 80018f0:	015d      	lsls	r5, r3, #5
 80018f2:	9b01      	ldr	r3, [sp, #4]
 80018f4:	442b      	add	r3, r5
 80018f6:	9301      	str	r3, [sp, #4]
 80018f8:	9b00      	ldr	r3, [sp, #0]
 80018fa:	4652      	mov	r2, sl
 80018fc:	4429      	add	r1, r5
 80018fe:	442a      	add	r2, r5
 8001900:	441d      	add	r5, r3

   /* If the blockSize is not a multiple of 8, compute any remaining output samples here.  
   ** No loop unrolling is used. */
   blkCnt = blockSize % 0x8u;

   while(blkCnt > 0u)
 8001902:	9b04      	ldr	r3, [sp, #16]
 8001904:	f013 0907 	ands.w	r9, r3, #7
 8001908:	d01e      	beq.n	8001948 <arm_fir_f32+0x450>
 800190a:	f8dd c004 	ldr.w	ip, [sp, #4]
 800190e:	464f      	mov	r7, r9
 8001910:	462e      	mov	r6, r5
   {
      /* Copy one sample at a time into state buffer */
      *pStateCurnt++ = *pSrc++;
 8001912:	f851 3b04 	ldr.w	r3, [r1], #4

      /* Set the accumulator to zero */
      acc0 = 0.0f;
 8001916:	eddf 6a12 	vldr	s13, [pc, #72]	; 8001960 <arm_fir_f32+0x468>
   blkCnt = blockSize % 0x8u;

   while(blkCnt > 0u)
   {
      /* Copy one sample at a time into state buffer */
      *pStateCurnt++ = *pSrc++;
 800191a:	f84c 3b04 	str.w	r3, [ip], #4

      /* Initialize state pointer */
      px = pState;

      /* Initialize Coefficient pointer */
      pb = (pCoeffs);
 800191e:	4644      	mov	r4, r8
   float32_t *pCoeffs = S->pCoeffs;               /* Coefficient pointer */
   float32_t *pStateCurnt;                        /* Points to the current sample of the state */
   float32_t *px, *pb;                            /* Temporary pointers for state and coefficient buffers */
   float32_t acc0, acc1, acc2, acc3, acc4, acc5, acc6, acc7;     /* Accumulators */
   float32_t x0, x1, x2, x3, x4, x5, x6, x7, c0;  /* Temporary variables to hold state and coefficient values */
   uint32_t numTaps = S->numTaps;                 /* Number of filter coefficients in the filter */
 8001920:	4673      	mov	r3, lr
   blkCnt = blockSize % 0x8u;

   while(blkCnt > 0u)
   {
      /* Copy one sample at a time into state buffer */
      *pStateCurnt++ = *pSrc++;
 8001922:	4630      	mov	r0, r6
      i = numTaps;

      /* Perform the multiply-accumulates */
      do
      {
         acc0 += *px++ * *pb++;
 8001924:	ecb0 7a01 	vldmia	r0!, {s14}
 8001928:	ecf4 7a01 	vldmia	r4!, {s15}
 800192c:	ee67 7a27 	vmul.f32	s15, s14, s15
         i--;

      } while(i > 0u);
 8001930:	3b01      	subs	r3, #1
      i = numTaps;

      /* Perform the multiply-accumulates */
      do
      {
         acc0 += *px++ * *pb++;
 8001932:	ee76 6aa7 	vadd.f32	s13, s13, s15
         i--;

      } while(i > 0u);
 8001936:	d1f5      	bne.n	8001924 <arm_fir_f32+0x42c>

   /* If the blockSize is not a multiple of 8, compute any remaining output samples here.  
   ** No loop unrolling is used. */
   blkCnt = blockSize % 0x8u;

   while(blkCnt > 0u)
 8001938:	3f01      	subs	r7, #1
         i--;

      } while(i > 0u);

      /* The result is store in the destination buffer. */
      *pDst++ = acc0;
 800193a:	ece2 6a01 	vstmia	r2!, {s13}

      /* Advance state pointer by 1 for the next sample */
      pState = pState + 1;
 800193e:	f106 0604 	add.w	r6, r6, #4

   /* If the blockSize is not a multiple of 8, compute any remaining output samples here.  
   ** No loop unrolling is used. */
   blkCnt = blockSize % 0x8u;

   while(blkCnt > 0u)
 8001942:	d1e6      	bne.n	8001912 <arm_fir_f32+0x41a>
 8001944:	eb05 0589 	add.w	r5, r5, r9, lsl #2
   ** This prepares the state buffer for the next function call. */

   /* Points to the start of the state buffer */
   pStateCurnt = S->pState;

   tapCnt = (numTaps - 1u) >> 2u;
 8001948:	f10e 3eff 	add.w	lr, lr, #4294967295	; 0xffffffff

   /* copy data */
   while(tapCnt > 0u)
 800194c:	ea5f 049e 	movs.w	r4, lr, lsr #2
 8001950:	d023      	beq.n	800199a <arm_fir_f32+0x4a2>
 8001952:	9b00      	ldr	r3, [sp, #0]
 8001954:	4621      	mov	r1, r4
 8001956:	f103 0210 	add.w	r2, r3, #16
 800195a:	f105 0310 	add.w	r3, r5, #16
 800195e:	e001      	b.n	8001964 <arm_fir_f32+0x46c>
 8001960:	00000000 	.word	0x00000000
   {
      *pStateCurnt++ = *pState++;
 8001964:	f853 0c10 	ldr.w	r0, [r3, #-16]
 8001968:	f842 0c10 	str.w	r0, [r2, #-16]
      *pStateCurnt++ = *pState++;
 800196c:	f853 0c0c 	ldr.w	r0, [r3, #-12]
 8001970:	f842 0c0c 	str.w	r0, [r2, #-12]
      *pStateCurnt++ = *pState++;
 8001974:	f853 0c08 	ldr.w	r0, [r3, #-8]
 8001978:	f842 0c08 	str.w	r0, [r2, #-8]
      *pStateCurnt++ = *pState++;
 800197c:	f853 0c04 	ldr.w	r0, [r3, #-4]
 8001980:	f842 0c04 	str.w	r0, [r2, #-4]
   pStateCurnt = S->pState;

   tapCnt = (numTaps - 1u) >> 2u;

   /* copy data */
   while(tapCnt > 0u)
 8001984:	3901      	subs	r1, #1
 8001986:	f103 0310 	add.w	r3, r3, #16
 800198a:	f102 0210 	add.w	r2, r2, #16
 800198e:	d1e9      	bne.n	8001964 <arm_fir_f32+0x46c>
 8001990:	9a00      	ldr	r2, [sp, #0]
 8001992:	0123      	lsls	r3, r4, #4
 8001994:	441a      	add	r2, r3
 8001996:	9200      	str	r2, [sp, #0]
 8001998:	441d      	add	r5, r3

   /* Calculate remaining number of copies */
   tapCnt = (numTaps - 1u) % 0x4u;

   /* Copy the remaining q31_t data */
   while(tapCnt > 0u)
 800199a:	f01e 0303 	ands.w	r3, lr, #3
 800199e:	d006      	beq.n	80019ae <arm_fir_f32+0x4b6>
 80019a0:	9a00      	ldr	r2, [sp, #0]
   {
      *pStateCurnt++ = *pState++;
 80019a2:	f855 1b04 	ldr.w	r1, [r5], #4
 80019a6:	f842 1b04 	str.w	r1, [r2], #4

   /* Calculate remaining number of copies */
   tapCnt = (numTaps - 1u) % 0x4u;

   /* Copy the remaining q31_t data */
   while(tapCnt > 0u)
 80019aa:	3b01      	subs	r3, #1
 80019ac:	d1f9      	bne.n	80019a2 <arm_fir_f32+0x4aa>
      *pStateCurnt++ = *pState++;

      /* Decrement the loop counter */
      tapCnt--;
   }
}
 80019ae:	b007      	add	sp, #28
 80019b0:	ecbd 8b10 	vpop	{d8-d15}
 80019b4:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

      /* Initialize state pointer */
      px = pState;

      /* Initialize coeff pointer */
      pb = (pCoeffs);		
 80019b8:	46c3      	mov	fp, r8
 80019ba:	e743      	b.n	8001844 <arm_fir_f32+0x34c>
 80019bc:	4625      	mov	r5, r4
 80019be:	e7a0      	b.n	8001902 <arm_fir_f32+0x40a>

080019c0 <arm_fir_init_f32>:
  arm_fir_instance_f32 * S,
  uint16_t numTaps,
  float32_t * pCoeffs,
  float32_t * pState,
  uint32_t blockSize)
{
 80019c0:	b570      	push	{r4, r5, r6, lr}
 80019c2:	9c04      	ldr	r4, [sp, #16]
  /* Assign filter taps */
  S->numTaps = numTaps;

  /* Assign coefficient pointer */
  S->pCoeffs = pCoeffs;
 80019c4:	6082      	str	r2, [r0, #8]
 80019c6:	f104 4480 	add.w	r4, r4, #1073741824	; 0x40000000
 80019ca:	3c01      	subs	r4, #1
  arm_fir_instance_f32 * S,
  uint16_t numTaps,
  float32_t * pCoeffs,
  float32_t * pState,
  uint32_t blockSize)
{
 80019cc:	4605      	mov	r5, r0

  /* Assign coefficient pointer */
  S->pCoeffs = pCoeffs;

  /* Clear state buffer and the size of state buffer is (blockSize + numTaps - 1) */
  memset(pState, 0, (numTaps + (blockSize - 1u)) * sizeof(float32_t));
 80019ce:	440c      	add	r4, r1
  float32_t * pCoeffs,
  float32_t * pState,
  uint32_t blockSize)
{
  /* Assign filter taps */
  S->numTaps = numTaps;
 80019d0:	8001      	strh	r1, [r0, #0]
  arm_fir_instance_f32 * S,
  uint16_t numTaps,
  float32_t * pCoeffs,
  float32_t * pState,
  uint32_t blockSize)
{
 80019d2:	461e      	mov	r6, r3

  /* Assign coefficient pointer */
  S->pCoeffs = pCoeffs;

  /* Clear state buffer and the size of state buffer is (blockSize + numTaps - 1) */
  memset(pState, 0, (numTaps + (blockSize - 1u)) * sizeof(float32_t));
 80019d4:	00a2      	lsls	r2, r4, #2
 80019d6:	4618      	mov	r0, r3
 80019d8:	2100      	movs	r1, #0
 80019da:	f000 ff51 	bl	8002880 <memset>

  /* Assign state pointer */
  S->pState = pState;
 80019de:	606e      	str	r6, [r5, #4]
 80019e0:	bd70      	pop	{r4, r5, r6, pc}
 80019e2:	bf00      	nop

080019e4 <arm_biquad_cascade_df2T_f32>:
void arm_biquad_cascade_df2T_f32(
const arm_biquad_cascade_df2T_instance_f32 * S,
float32_t * pSrc,
float32_t * pDst,
uint32_t blockSize)
{
 80019e4:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 80019e8:	b083      	sub	sp, #12
 80019ea:	4691      	mov	r9, r2
   float32_t *pCoeffs = S->pCoeffs;               /*  coefficient pointer       */
   float32_t acc1;                                /*  accumulator               */
   float32_t b0, b1, b2, a1, a2;                  /*  Filter coefficients       */
   float32_t Xn1;                                 /*  temporary input           */
   float32_t d1, d2;                              /*  state variables           */
   uint32_t sample, stage = S->numStages;         /*  loop counters             */
 80019ec:	7807      	ldrb	r7, [r0, #0]
      /*Reading the state values */
      d1 = pState[0];
      d2 = pState[1];

      /* Apply loop unrolling and compute 4 output values simultaneously. */
      sample = blockSize >> 2u;
 80019ee:	ea4f 0c93 	mov.w	ip, r3, lsr #2
 80019f2:	6885      	ldr	r5, [r0, #8]
 80019f4:	3514      	adds	r5, #20
 80019f6:	6846      	ldr	r6, [r0, #4]
 80019f8:	3608      	adds	r6, #8
 80019fa:	ea4f 1a0c 	mov.w	sl, ip, lsl #4

      /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.       
   ** a second loop below computes the remaining 1 to 3 samples. */
      while(sample > 0u) {
 80019fe:	4690      	mov	r8, r2
 8001a00:	f8cd c004 	str.w	ip, [sp, #4]

         pOut[0] = acc1;	
         pOut[1] = acc2;	
         pOut[2] = acc3;	
         pOut[3] = acc4;
		 pOut += 4;
 8001a04:	eb02 0b0a 	add.w	fp, r2, sl
				 
         sample--;	       
      }

      sample = blockSize & 0x3u;
 8001a08:	f003 0203 	and.w	r2, r3, #3

   /* Run the below code for Cortex-M4 and Cortex-M3 */
   do
   {
      /* Reading the coefficients */     
      b0 = *pCoeffs++;
 8001a0c:	ed55 3a05 	vldr	s7, [r5, #-20]	; 0xffffffec
      b1 = *pCoeffs++;
 8001a10:	ed15 3a04 	vldr	s6, [r5, #-16]
      b2 = *pCoeffs++;
 8001a14:	ed55 2a03 	vldr	s5, [r5, #-12]
      a1 = *pCoeffs++;
 8001a18:	ed15 2a02 	vldr	s4, [r5, #-8]
      a2 = *pCoeffs++;
 8001a1c:	ed55 1a01 	vldr	s3, [r5, #-4]
 8001a20:	4634      	mov	r4, r6
      

      /*Reading the state values */
      d1 = pState[0];
 8001a22:	ed56 5a02 	vldr	s11, [r6, #-8]
      d2 = pState[1];
 8001a26:	ed56 7a01 	vldr	s15, [r6, #-4]
      /* Apply loop unrolling and compute 4 output values simultaneously. */
      sample = blockSize >> 2u;

      /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.       
   ** a second loop below computes the remaining 1 to 3 samples. */
      while(sample > 0u) {
 8001a2a:	f1bc 0f00 	cmp.w	ip, #0
 8001a2e:	d065      	beq.n	8001afc <arm_biquad_cascade_df2T_f32+0x118>
 8001a30:	f101 0010 	add.w	r0, r1, #16
 8001a34:	f109 0310 	add.w	r3, r9, #16
 8001a38:	f8dd e004 	ldr.w	lr, [sp, #4]
         /* y[n] = b0 * x[n] + d1 */
         /* d1 = b1 * x[n] + a1 * y[n] + d2 */
         /* d2 = b2 * x[n] + a2 * y[n] */

         /* Read the four inputs */
         Xn1 = pIn[0];
 8001a3c:	ed50 4a04 	vldr	s9, [r0, #-16]
         Xn2 = pIn[1];
 8001a40:	ed10 4a03 	vldr	s8, [r0, #-12]
         Xn3 = pIn[2];
 8001a44:	ed10 5a02 	vldr	s10, [r0, #-8]
         Xn4 = pIn[3];
 8001a48:	ed50 6a01 	vldr	s13, [r0, #-4]
         pIn += 4;     

         p0 = b0 * Xn1; 
 8001a4c:	ee23 1aa4 	vmul.f32	s2, s7, s9
         p1 = b1 * Xn1;
         acc1 = p0 + d1;
 8001a50:	ee31 1a25 	vadd.f32	s2, s2, s11
         p0 = b0 * Xn2; 
 8001a54:	ee63 5a84 	vmul.f32	s11, s7, s8
         Xn3 = pIn[2];
         Xn4 = pIn[3];
         pIn += 4;     

         p0 = b0 * Xn1; 
         p1 = b1 * Xn1;
 8001a58:	ee23 7a24 	vmul.f32	s14, s6, s9
         acc1 = p0 + d1;
         p0 = b0 * Xn2; 
         p3 = a1 * acc1;
 8001a5c:	ee22 6a01 	vmul.f32	s12, s4, s2
         p2 = b2 * Xn1;
         A1 = p1 + p3;
 8001a60:	ee37 7a06 	vadd.f32	s14, s14, s12
         p4 = a2 * acc1;
         d1 = A1 + d2;
 8001a64:	ee77 7a27 	vadd.f32	s15, s14, s15
         d2 = p2 + p4;

         p1 = b1 * Xn2;
         acc2 = p0 + d1;
 8001a68:	ee35 7aa7 	vadd.f32	s14, s11, s15
         p0 = b0 * Xn3;	 
 8001a6c:	ee63 7a85 	vmul.f32	s15, s7, s10
         A1 = p1 + p3;
         p4 = a2 * acc1;
         d1 = A1 + d2;
         d2 = p2 + p4;

         p1 = b1 * Xn2;
 8001a70:	ee63 0a04 	vmul.f32	s1, s6, s8
         acc2 = p0 + d1;
         p0 = b0 * Xn3;	 
         p3 = a1 * acc2; 
 8001a74:	ee22 6a07 	vmul.f32	s12, s4, s14
         p2 = b2 * Xn2;                                 
         A1 = p1 + p3;
 8001a78:	ee70 0a86 	vadd.f32	s1, s1, s12
         p0 = b0 * Xn1; 
         p1 = b1 * Xn1;
         acc1 = p0 + d1;
         p0 = b0 * Xn2; 
         p3 = a1 * acc1;
         p2 = b2 * Xn1;
 8001a7c:	ee62 4aa4 	vmul.f32	s9, s5, s9
         A1 = p1 + p3;
         p4 = a2 * acc1;
 8001a80:	ee21 6a81 	vmul.f32	s12, s3, s2
         d1 = A1 + d2;
         d2 = p2 + p4;
 8001a84:	ee74 4a86 	vadd.f32	s9, s9, s12
         p0 = b0 * Xn3;	 
         p3 = a1 * acc2; 
         p2 = b2 * Xn2;                                 
         A1 = p1 + p3;
         p4 = a2 * acc2;
         d1 = A1 + d2;
 8001a88:	ee70 4aa4 	vadd.f32	s9, s1, s9
         d2 = p2 + p4;

         p1 = b1 * Xn3;
         acc3 = p0 + d1;
 8001a8c:	ee77 0aa4 	vadd.f32	s1, s15, s9
         p0 = b0 * Xn4;	
 8001a90:	ee63 7aa6 	vmul.f32	s15, s7, s13
         A1 = p1 + p3;
         p4 = a2 * acc2;
         d1 = A1 + d2;
         d2 = p2 + p4;

         p1 = b1 * Xn3;
 8001a94:	ee63 4a05 	vmul.f32	s9, s6, s10
         acc3 = p0 + d1;
         p0 = b0 * Xn4;	
         p3 = a1 * acc3;
 8001a98:	ee22 6a20 	vmul.f32	s12, s4, s1
         p2 = b2 * Xn3;
         A1 = p1 + p3;
 8001a9c:	ee74 4a86 	vadd.f32	s9, s9, s12

         p1 = b1 * Xn2;
         acc2 = p0 + d1;
         p0 = b0 * Xn3;	 
         p3 = a1 * acc2; 
         p2 = b2 * Xn2;                                 
 8001aa0:	ee22 4a84 	vmul.f32	s8, s5, s8
         A1 = p1 + p3;
         p4 = a2 * acc2;
 8001aa4:	ee21 6a87 	vmul.f32	s12, s3, s14
         d1 = A1 + d2;
         d2 = p2 + p4;
 8001aa8:	ee34 4a06 	vadd.f32	s8, s8, s12
         p0 = b0 * Xn4;	
         p3 = a1 * acc3;
         p2 = b2 * Xn3;
         A1 = p1 + p3;
         p4 = a2 * acc3;
         d1 = A1 + d2;
 8001aac:	ee34 4a84 	vadd.f32	s8, s9, s8
         d2 = p2 + p4;

         acc4 = p0 + d1;
 8001ab0:	ee77 4a84 	vadd.f32	s9, s15, s8
         p1 = b1 * Xn4;
 8001ab4:	ee63 5a26 	vmul.f32	s11, s6, s13
         p3 = a1 * acc4;
 8001ab8:	ee62 7a24 	vmul.f32	s15, s4, s9
         p2 = b2 * Xn4;
         A1 = p1 + p3;
 8001abc:	ee75 7aa7 	vadd.f32	s15, s11, s15

         p1 = b1 * Xn3;
         acc3 = p0 + d1;
         p0 = b0 * Xn4;	
         p3 = a1 * acc3;
         p2 = b2 * Xn3;
 8001ac0:	ee22 5a85 	vmul.f32	s10, s5, s10
         A1 = p1 + p3;
         p4 = a2 * acc3;
 8001ac4:	ee21 6aa0 	vmul.f32	s12, s3, s1
         d1 = A1 + d2;
         d2 = p2 + p4;
 8001ac8:	ee35 5a06 	vadd.f32	s10, s10, s12
         p1 = b1 * Xn4;
         p3 = a1 * acc4;
         p2 = b2 * Xn4;
         A1 = p1 + p3;
         p4 = a2 * acc4;
         d1 = A1 + d2;
 8001acc:	ee77 5a85 	vadd.f32	s11, s15, s10
         d2 = p2 + p4;

         acc4 = p0 + d1;
         p1 = b1 * Xn4;
         p3 = a1 * acc4;
         p2 = b2 * Xn4;
 8001ad0:	ee62 7aa6 	vmul.f32	s15, s5, s13
         A1 = p1 + p3;
         p4 = a2 * acc4;
 8001ad4:	ee21 6aa4 	vmul.f32	s12, s3, s9
         d1 = A1 + d2;
         d2 = p2 + p4;
 8001ad8:	ee77 7a86 	vadd.f32	s15, s15, s12

         pOut[0] = acc1;	
 8001adc:	ed03 1a04 	vstr	s2, [r3, #-16]
         pOut[1] = acc2;	
 8001ae0:	ed03 7a03 	vstr	s14, [r3, #-12]
         pOut[2] = acc3;	
 8001ae4:	ed43 0a02 	vstr	s1, [r3, #-8]
         pOut[3] = acc4;
 8001ae8:	ed43 4a01 	vstr	s9, [r3, #-4]
 8001aec:	3010      	adds	r0, #16
 8001aee:	3310      	adds	r3, #16
      /* Apply loop unrolling and compute 4 output values simultaneously. */
      sample = blockSize >> 2u;

      /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.       
   ** a second loop below computes the remaining 1 to 3 samples. */
      while(sample > 0u) {
 8001af0:	f1be 0e01 	subs.w	lr, lr, #1
 8001af4:	d1a2      	bne.n	8001a3c <arm_biquad_cascade_df2T_f32+0x58>
 8001af6:	4451      	add	r1, sl

         pOut[0] = acc1;	
         pOut[1] = acc2;	
         pOut[2] = acc3;	
         pOut[3] = acc4;
		 pOut += 4;
 8001af8:	4658      	mov	r0, fp
 8001afa:	e000      	b.n	8001afe <arm_biquad_cascade_df2T_f32+0x11a>
      /* Apply loop unrolling and compute 4 output values simultaneously. */
      sample = blockSize >> 2u;

      /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.       
   ** a second loop below computes the remaining 1 to 3 samples. */
      while(sample > 0u) {
 8001afc:	4640      	mov	r0, r8
		 pOut += 4;
				 
         sample--;	       
      }

      sample = blockSize & 0x3u;
 8001afe:	4613      	mov	r3, r2
      while(sample > 0u) {
 8001b00:	b1ba      	cbz	r2, 8001b32 <arm_biquad_cascade_df2T_f32+0x14e>
         Xn1 = *pIn++;
 8001b02:	ecb1 7a01 	vldmia	r1!, {s14}

         p0 = b0 * Xn1; 
 8001b06:	ee23 6a87 	vmul.f32	s12, s7, s14
         p1 = b1 * Xn1;
         acc1 = p0 + d1;
 8001b0a:	ee36 6a25 	vadd.f32	s12, s12, s11
      sample = blockSize & 0x3u;
      while(sample > 0u) {
         Xn1 = *pIn++;

         p0 = b0 * Xn1; 
         p1 = b1 * Xn1;
 8001b0e:	ee63 5a07 	vmul.f32	s11, s6, s14
         acc1 = p0 + d1;
         p3 = a1 * acc1;
 8001b12:	ee62 6a06 	vmul.f32	s13, s4, s12
         p2 = b2 * Xn1;
         A1 = p1 + p3;
 8001b16:	ee75 5aa6 	vadd.f32	s11, s11, s13
         p4 = a2 * acc1;
         d1 = A1 + d2;
 8001b1a:	ee75 5aa7 	vadd.f32	s11, s11, s15

         p0 = b0 * Xn1; 
         p1 = b1 * Xn1;
         acc1 = p0 + d1;
         p3 = a1 * acc1;
         p2 = b2 * Xn1;
 8001b1e:	ee62 7a87 	vmul.f32	s15, s5, s14
         A1 = p1 + p3;
         p4 = a2 * acc1;
 8001b22:	ee61 6a86 	vmul.f32	s13, s3, s12
         d1 = A1 + d2;
         d2 = p2 + p4;
 8001b26:	ee77 7aa6 	vadd.f32	s15, s15, s13
	
         *pOut++ = acc1;
 8001b2a:	eca0 6a01 	vstmia	r0!, {s12}
				 
         sample--;	       
      }

      sample = blockSize & 0x3u;
      while(sample > 0u) {
 8001b2e:	3b01      	subs	r3, #1
 8001b30:	d1e7      	bne.n	8001b02 <arm_biquad_cascade_df2T_f32+0x11e>
         
         sample--;	       
      }

      /* Store the updated state variables back into the state array */
      *pState++ = d1;
 8001b32:	ed44 5a02 	vstr	s11, [r4, #-8]
      *pState++ = d2;
 8001b36:	ed44 7a01 	vstr	s15, [r4, #-4]
 8001b3a:	3514      	adds	r5, #20
 8001b3c:	3608      	adds	r6, #8

      /* The current stage input is given as the output to the next stage */
      pIn = pDst;
 8001b3e:	4641      	mov	r1, r8
      pOut = pDst;

      /* decrement the loop counter */
      stage--;

   } while(stage > 0u);
 8001b40:	3f01      	subs	r7, #1
 8001b42:	f47f af63 	bne.w	8001a0c <arm_biquad_cascade_df2T_f32+0x28>

#endif 

}
 8001b46:	b003      	add	sp, #12
 8001b48:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

08001b4c <arm_biquad_cascade_df2T_init_f32>:
void arm_biquad_cascade_df2T_init_f32(
  arm_biquad_cascade_df2T_instance_f32 * S,
  uint8_t numStages,
  float32_t * pCoeffs,
  float32_t * pState)
{
 8001b4c:	b538      	push	{r3, r4, r5, lr}
 8001b4e:	4604      	mov	r4, r0
  /* Assign filter stages */
  S->numStages = numStages;

  /* Assign coefficient pointer */
  S->pCoeffs = pCoeffs;
 8001b50:	6082      	str	r2, [r0, #8]
  uint8_t numStages,
  float32_t * pCoeffs,
  float32_t * pState)
{
  /* Assign filter stages */
  S->numStages = numStages;
 8001b52:	7001      	strb	r1, [r0, #0]
void arm_biquad_cascade_df2T_init_f32(
  arm_biquad_cascade_df2T_instance_f32 * S,
  uint8_t numStages,
  float32_t * pCoeffs,
  float32_t * pState)
{
 8001b54:	461d      	mov	r5, r3

  /* Assign coefficient pointer */
  S->pCoeffs = pCoeffs;

  /* Clear state buffer and size is always 2 * numStages */
  memset(pState, 0, (2u * (uint32_t) numStages) * sizeof(float32_t));
 8001b56:	00ca      	lsls	r2, r1, #3
 8001b58:	4618      	mov	r0, r3
 8001b5a:	2100      	movs	r1, #0
 8001b5c:	f000 fe90 	bl	8002880 <memset>

  /* Assign state pointer */
  S->pState = pState;
 8001b60:	6065      	str	r5, [r4, #4]
 8001b62:	bd38      	pop	{r3, r4, r5, pc}

08001b64 <arm_copy_f32>:

void arm_copy_f32(
  float32_t * pSrc,
  float32_t * pDst,
  uint32_t blockSize)
{
 8001b64:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  /*loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.    
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 8001b68:	ea5f 0892 	movs.w	r8, r2, lsr #2
 8001b6c:	d01e      	beq.n	8001bac <arm_copy_f32+0x48>
 8001b6e:	f100 0410 	add.w	r4, r0, #16
 8001b72:	f101 0310 	add.w	r3, r1, #16
 8001b76:	4645      	mov	r5, r8
  {
    /* C = A */
    /* Copy and then store the results in the destination buffer */
    in1 = *pSrc++;
 8001b78:	f854 cc10 	ldr.w	ip, [r4, #-16]
    in2 = *pSrc++;
 8001b7c:	f854 ec0c 	ldr.w	lr, [r4, #-12]
    in3 = *pSrc++;
 8001b80:	f854 7c08 	ldr.w	r7, [r4, #-8]
    in4 = *pSrc++;
 8001b84:	f854 6c04 	ldr.w	r6, [r4, #-4]

    *pDst++ = in1;
    *pDst++ = in2;
    *pDst++ = in3;
    *pDst++ = in4;
 8001b88:	f843 6c04 	str.w	r6, [r3, #-4]
  /*loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.    
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 8001b8c:	3d01      	subs	r5, #1
    in1 = *pSrc++;
    in2 = *pSrc++;
    in3 = *pSrc++;
    in4 = *pSrc++;

    *pDst++ = in1;
 8001b8e:	f843 cc10 	str.w	ip, [r3, #-16]
    *pDst++ = in2;
 8001b92:	f843 ec0c 	str.w	lr, [r3, #-12]
    *pDst++ = in3;
 8001b96:	f843 7c08 	str.w	r7, [r3, #-8]
 8001b9a:	f104 0410 	add.w	r4, r4, #16
 8001b9e:	f103 0310 	add.w	r3, r3, #16
  /*loop Unrolling */
  blkCnt = blockSize >> 2u;

  /* First part of the processing with loop unrolling.  Compute 4 outputs at a time.    
   ** a second loop below computes the remaining 1 to 3 samples. */
  while(blkCnt > 0u)
 8001ba2:	d1e9      	bne.n	8001b78 <arm_copy_f32+0x14>
 8001ba4:	ea4f 1808 	mov.w	r8, r8, lsl #4
 8001ba8:	4440      	add	r0, r8
 8001baa:	4441      	add	r1, r8
  /* Loop over blockSize number of values */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 8001bac:	f012 0203 	ands.w	r2, r2, #3
 8001bb0:	d005      	beq.n	8001bbe <arm_copy_f32+0x5a>
  {
    /* C = A */
    /* Copy and then store the results in the destination buffer */
    *pDst++ = *pSrc++;
 8001bb2:	f850 3b04 	ldr.w	r3, [r0], #4
 8001bb6:	f841 3b04 	str.w	r3, [r1], #4
  /* Loop over blockSize number of values */
  blkCnt = blockSize;

#endif /* #ifndef ARM_MATH_CM0_FAMILY */

  while(blkCnt > 0u)
 8001bba:	3a01      	subs	r2, #1
 8001bbc:	d1f9      	bne.n	8001bb2 <arm_copy_f32+0x4e>
 8001bbe:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}
 8001bc2:	bf00      	nop

08001bc4 <__aeabi_drsub>:
 8001bc4:	f081 4100 	eor.w	r1, r1, #2147483648	; 0x80000000
 8001bc8:	e002      	b.n	8001bd0 <__adddf3>
 8001bca:	bf00      	nop

08001bcc <__aeabi_dsub>:
 8001bcc:	f083 4300 	eor.w	r3, r3, #2147483648	; 0x80000000

08001bd0 <__adddf3>:
 8001bd0:	b530      	push	{r4, r5, lr}
 8001bd2:	ea4f 0441 	mov.w	r4, r1, lsl #1
 8001bd6:	ea4f 0543 	mov.w	r5, r3, lsl #1
 8001bda:	ea94 0f05 	teq	r4, r5
 8001bde:	bf08      	it	eq
 8001be0:	ea90 0f02 	teqeq	r0, r2
 8001be4:	bf1f      	itttt	ne
 8001be6:	ea54 0c00 	orrsne.w	ip, r4, r0
 8001bea:	ea55 0c02 	orrsne.w	ip, r5, r2
 8001bee:	ea7f 5c64 	mvnsne.w	ip, r4, asr #21
 8001bf2:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 8001bf6:	f000 80e2 	beq.w	8001dbe <__adddf3+0x1ee>
 8001bfa:	ea4f 5454 	mov.w	r4, r4, lsr #21
 8001bfe:	ebd4 5555 	rsbs	r5, r4, r5, lsr #21
 8001c02:	bfb8      	it	lt
 8001c04:	426d      	neglt	r5, r5
 8001c06:	dd0c      	ble.n	8001c22 <__adddf3+0x52>
 8001c08:	442c      	add	r4, r5
 8001c0a:	ea80 0202 	eor.w	r2, r0, r2
 8001c0e:	ea81 0303 	eor.w	r3, r1, r3
 8001c12:	ea82 0000 	eor.w	r0, r2, r0
 8001c16:	ea83 0101 	eor.w	r1, r3, r1
 8001c1a:	ea80 0202 	eor.w	r2, r0, r2
 8001c1e:	ea81 0303 	eor.w	r3, r1, r3
 8001c22:	2d36      	cmp	r5, #54	; 0x36
 8001c24:	bf88      	it	hi
 8001c26:	bd30      	pophi	{r4, r5, pc}
 8001c28:	f011 4f00 	tst.w	r1, #2147483648	; 0x80000000
 8001c2c:	ea4f 3101 	mov.w	r1, r1, lsl #12
 8001c30:	f44f 1c80 	mov.w	ip, #1048576	; 0x100000
 8001c34:	ea4c 3111 	orr.w	r1, ip, r1, lsr #12
 8001c38:	d002      	beq.n	8001c40 <__adddf3+0x70>
 8001c3a:	4240      	negs	r0, r0
 8001c3c:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 8001c40:	f013 4f00 	tst.w	r3, #2147483648	; 0x80000000
 8001c44:	ea4f 3303 	mov.w	r3, r3, lsl #12
 8001c48:	ea4c 3313 	orr.w	r3, ip, r3, lsr #12
 8001c4c:	d002      	beq.n	8001c54 <__adddf3+0x84>
 8001c4e:	4252      	negs	r2, r2
 8001c50:	eb63 0343 	sbc.w	r3, r3, r3, lsl #1
 8001c54:	ea94 0f05 	teq	r4, r5
 8001c58:	f000 80a7 	beq.w	8001daa <__adddf3+0x1da>
 8001c5c:	f1a4 0401 	sub.w	r4, r4, #1
 8001c60:	f1d5 0e20 	rsbs	lr, r5, #32
 8001c64:	db0d      	blt.n	8001c82 <__adddf3+0xb2>
 8001c66:	fa02 fc0e 	lsl.w	ip, r2, lr
 8001c6a:	fa22 f205 	lsr.w	r2, r2, r5
 8001c6e:	1880      	adds	r0, r0, r2
 8001c70:	f141 0100 	adc.w	r1, r1, #0
 8001c74:	fa03 f20e 	lsl.w	r2, r3, lr
 8001c78:	1880      	adds	r0, r0, r2
 8001c7a:	fa43 f305 	asr.w	r3, r3, r5
 8001c7e:	4159      	adcs	r1, r3
 8001c80:	e00e      	b.n	8001ca0 <__adddf3+0xd0>
 8001c82:	f1a5 0520 	sub.w	r5, r5, #32
 8001c86:	f10e 0e20 	add.w	lr, lr, #32
 8001c8a:	2a01      	cmp	r2, #1
 8001c8c:	fa03 fc0e 	lsl.w	ip, r3, lr
 8001c90:	bf28      	it	cs
 8001c92:	f04c 0c02 	orrcs.w	ip, ip, #2
 8001c96:	fa43 f305 	asr.w	r3, r3, r5
 8001c9a:	18c0      	adds	r0, r0, r3
 8001c9c:	eb51 71e3 	adcs.w	r1, r1, r3, asr #31
 8001ca0:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8001ca4:	d507      	bpl.n	8001cb6 <__adddf3+0xe6>
 8001ca6:	f04f 0e00 	mov.w	lr, #0
 8001caa:	f1dc 0c00 	rsbs	ip, ip, #0
 8001cae:	eb7e 0000 	sbcs.w	r0, lr, r0
 8001cb2:	eb6e 0101 	sbc.w	r1, lr, r1
 8001cb6:	f5b1 1f80 	cmp.w	r1, #1048576	; 0x100000
 8001cba:	d31b      	bcc.n	8001cf4 <__adddf3+0x124>
 8001cbc:	f5b1 1f00 	cmp.w	r1, #2097152	; 0x200000
 8001cc0:	d30c      	bcc.n	8001cdc <__adddf3+0x10c>
 8001cc2:	0849      	lsrs	r1, r1, #1
 8001cc4:	ea5f 0030 	movs.w	r0, r0, rrx
 8001cc8:	ea4f 0c3c 	mov.w	ip, ip, rrx
 8001ccc:	f104 0401 	add.w	r4, r4, #1
 8001cd0:	ea4f 5244 	mov.w	r2, r4, lsl #21
 8001cd4:	f512 0f80 	cmn.w	r2, #4194304	; 0x400000
 8001cd8:	f080 809a 	bcs.w	8001e10 <__adddf3+0x240>
 8001cdc:	f1bc 4f00 	cmp.w	ip, #2147483648	; 0x80000000
 8001ce0:	bf08      	it	eq
 8001ce2:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 8001ce6:	f150 0000 	adcs.w	r0, r0, #0
 8001cea:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 8001cee:	ea41 0105 	orr.w	r1, r1, r5
 8001cf2:	bd30      	pop	{r4, r5, pc}
 8001cf4:	ea5f 0c4c 	movs.w	ip, ip, lsl #1
 8001cf8:	4140      	adcs	r0, r0
 8001cfa:	eb41 0101 	adc.w	r1, r1, r1
 8001cfe:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 8001d02:	f1a4 0401 	sub.w	r4, r4, #1
 8001d06:	d1e9      	bne.n	8001cdc <__adddf3+0x10c>
 8001d08:	f091 0f00 	teq	r1, #0
 8001d0c:	bf04      	itt	eq
 8001d0e:	4601      	moveq	r1, r0
 8001d10:	2000      	moveq	r0, #0
 8001d12:	fab1 f381 	clz	r3, r1
 8001d16:	bf08      	it	eq
 8001d18:	3320      	addeq	r3, #32
 8001d1a:	f1a3 030b 	sub.w	r3, r3, #11
 8001d1e:	f1b3 0220 	subs.w	r2, r3, #32
 8001d22:	da0c      	bge.n	8001d3e <__adddf3+0x16e>
 8001d24:	320c      	adds	r2, #12
 8001d26:	dd08      	ble.n	8001d3a <__adddf3+0x16a>
 8001d28:	f102 0c14 	add.w	ip, r2, #20
 8001d2c:	f1c2 020c 	rsb	r2, r2, #12
 8001d30:	fa01 f00c 	lsl.w	r0, r1, ip
 8001d34:	fa21 f102 	lsr.w	r1, r1, r2
 8001d38:	e00c      	b.n	8001d54 <__adddf3+0x184>
 8001d3a:	f102 0214 	add.w	r2, r2, #20
 8001d3e:	bfd8      	it	le
 8001d40:	f1c2 0c20 	rsble	ip, r2, #32
 8001d44:	fa01 f102 	lsl.w	r1, r1, r2
 8001d48:	fa20 fc0c 	lsr.w	ip, r0, ip
 8001d4c:	bfdc      	itt	le
 8001d4e:	ea41 010c 	orrle.w	r1, r1, ip
 8001d52:	4090      	lslle	r0, r2
 8001d54:	1ae4      	subs	r4, r4, r3
 8001d56:	bfa2      	ittt	ge
 8001d58:	eb01 5104 	addge.w	r1, r1, r4, lsl #20
 8001d5c:	4329      	orrge	r1, r5
 8001d5e:	bd30      	popge	{r4, r5, pc}
 8001d60:	ea6f 0404 	mvn.w	r4, r4
 8001d64:	3c1f      	subs	r4, #31
 8001d66:	da1c      	bge.n	8001da2 <__adddf3+0x1d2>
 8001d68:	340c      	adds	r4, #12
 8001d6a:	dc0e      	bgt.n	8001d8a <__adddf3+0x1ba>
 8001d6c:	f104 0414 	add.w	r4, r4, #20
 8001d70:	f1c4 0220 	rsb	r2, r4, #32
 8001d74:	fa20 f004 	lsr.w	r0, r0, r4
 8001d78:	fa01 f302 	lsl.w	r3, r1, r2
 8001d7c:	ea40 0003 	orr.w	r0, r0, r3
 8001d80:	fa21 f304 	lsr.w	r3, r1, r4
 8001d84:	ea45 0103 	orr.w	r1, r5, r3
 8001d88:	bd30      	pop	{r4, r5, pc}
 8001d8a:	f1c4 040c 	rsb	r4, r4, #12
 8001d8e:	f1c4 0220 	rsb	r2, r4, #32
 8001d92:	fa20 f002 	lsr.w	r0, r0, r2
 8001d96:	fa01 f304 	lsl.w	r3, r1, r4
 8001d9a:	ea40 0003 	orr.w	r0, r0, r3
 8001d9e:	4629      	mov	r1, r5
 8001da0:	bd30      	pop	{r4, r5, pc}
 8001da2:	fa21 f004 	lsr.w	r0, r1, r4
 8001da6:	4629      	mov	r1, r5
 8001da8:	bd30      	pop	{r4, r5, pc}
 8001daa:	f094 0f00 	teq	r4, #0
 8001dae:	f483 1380 	eor.w	r3, r3, #1048576	; 0x100000
 8001db2:	bf06      	itte	eq
 8001db4:	f481 1180 	eoreq.w	r1, r1, #1048576	; 0x100000
 8001db8:	3401      	addeq	r4, #1
 8001dba:	3d01      	subne	r5, #1
 8001dbc:	e74e      	b.n	8001c5c <__adddf3+0x8c>
 8001dbe:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 8001dc2:	bf18      	it	ne
 8001dc4:	ea7f 5c65 	mvnsne.w	ip, r5, asr #21
 8001dc8:	d029      	beq.n	8001e1e <__adddf3+0x24e>
 8001dca:	ea94 0f05 	teq	r4, r5
 8001dce:	bf08      	it	eq
 8001dd0:	ea90 0f02 	teqeq	r0, r2
 8001dd4:	d005      	beq.n	8001de2 <__adddf3+0x212>
 8001dd6:	ea54 0c00 	orrs.w	ip, r4, r0
 8001dda:	bf04      	itt	eq
 8001ddc:	4619      	moveq	r1, r3
 8001dde:	4610      	moveq	r0, r2
 8001de0:	bd30      	pop	{r4, r5, pc}
 8001de2:	ea91 0f03 	teq	r1, r3
 8001de6:	bf1e      	ittt	ne
 8001de8:	2100      	movne	r1, #0
 8001dea:	2000      	movne	r0, #0
 8001dec:	bd30      	popne	{r4, r5, pc}
 8001dee:	ea5f 5c54 	movs.w	ip, r4, lsr #21
 8001df2:	d105      	bne.n	8001e00 <__adddf3+0x230>
 8001df4:	0040      	lsls	r0, r0, #1
 8001df6:	4149      	adcs	r1, r1
 8001df8:	bf28      	it	cs
 8001dfa:	f041 4100 	orrcs.w	r1, r1, #2147483648	; 0x80000000
 8001dfe:	bd30      	pop	{r4, r5, pc}
 8001e00:	f514 0480 	adds.w	r4, r4, #4194304	; 0x400000
 8001e04:	bf3c      	itt	cc
 8001e06:	f501 1180 	addcc.w	r1, r1, #1048576	; 0x100000
 8001e0a:	bd30      	popcc	{r4, r5, pc}
 8001e0c:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8001e10:	f045 41fe 	orr.w	r1, r5, #2130706432	; 0x7f000000
 8001e14:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 8001e18:	f04f 0000 	mov.w	r0, #0
 8001e1c:	bd30      	pop	{r4, r5, pc}
 8001e1e:	ea7f 5c64 	mvns.w	ip, r4, asr #21
 8001e22:	bf1a      	itte	ne
 8001e24:	4619      	movne	r1, r3
 8001e26:	4610      	movne	r0, r2
 8001e28:	ea7f 5c65 	mvnseq.w	ip, r5, asr #21
 8001e2c:	bf1c      	itt	ne
 8001e2e:	460b      	movne	r3, r1
 8001e30:	4602      	movne	r2, r0
 8001e32:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 8001e36:	bf06      	itte	eq
 8001e38:	ea52 3503 	orrseq.w	r5, r2, r3, lsl #12
 8001e3c:	ea91 0f03 	teqeq	r1, r3
 8001e40:	f441 2100 	orrne.w	r1, r1, #524288	; 0x80000
 8001e44:	bd30      	pop	{r4, r5, pc}
 8001e46:	bf00      	nop

08001e48 <__aeabi_ui2d>:
 8001e48:	f090 0f00 	teq	r0, #0
 8001e4c:	bf04      	itt	eq
 8001e4e:	2100      	moveq	r1, #0
 8001e50:	4770      	bxeq	lr
 8001e52:	b530      	push	{r4, r5, lr}
 8001e54:	f44f 6480 	mov.w	r4, #1024	; 0x400
 8001e58:	f104 0432 	add.w	r4, r4, #50	; 0x32
 8001e5c:	f04f 0500 	mov.w	r5, #0
 8001e60:	f04f 0100 	mov.w	r1, #0
 8001e64:	e750      	b.n	8001d08 <__adddf3+0x138>
 8001e66:	bf00      	nop

08001e68 <__aeabi_i2d>:
 8001e68:	f090 0f00 	teq	r0, #0
 8001e6c:	bf04      	itt	eq
 8001e6e:	2100      	moveq	r1, #0
 8001e70:	4770      	bxeq	lr
 8001e72:	b530      	push	{r4, r5, lr}
 8001e74:	f44f 6480 	mov.w	r4, #1024	; 0x400
 8001e78:	f104 0432 	add.w	r4, r4, #50	; 0x32
 8001e7c:	f010 4500 	ands.w	r5, r0, #2147483648	; 0x80000000
 8001e80:	bf48      	it	mi
 8001e82:	4240      	negmi	r0, r0
 8001e84:	f04f 0100 	mov.w	r1, #0
 8001e88:	e73e      	b.n	8001d08 <__adddf3+0x138>
 8001e8a:	bf00      	nop

08001e8c <__aeabi_f2d>:
 8001e8c:	0042      	lsls	r2, r0, #1
 8001e8e:	ea4f 01e2 	mov.w	r1, r2, asr #3
 8001e92:	ea4f 0131 	mov.w	r1, r1, rrx
 8001e96:	ea4f 7002 	mov.w	r0, r2, lsl #28
 8001e9a:	bf1f      	itttt	ne
 8001e9c:	f012 437f 	andsne.w	r3, r2, #4278190080	; 0xff000000
 8001ea0:	f093 4f7f 	teqne	r3, #4278190080	; 0xff000000
 8001ea4:	f081 5160 	eorne.w	r1, r1, #939524096	; 0x38000000
 8001ea8:	4770      	bxne	lr
 8001eaa:	f092 0f00 	teq	r2, #0
 8001eae:	bf14      	ite	ne
 8001eb0:	f093 4f7f 	teqne	r3, #4278190080	; 0xff000000
 8001eb4:	4770      	bxeq	lr
 8001eb6:	b530      	push	{r4, r5, lr}
 8001eb8:	f44f 7460 	mov.w	r4, #896	; 0x380
 8001ebc:	f001 4500 	and.w	r5, r1, #2147483648	; 0x80000000
 8001ec0:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 8001ec4:	e720      	b.n	8001d08 <__adddf3+0x138>
 8001ec6:	bf00      	nop

08001ec8 <__aeabi_ul2d>:
 8001ec8:	ea50 0201 	orrs.w	r2, r0, r1
 8001ecc:	bf08      	it	eq
 8001ece:	4770      	bxeq	lr
 8001ed0:	b530      	push	{r4, r5, lr}
 8001ed2:	f04f 0500 	mov.w	r5, #0
 8001ed6:	e00a      	b.n	8001eee <__aeabi_l2d+0x16>

08001ed8 <__aeabi_l2d>:
 8001ed8:	ea50 0201 	orrs.w	r2, r0, r1
 8001edc:	bf08      	it	eq
 8001ede:	4770      	bxeq	lr
 8001ee0:	b530      	push	{r4, r5, lr}
 8001ee2:	f011 4500 	ands.w	r5, r1, #2147483648	; 0x80000000
 8001ee6:	d502      	bpl.n	8001eee <__aeabi_l2d+0x16>
 8001ee8:	4240      	negs	r0, r0
 8001eea:	eb61 0141 	sbc.w	r1, r1, r1, lsl #1
 8001eee:	f44f 6480 	mov.w	r4, #1024	; 0x400
 8001ef2:	f104 0432 	add.w	r4, r4, #50	; 0x32
 8001ef6:	ea5f 5c91 	movs.w	ip, r1, lsr #22
 8001efa:	f43f aedc 	beq.w	8001cb6 <__adddf3+0xe6>
 8001efe:	f04f 0203 	mov.w	r2, #3
 8001f02:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 8001f06:	bf18      	it	ne
 8001f08:	3203      	addne	r2, #3
 8001f0a:	ea5f 0cdc 	movs.w	ip, ip, lsr #3
 8001f0e:	bf18      	it	ne
 8001f10:	3203      	addne	r2, #3
 8001f12:	eb02 02dc 	add.w	r2, r2, ip, lsr #3
 8001f16:	f1c2 0320 	rsb	r3, r2, #32
 8001f1a:	fa00 fc03 	lsl.w	ip, r0, r3
 8001f1e:	fa20 f002 	lsr.w	r0, r0, r2
 8001f22:	fa01 fe03 	lsl.w	lr, r1, r3
 8001f26:	ea40 000e 	orr.w	r0, r0, lr
 8001f2a:	fa21 f102 	lsr.w	r1, r1, r2
 8001f2e:	4414      	add	r4, r2
 8001f30:	e6c1      	b.n	8001cb6 <__adddf3+0xe6>
 8001f32:	bf00      	nop

08001f34 <__aeabi_dmul>:
 8001f34:	b570      	push	{r4, r5, r6, lr}
 8001f36:	f04f 0cff 	mov.w	ip, #255	; 0xff
 8001f3a:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 8001f3e:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 8001f42:	bf1d      	ittte	ne
 8001f44:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 8001f48:	ea94 0f0c 	teqne	r4, ip
 8001f4c:	ea95 0f0c 	teqne	r5, ip
 8001f50:	f000 f8de 	bleq	8002110 <__aeabi_dmul+0x1dc>
 8001f54:	442c      	add	r4, r5
 8001f56:	ea81 0603 	eor.w	r6, r1, r3
 8001f5a:	ea21 514c 	bic.w	r1, r1, ip, lsl #21
 8001f5e:	ea23 534c 	bic.w	r3, r3, ip, lsl #21
 8001f62:	ea50 3501 	orrs.w	r5, r0, r1, lsl #12
 8001f66:	bf18      	it	ne
 8001f68:	ea52 3503 	orrsne.w	r5, r2, r3, lsl #12
 8001f6c:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 8001f70:	f443 1380 	orr.w	r3, r3, #1048576	; 0x100000
 8001f74:	d038      	beq.n	8001fe8 <__aeabi_dmul+0xb4>
 8001f76:	fba0 ce02 	umull	ip, lr, r0, r2
 8001f7a:	f04f 0500 	mov.w	r5, #0
 8001f7e:	fbe1 e502 	umlal	lr, r5, r1, r2
 8001f82:	f006 4200 	and.w	r2, r6, #2147483648	; 0x80000000
 8001f86:	fbe0 e503 	umlal	lr, r5, r0, r3
 8001f8a:	f04f 0600 	mov.w	r6, #0
 8001f8e:	fbe1 5603 	umlal	r5, r6, r1, r3
 8001f92:	f09c 0f00 	teq	ip, #0
 8001f96:	bf18      	it	ne
 8001f98:	f04e 0e01 	orrne.w	lr, lr, #1
 8001f9c:	f1a4 04ff 	sub.w	r4, r4, #255	; 0xff
 8001fa0:	f5b6 7f00 	cmp.w	r6, #512	; 0x200
 8001fa4:	f564 7440 	sbc.w	r4, r4, #768	; 0x300
 8001fa8:	d204      	bcs.n	8001fb4 <__aeabi_dmul+0x80>
 8001faa:	ea5f 0e4e 	movs.w	lr, lr, lsl #1
 8001fae:	416d      	adcs	r5, r5
 8001fb0:	eb46 0606 	adc.w	r6, r6, r6
 8001fb4:	ea42 21c6 	orr.w	r1, r2, r6, lsl #11
 8001fb8:	ea41 5155 	orr.w	r1, r1, r5, lsr #21
 8001fbc:	ea4f 20c5 	mov.w	r0, r5, lsl #11
 8001fc0:	ea40 505e 	orr.w	r0, r0, lr, lsr #21
 8001fc4:	ea4f 2ece 	mov.w	lr, lr, lsl #11
 8001fc8:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 8001fcc:	bf88      	it	hi
 8001fce:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 8001fd2:	d81e      	bhi.n	8002012 <__aeabi_dmul+0xde>
 8001fd4:	f1be 4f00 	cmp.w	lr, #2147483648	; 0x80000000
 8001fd8:	bf08      	it	eq
 8001fda:	ea5f 0e50 	movseq.w	lr, r0, lsr #1
 8001fde:	f150 0000 	adcs.w	r0, r0, #0
 8001fe2:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 8001fe6:	bd70      	pop	{r4, r5, r6, pc}
 8001fe8:	f006 4600 	and.w	r6, r6, #2147483648	; 0x80000000
 8001fec:	ea46 0101 	orr.w	r1, r6, r1
 8001ff0:	ea40 0002 	orr.w	r0, r0, r2
 8001ff4:	ea81 0103 	eor.w	r1, r1, r3
 8001ff8:	ebb4 045c 	subs.w	r4, r4, ip, lsr #1
 8001ffc:	bfc2      	ittt	gt
 8001ffe:	ebd4 050c 	rsbsgt	r5, r4, ip
 8002002:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 8002006:	bd70      	popgt	{r4, r5, r6, pc}
 8002008:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 800200c:	f04f 0e00 	mov.w	lr, #0
 8002010:	3c01      	subs	r4, #1
 8002012:	f300 80ab 	bgt.w	800216c <__aeabi_dmul+0x238>
 8002016:	f114 0f36 	cmn.w	r4, #54	; 0x36
 800201a:	bfde      	ittt	le
 800201c:	2000      	movle	r0, #0
 800201e:	f001 4100 	andle.w	r1, r1, #2147483648	; 0x80000000
 8002022:	bd70      	pople	{r4, r5, r6, pc}
 8002024:	f1c4 0400 	rsb	r4, r4, #0
 8002028:	3c20      	subs	r4, #32
 800202a:	da35      	bge.n	8002098 <__aeabi_dmul+0x164>
 800202c:	340c      	adds	r4, #12
 800202e:	dc1b      	bgt.n	8002068 <__aeabi_dmul+0x134>
 8002030:	f104 0414 	add.w	r4, r4, #20
 8002034:	f1c4 0520 	rsb	r5, r4, #32
 8002038:	fa00 f305 	lsl.w	r3, r0, r5
 800203c:	fa20 f004 	lsr.w	r0, r0, r4
 8002040:	fa01 f205 	lsl.w	r2, r1, r5
 8002044:	ea40 0002 	orr.w	r0, r0, r2
 8002048:	f001 4200 	and.w	r2, r1, #2147483648	; 0x80000000
 800204c:	f021 4100 	bic.w	r1, r1, #2147483648	; 0x80000000
 8002050:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 8002054:	fa21 f604 	lsr.w	r6, r1, r4
 8002058:	eb42 0106 	adc.w	r1, r2, r6
 800205c:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 8002060:	bf08      	it	eq
 8002062:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 8002066:	bd70      	pop	{r4, r5, r6, pc}
 8002068:	f1c4 040c 	rsb	r4, r4, #12
 800206c:	f1c4 0520 	rsb	r5, r4, #32
 8002070:	fa00 f304 	lsl.w	r3, r0, r4
 8002074:	fa20 f005 	lsr.w	r0, r0, r5
 8002078:	fa01 f204 	lsl.w	r2, r1, r4
 800207c:	ea40 0002 	orr.w	r0, r0, r2
 8002080:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8002084:	eb10 70d3 	adds.w	r0, r0, r3, lsr #31
 8002088:	f141 0100 	adc.w	r1, r1, #0
 800208c:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 8002090:	bf08      	it	eq
 8002092:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 8002096:	bd70      	pop	{r4, r5, r6, pc}
 8002098:	f1c4 0520 	rsb	r5, r4, #32
 800209c:	fa00 f205 	lsl.w	r2, r0, r5
 80020a0:	ea4e 0e02 	orr.w	lr, lr, r2
 80020a4:	fa20 f304 	lsr.w	r3, r0, r4
 80020a8:	fa01 f205 	lsl.w	r2, r1, r5
 80020ac:	ea43 0302 	orr.w	r3, r3, r2
 80020b0:	fa21 f004 	lsr.w	r0, r1, r4
 80020b4:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 80020b8:	fa21 f204 	lsr.w	r2, r1, r4
 80020bc:	ea20 0002 	bic.w	r0, r0, r2
 80020c0:	eb00 70d3 	add.w	r0, r0, r3, lsr #31
 80020c4:	ea5e 0e43 	orrs.w	lr, lr, r3, lsl #1
 80020c8:	bf08      	it	eq
 80020ca:	ea20 70d3 	biceq.w	r0, r0, r3, lsr #31
 80020ce:	bd70      	pop	{r4, r5, r6, pc}
 80020d0:	f094 0f00 	teq	r4, #0
 80020d4:	d10f      	bne.n	80020f6 <__aeabi_dmul+0x1c2>
 80020d6:	f001 4600 	and.w	r6, r1, #2147483648	; 0x80000000
 80020da:	0040      	lsls	r0, r0, #1
 80020dc:	eb41 0101 	adc.w	r1, r1, r1
 80020e0:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 80020e4:	bf08      	it	eq
 80020e6:	3c01      	subeq	r4, #1
 80020e8:	d0f7      	beq.n	80020da <__aeabi_dmul+0x1a6>
 80020ea:	ea41 0106 	orr.w	r1, r1, r6
 80020ee:	f095 0f00 	teq	r5, #0
 80020f2:	bf18      	it	ne
 80020f4:	4770      	bxne	lr
 80020f6:	f003 4600 	and.w	r6, r3, #2147483648	; 0x80000000
 80020fa:	0052      	lsls	r2, r2, #1
 80020fc:	eb43 0303 	adc.w	r3, r3, r3
 8002100:	f413 1f80 	tst.w	r3, #1048576	; 0x100000
 8002104:	bf08      	it	eq
 8002106:	3d01      	subeq	r5, #1
 8002108:	d0f7      	beq.n	80020fa <__aeabi_dmul+0x1c6>
 800210a:	ea43 0306 	orr.w	r3, r3, r6
 800210e:	4770      	bx	lr
 8002110:	ea94 0f0c 	teq	r4, ip
 8002114:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 8002118:	bf18      	it	ne
 800211a:	ea95 0f0c 	teqne	r5, ip
 800211e:	d00c      	beq.n	800213a <__aeabi_dmul+0x206>
 8002120:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 8002124:	bf18      	it	ne
 8002126:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 800212a:	d1d1      	bne.n	80020d0 <__aeabi_dmul+0x19c>
 800212c:	ea81 0103 	eor.w	r1, r1, r3
 8002130:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8002134:	f04f 0000 	mov.w	r0, #0
 8002138:	bd70      	pop	{r4, r5, r6, pc}
 800213a:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 800213e:	bf06      	itte	eq
 8002140:	4610      	moveq	r0, r2
 8002142:	4619      	moveq	r1, r3
 8002144:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 8002148:	d019      	beq.n	800217e <__aeabi_dmul+0x24a>
 800214a:	ea94 0f0c 	teq	r4, ip
 800214e:	d102      	bne.n	8002156 <__aeabi_dmul+0x222>
 8002150:	ea50 3601 	orrs.w	r6, r0, r1, lsl #12
 8002154:	d113      	bne.n	800217e <__aeabi_dmul+0x24a>
 8002156:	ea95 0f0c 	teq	r5, ip
 800215a:	d105      	bne.n	8002168 <__aeabi_dmul+0x234>
 800215c:	ea52 3603 	orrs.w	r6, r2, r3, lsl #12
 8002160:	bf1c      	itt	ne
 8002162:	4610      	movne	r0, r2
 8002164:	4619      	movne	r1, r3
 8002166:	d10a      	bne.n	800217e <__aeabi_dmul+0x24a>
 8002168:	ea81 0103 	eor.w	r1, r1, r3
 800216c:	f001 4100 	and.w	r1, r1, #2147483648	; 0x80000000
 8002170:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 8002174:	f441 0170 	orr.w	r1, r1, #15728640	; 0xf00000
 8002178:	f04f 0000 	mov.w	r0, #0
 800217c:	bd70      	pop	{r4, r5, r6, pc}
 800217e:	f041 41fe 	orr.w	r1, r1, #2130706432	; 0x7f000000
 8002182:	f441 0178 	orr.w	r1, r1, #16252928	; 0xf80000
 8002186:	bd70      	pop	{r4, r5, r6, pc}

08002188 <__aeabi_ddiv>:
 8002188:	b570      	push	{r4, r5, r6, lr}
 800218a:	f04f 0cff 	mov.w	ip, #255	; 0xff
 800218e:	f44c 6ce0 	orr.w	ip, ip, #1792	; 0x700
 8002192:	ea1c 5411 	ands.w	r4, ip, r1, lsr #20
 8002196:	bf1d      	ittte	ne
 8002198:	ea1c 5513 	andsne.w	r5, ip, r3, lsr #20
 800219c:	ea94 0f0c 	teqne	r4, ip
 80021a0:	ea95 0f0c 	teqne	r5, ip
 80021a4:	f000 f8a7 	bleq	80022f6 <__aeabi_ddiv+0x16e>
 80021a8:	eba4 0405 	sub.w	r4, r4, r5
 80021ac:	ea81 0e03 	eor.w	lr, r1, r3
 80021b0:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 80021b4:	ea4f 3101 	mov.w	r1, r1, lsl #12
 80021b8:	f000 8088 	beq.w	80022cc <__aeabi_ddiv+0x144>
 80021bc:	ea4f 3303 	mov.w	r3, r3, lsl #12
 80021c0:	f04f 5580 	mov.w	r5, #268435456	; 0x10000000
 80021c4:	ea45 1313 	orr.w	r3, r5, r3, lsr #4
 80021c8:	ea43 6312 	orr.w	r3, r3, r2, lsr #24
 80021cc:	ea4f 2202 	mov.w	r2, r2, lsl #8
 80021d0:	ea45 1511 	orr.w	r5, r5, r1, lsr #4
 80021d4:	ea45 6510 	orr.w	r5, r5, r0, lsr #24
 80021d8:	ea4f 2600 	mov.w	r6, r0, lsl #8
 80021dc:	f00e 4100 	and.w	r1, lr, #2147483648	; 0x80000000
 80021e0:	429d      	cmp	r5, r3
 80021e2:	bf08      	it	eq
 80021e4:	4296      	cmpeq	r6, r2
 80021e6:	f144 04fd 	adc.w	r4, r4, #253	; 0xfd
 80021ea:	f504 7440 	add.w	r4, r4, #768	; 0x300
 80021ee:	d202      	bcs.n	80021f6 <__aeabi_ddiv+0x6e>
 80021f0:	085b      	lsrs	r3, r3, #1
 80021f2:	ea4f 0232 	mov.w	r2, r2, rrx
 80021f6:	1ab6      	subs	r6, r6, r2
 80021f8:	eb65 0503 	sbc.w	r5, r5, r3
 80021fc:	085b      	lsrs	r3, r3, #1
 80021fe:	ea4f 0232 	mov.w	r2, r2, rrx
 8002202:	f44f 1080 	mov.w	r0, #1048576	; 0x100000
 8002206:	f44f 2c00 	mov.w	ip, #524288	; 0x80000
 800220a:	ebb6 0e02 	subs.w	lr, r6, r2
 800220e:	eb75 0e03 	sbcs.w	lr, r5, r3
 8002212:	bf22      	ittt	cs
 8002214:	1ab6      	subcs	r6, r6, r2
 8002216:	4675      	movcs	r5, lr
 8002218:	ea40 000c 	orrcs.w	r0, r0, ip
 800221c:	085b      	lsrs	r3, r3, #1
 800221e:	ea4f 0232 	mov.w	r2, r2, rrx
 8002222:	ebb6 0e02 	subs.w	lr, r6, r2
 8002226:	eb75 0e03 	sbcs.w	lr, r5, r3
 800222a:	bf22      	ittt	cs
 800222c:	1ab6      	subcs	r6, r6, r2
 800222e:	4675      	movcs	r5, lr
 8002230:	ea40 005c 	orrcs.w	r0, r0, ip, lsr #1
 8002234:	085b      	lsrs	r3, r3, #1
 8002236:	ea4f 0232 	mov.w	r2, r2, rrx
 800223a:	ebb6 0e02 	subs.w	lr, r6, r2
 800223e:	eb75 0e03 	sbcs.w	lr, r5, r3
 8002242:	bf22      	ittt	cs
 8002244:	1ab6      	subcs	r6, r6, r2
 8002246:	4675      	movcs	r5, lr
 8002248:	ea40 009c 	orrcs.w	r0, r0, ip, lsr #2
 800224c:	085b      	lsrs	r3, r3, #1
 800224e:	ea4f 0232 	mov.w	r2, r2, rrx
 8002252:	ebb6 0e02 	subs.w	lr, r6, r2
 8002256:	eb75 0e03 	sbcs.w	lr, r5, r3
 800225a:	bf22      	ittt	cs
 800225c:	1ab6      	subcs	r6, r6, r2
 800225e:	4675      	movcs	r5, lr
 8002260:	ea40 00dc 	orrcs.w	r0, r0, ip, lsr #3
 8002264:	ea55 0e06 	orrs.w	lr, r5, r6
 8002268:	d018      	beq.n	800229c <__aeabi_ddiv+0x114>
 800226a:	ea4f 1505 	mov.w	r5, r5, lsl #4
 800226e:	ea45 7516 	orr.w	r5, r5, r6, lsr #28
 8002272:	ea4f 1606 	mov.w	r6, r6, lsl #4
 8002276:	ea4f 03c3 	mov.w	r3, r3, lsl #3
 800227a:	ea43 7352 	orr.w	r3, r3, r2, lsr #29
 800227e:	ea4f 02c2 	mov.w	r2, r2, lsl #3
 8002282:	ea5f 1c1c 	movs.w	ip, ip, lsr #4
 8002286:	d1c0      	bne.n	800220a <__aeabi_ddiv+0x82>
 8002288:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 800228c:	d10b      	bne.n	80022a6 <__aeabi_ddiv+0x11e>
 800228e:	ea41 0100 	orr.w	r1, r1, r0
 8002292:	f04f 0000 	mov.w	r0, #0
 8002296:	f04f 4c00 	mov.w	ip, #2147483648	; 0x80000000
 800229a:	e7b6      	b.n	800220a <__aeabi_ddiv+0x82>
 800229c:	f411 1f80 	tst.w	r1, #1048576	; 0x100000
 80022a0:	bf04      	itt	eq
 80022a2:	4301      	orreq	r1, r0
 80022a4:	2000      	moveq	r0, #0
 80022a6:	f1b4 0cfd 	subs.w	ip, r4, #253	; 0xfd
 80022aa:	bf88      	it	hi
 80022ac:	f5bc 6fe0 	cmphi.w	ip, #1792	; 0x700
 80022b0:	f63f aeaf 	bhi.w	8002012 <__aeabi_dmul+0xde>
 80022b4:	ebb5 0c03 	subs.w	ip, r5, r3
 80022b8:	bf04      	itt	eq
 80022ba:	ebb6 0c02 	subseq.w	ip, r6, r2
 80022be:	ea5f 0c50 	movseq.w	ip, r0, lsr #1
 80022c2:	f150 0000 	adcs.w	r0, r0, #0
 80022c6:	eb41 5104 	adc.w	r1, r1, r4, lsl #20
 80022ca:	bd70      	pop	{r4, r5, r6, pc}
 80022cc:	f00e 4e00 	and.w	lr, lr, #2147483648	; 0x80000000
 80022d0:	ea4e 3111 	orr.w	r1, lr, r1, lsr #12
 80022d4:	eb14 045c 	adds.w	r4, r4, ip, lsr #1
 80022d8:	bfc2      	ittt	gt
 80022da:	ebd4 050c 	rsbsgt	r5, r4, ip
 80022de:	ea41 5104 	orrgt.w	r1, r1, r4, lsl #20
 80022e2:	bd70      	popgt	{r4, r5, r6, pc}
 80022e4:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 80022e8:	f04f 0e00 	mov.w	lr, #0
 80022ec:	3c01      	subs	r4, #1
 80022ee:	e690      	b.n	8002012 <__aeabi_dmul+0xde>
 80022f0:	ea45 0e06 	orr.w	lr, r5, r6
 80022f4:	e68d      	b.n	8002012 <__aeabi_dmul+0xde>
 80022f6:	ea0c 5513 	and.w	r5, ip, r3, lsr #20
 80022fa:	ea94 0f0c 	teq	r4, ip
 80022fe:	bf08      	it	eq
 8002300:	ea95 0f0c 	teqeq	r5, ip
 8002304:	f43f af3b 	beq.w	800217e <__aeabi_dmul+0x24a>
 8002308:	ea94 0f0c 	teq	r4, ip
 800230c:	d10a      	bne.n	8002324 <__aeabi_ddiv+0x19c>
 800230e:	ea50 3401 	orrs.w	r4, r0, r1, lsl #12
 8002312:	f47f af34 	bne.w	800217e <__aeabi_dmul+0x24a>
 8002316:	ea95 0f0c 	teq	r5, ip
 800231a:	f47f af25 	bne.w	8002168 <__aeabi_dmul+0x234>
 800231e:	4610      	mov	r0, r2
 8002320:	4619      	mov	r1, r3
 8002322:	e72c      	b.n	800217e <__aeabi_dmul+0x24a>
 8002324:	ea95 0f0c 	teq	r5, ip
 8002328:	d106      	bne.n	8002338 <__aeabi_ddiv+0x1b0>
 800232a:	ea52 3503 	orrs.w	r5, r2, r3, lsl #12
 800232e:	f43f aefd 	beq.w	800212c <__aeabi_dmul+0x1f8>
 8002332:	4610      	mov	r0, r2
 8002334:	4619      	mov	r1, r3
 8002336:	e722      	b.n	800217e <__aeabi_dmul+0x24a>
 8002338:	ea50 0641 	orrs.w	r6, r0, r1, lsl #1
 800233c:	bf18      	it	ne
 800233e:	ea52 0643 	orrsne.w	r6, r2, r3, lsl #1
 8002342:	f47f aec5 	bne.w	80020d0 <__aeabi_dmul+0x19c>
 8002346:	ea50 0441 	orrs.w	r4, r0, r1, lsl #1
 800234a:	f47f af0d 	bne.w	8002168 <__aeabi_dmul+0x234>
 800234e:	ea52 0543 	orrs.w	r5, r2, r3, lsl #1
 8002352:	f47f aeeb 	bne.w	800212c <__aeabi_dmul+0x1f8>
 8002356:	e712      	b.n	800217e <__aeabi_dmul+0x24a>

08002358 <__aeabi_d2f>:
 8002358:	ea4f 0241 	mov.w	r2, r1, lsl #1
 800235c:	f1b2 43e0 	subs.w	r3, r2, #1879048192	; 0x70000000
 8002360:	bf24      	itt	cs
 8002362:	f5b3 1c00 	subscs.w	ip, r3, #2097152	; 0x200000
 8002366:	f1dc 5cfe 	rsbscs	ip, ip, #532676608	; 0x1fc00000
 800236a:	d90d      	bls.n	8002388 <__aeabi_d2f+0x30>
 800236c:	f001 4c00 	and.w	ip, r1, #2147483648	; 0x80000000
 8002370:	ea4f 02c0 	mov.w	r2, r0, lsl #3
 8002374:	ea4c 7050 	orr.w	r0, ip, r0, lsr #29
 8002378:	f1b2 4f00 	cmp.w	r2, #2147483648	; 0x80000000
 800237c:	eb40 0083 	adc.w	r0, r0, r3, lsl #2
 8002380:	bf08      	it	eq
 8002382:	f020 0001 	biceq.w	r0, r0, #1
 8002386:	4770      	bx	lr
 8002388:	f011 4f80 	tst.w	r1, #1073741824	; 0x40000000
 800238c:	d121      	bne.n	80023d2 <__aeabi_d2f+0x7a>
 800238e:	f113 7238 	adds.w	r2, r3, #48234496	; 0x2e00000
 8002392:	bfbc      	itt	lt
 8002394:	f001 4000 	andlt.w	r0, r1, #2147483648	; 0x80000000
 8002398:	4770      	bxlt	lr
 800239a:	f441 1180 	orr.w	r1, r1, #1048576	; 0x100000
 800239e:	ea4f 5252 	mov.w	r2, r2, lsr #21
 80023a2:	f1c2 0218 	rsb	r2, r2, #24
 80023a6:	f1c2 0c20 	rsb	ip, r2, #32
 80023aa:	fa10 f30c 	lsls.w	r3, r0, ip
 80023ae:	fa20 f002 	lsr.w	r0, r0, r2
 80023b2:	bf18      	it	ne
 80023b4:	f040 0001 	orrne.w	r0, r0, #1
 80023b8:	ea4f 23c1 	mov.w	r3, r1, lsl #11
 80023bc:	ea4f 23d3 	mov.w	r3, r3, lsr #11
 80023c0:	fa03 fc0c 	lsl.w	ip, r3, ip
 80023c4:	ea40 000c 	orr.w	r0, r0, ip
 80023c8:	fa23 f302 	lsr.w	r3, r3, r2
 80023cc:	ea4f 0343 	mov.w	r3, r3, lsl #1
 80023d0:	e7cc      	b.n	800236c <__aeabi_d2f+0x14>
 80023d2:	ea7f 5362 	mvns.w	r3, r2, asr #21
 80023d6:	d107      	bne.n	80023e8 <__aeabi_d2f+0x90>
 80023d8:	ea50 3301 	orrs.w	r3, r0, r1, lsl #12
 80023dc:	bf1e      	ittt	ne
 80023de:	f04f 40fe 	movne.w	r0, #2130706432	; 0x7f000000
 80023e2:	f440 0040 	orrne.w	r0, r0, #12582912	; 0xc00000
 80023e6:	4770      	bxne	lr
 80023e8:	f001 4000 	and.w	r0, r1, #2147483648	; 0x80000000
 80023ec:	f040 40fe 	orr.w	r0, r0, #2130706432	; 0x7f000000
 80023f0:	f440 0000 	orr.w	r0, r0, #8388608	; 0x800000
 80023f4:	4770      	bx	lr
 80023f6:	bf00      	nop

080023f8 <memcpy>:
 80023f8:	4684      	mov	ip, r0
 80023fa:	ea41 0300 	orr.w	r3, r1, r0
 80023fe:	f013 0303 	ands.w	r3, r3, #3
 8002402:	d16d      	bne.n	80024e0 <memcpy+0xe8>
 8002404:	3a40      	subs	r2, #64	; 0x40
 8002406:	d341      	bcc.n	800248c <memcpy+0x94>
 8002408:	f851 3b04 	ldr.w	r3, [r1], #4
 800240c:	f840 3b04 	str.w	r3, [r0], #4
 8002410:	f851 3b04 	ldr.w	r3, [r1], #4
 8002414:	f840 3b04 	str.w	r3, [r0], #4
 8002418:	f851 3b04 	ldr.w	r3, [r1], #4
 800241c:	f840 3b04 	str.w	r3, [r0], #4
 8002420:	f851 3b04 	ldr.w	r3, [r1], #4
 8002424:	f840 3b04 	str.w	r3, [r0], #4
 8002428:	f851 3b04 	ldr.w	r3, [r1], #4
 800242c:	f840 3b04 	str.w	r3, [r0], #4
 8002430:	f851 3b04 	ldr.w	r3, [r1], #4
 8002434:	f840 3b04 	str.w	r3, [r0], #4
 8002438:	f851 3b04 	ldr.w	r3, [r1], #4
 800243c:	f840 3b04 	str.w	r3, [r0], #4
 8002440:	f851 3b04 	ldr.w	r3, [r1], #4
 8002444:	f840 3b04 	str.w	r3, [r0], #4
 8002448:	f851 3b04 	ldr.w	r3, [r1], #4
 800244c:	f840 3b04 	str.w	r3, [r0], #4
 8002450:	f851 3b04 	ldr.w	r3, [r1], #4
 8002454:	f840 3b04 	str.w	r3, [r0], #4
 8002458:	f851 3b04 	ldr.w	r3, [r1], #4
 800245c:	f840 3b04 	str.w	r3, [r0], #4
 8002460:	f851 3b04 	ldr.w	r3, [r1], #4
 8002464:	f840 3b04 	str.w	r3, [r0], #4
 8002468:	f851 3b04 	ldr.w	r3, [r1], #4
 800246c:	f840 3b04 	str.w	r3, [r0], #4
 8002470:	f851 3b04 	ldr.w	r3, [r1], #4
 8002474:	f840 3b04 	str.w	r3, [r0], #4
 8002478:	f851 3b04 	ldr.w	r3, [r1], #4
 800247c:	f840 3b04 	str.w	r3, [r0], #4
 8002480:	f851 3b04 	ldr.w	r3, [r1], #4
 8002484:	f840 3b04 	str.w	r3, [r0], #4
 8002488:	3a40      	subs	r2, #64	; 0x40
 800248a:	d2bd      	bcs.n	8002408 <memcpy+0x10>
 800248c:	3230      	adds	r2, #48	; 0x30
 800248e:	d311      	bcc.n	80024b4 <memcpy+0xbc>
 8002490:	f851 3b04 	ldr.w	r3, [r1], #4
 8002494:	f840 3b04 	str.w	r3, [r0], #4
 8002498:	f851 3b04 	ldr.w	r3, [r1], #4
 800249c:	f840 3b04 	str.w	r3, [r0], #4
 80024a0:	f851 3b04 	ldr.w	r3, [r1], #4
 80024a4:	f840 3b04 	str.w	r3, [r0], #4
 80024a8:	f851 3b04 	ldr.w	r3, [r1], #4
 80024ac:	f840 3b04 	str.w	r3, [r0], #4
 80024b0:	3a10      	subs	r2, #16
 80024b2:	d2ed      	bcs.n	8002490 <memcpy+0x98>
 80024b4:	320c      	adds	r2, #12
 80024b6:	d305      	bcc.n	80024c4 <memcpy+0xcc>
 80024b8:	f851 3b04 	ldr.w	r3, [r1], #4
 80024bc:	f840 3b04 	str.w	r3, [r0], #4
 80024c0:	3a04      	subs	r2, #4
 80024c2:	d2f9      	bcs.n	80024b8 <memcpy+0xc0>
 80024c4:	3204      	adds	r2, #4
 80024c6:	d008      	beq.n	80024da <memcpy+0xe2>
 80024c8:	07d2      	lsls	r2, r2, #31
 80024ca:	bf1c      	itt	ne
 80024cc:	f811 3b01 	ldrbne.w	r3, [r1], #1
 80024d0:	f800 3b01 	strbne.w	r3, [r0], #1
 80024d4:	d301      	bcc.n	80024da <memcpy+0xe2>
 80024d6:	880b      	ldrh	r3, [r1, #0]
 80024d8:	8003      	strh	r3, [r0, #0]
 80024da:	4660      	mov	r0, ip
 80024dc:	4770      	bx	lr
 80024de:	bf00      	nop
 80024e0:	2a08      	cmp	r2, #8
 80024e2:	d313      	bcc.n	800250c <memcpy+0x114>
 80024e4:	078b      	lsls	r3, r1, #30
 80024e6:	d08d      	beq.n	8002404 <memcpy+0xc>
 80024e8:	f010 0303 	ands.w	r3, r0, #3
 80024ec:	d08a      	beq.n	8002404 <memcpy+0xc>
 80024ee:	f1c3 0304 	rsb	r3, r3, #4
 80024f2:	1ad2      	subs	r2, r2, r3
 80024f4:	07db      	lsls	r3, r3, #31
 80024f6:	bf1c      	itt	ne
 80024f8:	f811 3b01 	ldrbne.w	r3, [r1], #1
 80024fc:	f800 3b01 	strbne.w	r3, [r0], #1
 8002500:	d380      	bcc.n	8002404 <memcpy+0xc>
 8002502:	f831 3b02 	ldrh.w	r3, [r1], #2
 8002506:	f820 3b02 	strh.w	r3, [r0], #2
 800250a:	e77b      	b.n	8002404 <memcpy+0xc>
 800250c:	3a04      	subs	r2, #4
 800250e:	d3d9      	bcc.n	80024c4 <memcpy+0xcc>
 8002510:	3a01      	subs	r2, #1
 8002512:	f811 3b01 	ldrb.w	r3, [r1], #1
 8002516:	f800 3b01 	strb.w	r3, [r0], #1
 800251a:	d2f9      	bcs.n	8002510 <memcpy+0x118>
 800251c:	780b      	ldrb	r3, [r1, #0]
 800251e:	7003      	strb	r3, [r0, #0]
 8002520:	784b      	ldrb	r3, [r1, #1]
 8002522:	7043      	strb	r3, [r0, #1]
 8002524:	788b      	ldrb	r3, [r1, #2]
 8002526:	7083      	strb	r3, [r0, #2]
 8002528:	4660      	mov	r0, ip
 800252a:	4770      	bx	lr

0800252c <Reset_Handler>:

    .section  .text.Reset_Handler
  .weak  Reset_Handler
  .type  Reset_Handler, %function
Reset_Handler:
  ldr   sp, =_estack     /* set stack pointer */
 800252c:	f8df d0a0 	ldr.w	sp, [pc, #160]	; 80025d0 <CLK_PLL_STATUS+0x1c>

/* Copy the data segment initializers from flash to SRAM */
  movs  r1, #0
 8002530:	2100      	movs	r1, #0
  b  LoopCopyDataInit
 8002532:	e003      	b.n	800253c <LoopCopyDataInit>

08002534 <CopyDataInit>:

CopyDataInit:
  ldr  r3, =_sidata
 8002534:	4b27      	ldr	r3, [pc, #156]	; (80025d4 <CLK_PLL_STATUS+0x20>)
  ldr  r3, [r3, r1]
 8002536:	585b      	ldr	r3, [r3, r1]
  str  r3, [r0, r1]
 8002538:	5043      	str	r3, [r0, r1]
  adds  r1, r1, #4
 800253a:	3104      	adds	r1, #4

0800253c <LoopCopyDataInit>:

LoopCopyDataInit:
  ldr  r0, =_sdata
 800253c:	4826      	ldr	r0, [pc, #152]	; (80025d8 <CLK_PLL_STATUS+0x24>)
  ldr  r3, =_edata
 800253e:	4b27      	ldr	r3, [pc, #156]	; (80025dc <CLK_PLL_STATUS+0x28>)
  adds  r2, r0, r1
 8002540:	1842      	adds	r2, r0, r1
  cmp  r2, r3
 8002542:	429a      	cmp	r2, r3
  bcc  CopyDataInit
 8002544:	d3f6      	bcc.n	8002534 <CopyDataInit>
  ldr  r2, =_sbss
 8002546:	4a26      	ldr	r2, [pc, #152]	; (80025e0 <CLK_PLL_STATUS+0x2c>)
  b  LoopFillZerobss
 8002548:	e002      	b.n	8002550 <LoopFillZerobss>

0800254a <FillZerobss>:
/* Zero fill the bss segment. */
FillZerobss:
  movs  r3, #0
 800254a:	2300      	movs	r3, #0
  str  r3, [r2], #4
 800254c:	f842 3b04 	str.w	r3, [r2], #4

08002550 <LoopFillZerobss>:

LoopFillZerobss:
  ldr  r3, = _ebss
 8002550:	4b24      	ldr	r3, [pc, #144]	; (80025e4 <CLK_PLL_STATUS+0x30>)
  cmp  r2, r3
 8002552:	429a      	cmp	r2, r3
  bcc  FillZerobss
 8002554:	d3f9      	bcc.n	800254a <FillZerobss>

08002556 <CLK_PWR_MODE>:
/* Call the clock system intitialization function.*/
@  bl  SystemInit
/* Call static constructors */
@    bl __libc_init_array
/* Call the application's entry point.*/
  CLOCK_INIT
 8002556:	f04f 5280 	mov.w	r2, #268435456	; 0x10000000
 800255a:	4823      	ldr	r0, [pc, #140]	; (80025e8 <CLK_PLL_STATUS+0x34>)
 800255c:	6c01      	ldr	r1, [r0, #64]	; 0x40
 800255e:	ea42 0201 	orr.w	r2, r2, r1
 8002562:	6402      	str	r2, [r0, #64]	; 0x40

08002564 <CLK_BUS_PRESET>:
 8002564:	4a21      	ldr	r2, [pc, #132]	; (80025ec <CLK_PLL_STATUS+0x38>)
 8002566:	6881      	ldr	r1, [r0, #8]
 8002568:	ea42 0201 	orr.w	r2, r2, r1
 800256c:	6082      	str	r2, [r0, #8]

0800256e <CLK_HSE_ON>:
 800256e:	f44f 3280 	mov.w	r2, #65536	; 0x10000
 8002572:	6801      	ldr	r1, [r0, #0]
 8002574:	ea42 0201 	orr.w	r2, r2, r1
 8002578:	6002      	str	r2, [r0, #0]

0800257a <CLK_HSE_RDY>:
 800257a:	6802      	ldr	r2, [r0, #0]
 800257c:	f412 3f00 	tst.w	r2, #131072	; 0x20000
 8002580:	d0fb      	beq.n	800257a <CLK_HSE_RDY>

08002582 <CLK_PLL_PRESET>:
 8002582:	4a1b      	ldr	r2, [pc, #108]	; (80025f0 <CLK_PLL_STATUS+0x3c>)
 8002584:	6042      	str	r2, [r0, #4]

08002586 <CLK_PLL_ON>:
 8002586:	f04f 7280 	mov.w	r2, #16777216	; 0x1000000
 800258a:	6801      	ldr	r1, [r0, #0]
 800258c:	ea42 0201 	orr.w	r2, r2, r1
 8002590:	6002      	str	r2, [r0, #0]

08002592 <CLK_PLL_RDY>:
 8002592:	6802      	ldr	r2, [r0, #0]
 8002594:	f012 7f00 	tst.w	r2, #33554432	; 0x2000000
 8002598:	d0fb      	beq.n	8002592 <CLK_PLL_RDY>

0800259a <CLK_FLASH_LATENCY>:
 800259a:	4a16      	ldr	r2, [pc, #88]	; (80025f4 <CLK_PLL_STATUS+0x40>)
 800259c:	4816      	ldr	r0, [pc, #88]	; (80025f8 <CLK_PLL_STATUS+0x44>)
 800259e:	6801      	ldr	r1, [r0, #0]
 80025a0:	ea42 0201 	orr.w	r2, r2, r1
 80025a4:	6002      	str	r2, [r0, #0]

080025a6 <CLK_ACTIVATE_PLL>:
 80025a6:	f04f 0202 	mov.w	r2, #2
 80025aa:	480f      	ldr	r0, [pc, #60]	; (80025e8 <CLK_PLL_STATUS+0x34>)
 80025ac:	6881      	ldr	r1, [r0, #8]
 80025ae:	ea42 0201 	orr.w	r2, r2, r1
 80025b2:	6082      	str	r2, [r0, #8]

080025b4 <CLK_PLL_STATUS>:
 80025b4:	6882      	ldr	r2, [r0, #8]
 80025b6:	f002 020f 	and.w	r2, r2, #15
 80025ba:	f012 0f0a 	tst.w	r2, #10
 80025be:	d0f9      	beq.n	80025b4 <CLK_PLL_STATUS>

@ Initalize FPU - The source of all evil.
    MOV    R2, #(0xF << 20)
 80025c0:	f44f 0270 	mov.w	r2, #15728640	; 0xf00000
    LDR    R3, =FPU_CPACR
 80025c4:	4b0d      	ldr	r3, [pc, #52]	; (80025fc <CLK_PLL_STATUS+0x48>)
    STR    R2, [R3]
 80025c6:	601a      	str	r2, [r3, #0]

  bl  main
 80025c8:	f000 f81c 	bl	8002604 <main>
  bx  lr
 80025cc:	4770      	bx	lr
 80025ce:	0000      	.short	0x0000

    .section  .text.Reset_Handler
  .weak  Reset_Handler
  .type  Reset_Handler, %function
Reset_Handler:
  ldr   sp, =_estack     /* set stack pointer */
 80025d0:	2001ffff 	.word	0x2001ffff
/* Copy the data segment initializers from flash to SRAM */
  movs  r1, #0
  b  LoopCopyDataInit

CopyDataInit:
  ldr  r3, =_sidata
 80025d4:	08037fb4 	.word	0x08037fb4
  ldr  r3, [r3, r1]
  str  r3, [r0, r1]
  adds  r1, r1, #4

LoopCopyDataInit:
  ldr  r0, =_sdata
 80025d8:	20000000 	.word	0x20000000
  ldr  r3, =_edata
 80025dc:	200008ec 	.word	0x200008ec
  adds  r2, r0, r1
  cmp  r2, r3
  bcc  CopyDataInit
  ldr  r2, =_sbss
 80025e0:	200008f0 	.word	0x200008f0
FillZerobss:
  movs  r3, #0
  str  r3, [r2], #4

LoopFillZerobss:
  ldr  r3, = _ebss
 80025e4:	20009a80 	.word	0x20009a80
/* Call the clock system intitialization function.*/
@  bl  SystemInit
/* Call static constructors */
@    bl __libc_init_array
/* Call the application's entry point.*/
  CLOCK_INIT
 80025e8:	40023800 	.word	0x40023800
 80025ec:	00009400 	.word	0x00009400
 80025f0:	27402a08 	.word	0x27402a08
 80025f4:	00000605 	.word	0x00000605
 80025f8:	40023c00 	.word	0x40023c00

@ Initalize FPU - The source of all evil.
    MOV    R2, #(0xF << 20)
    LDR    R3, =FPU_CPACR
 80025fc:	e000ed88 	.word	0xe000ed88

08002600 <ADC_IRQHandler>:
 * @retval None
*/
    .section  .text.Default_Handler,"ax",%progbits
Default_Handler:
Infinite_Loop:
  b  Infinite_Loop
 8002600:	e7fe      	b.n	8002600 <ADC_IRQHandler>
	...

08002604 <main>:
// Global FIR variable
arm_fir_instance_f32 FIR_Struct[ADS1299_CHANNELS];
uint32_t blockSize, numBlocks;

int main()
{
 8002604:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}

    // Peripheral Configs
    init_GPIO();
 8002608:	f7fe fc32 	bl	8000e70 <init_GPIO>
  This function enables IRQ interrupts by clearing the I-bit in the CPSR.
  Can only be executed in Privileged modes.
 */
__attribute__( ( always_inline ) ) __STATIC_INLINE void __enable_irq(void)
{
  __ASM volatile ("cpsie i" : : : "memory");
 800260c:	b662      	cpsie	i
    __enable_irq();
    init_USART6();
 800260e:	f7fe fc81 	bl	8000f14 <init_USART6>
 8002612:	4e10      	ldr	r6, [pc, #64]	; (8002654 <main+0x50>)
 8002614:	4d10      	ldr	r5, [pc, #64]	; (8002658 <main+0x54>)
 8002616:	f8df 8050 	ldr.w	r8, [pc, #80]	; 8002668 <main+0x64>
 800261a:	4f10      	ldr	r7, [pc, #64]	; (800265c <main+0x58>)
 800261c:	2400      	movs	r4, #0

    // Initialize IIR filters for Real Time Impedance Calculation
    for (int i = 0; i < ADS1299_CHANNELS; i++) {
        arm_biquad_cascade_df2T_init_f32(&biquad_HP_Struct[i], BIQUAD_STAGES_HP, biquad_HP_Coeffs, biquad_HP_State[i]);
 800261e:	eb08 0004 	add.w	r0, r8, r4
 8002622:	4633      	mov	r3, r6
 8002624:	2127      	movs	r1, #39	; 0x27
 8002626:	4a0e      	ldr	r2, [pc, #56]	; (8002660 <main+0x5c>)
 8002628:	f7ff fa90 	bl	8001b4c <arm_biquad_cascade_df2T_init_f32>
        arm_biquad_cascade_df2T_init_f32(&biquad_BP_Struct[i], BIQUAD_STAGES_BP, biquad_BP_Coeffs, biquad_BP_State[i]);
 800262c:	1938      	adds	r0, r7, r4
 800262e:	462b      	mov	r3, r5
 8002630:	2115      	movs	r1, #21
 8002632:	340c      	adds	r4, #12
 8002634:	4a0b      	ldr	r2, [pc, #44]	; (8002664 <main+0x60>)
 8002636:	f7ff fa89 	bl	8001b4c <arm_biquad_cascade_df2T_init_f32>
    init_GPIO();
    __enable_irq();
    init_USART6();

    // Initialize IIR filters for Real Time Impedance Calculation
    for (int i = 0; i < ADS1299_CHANNELS; i++) {
 800263a:	2c60      	cmp	r4, #96	; 0x60
 800263c:	f506 769c 	add.w	r6, r6, #312	; 0x138
 8002640:	f105 05a8 	add.w	r5, r5, #168	; 0xa8
 8002644:	d1eb      	bne.n	800261e <main+0x1a>
        arm_biquad_cascade_df2T_init_f32(&biquad_HP_Struct[i], BIQUAD_STAGES_HP, biquad_HP_Coeffs, biquad_HP_State[i]);
        arm_biquad_cascade_df2T_init_f32(&biquad_BP_Struct[i], BIQUAD_STAGES_BP, biquad_BP_Coeffs, biquad_BP_State[i]);
    }

    init_SPI1();
 8002646:	f7fe fc9f 	bl	8000f88 <init_SPI1>
    ads1299_init();
 800264a:	f7fd ff9b 	bl	8000584 <ads1299_init>
    init_EXTI();
 800264e:	f7fe fca9 	bl	8000fa4 <init_EXTI>
    while (1); //__WFI();
 8002652:	e7fe      	b.n	8002652 <main+0x4e>
 8002654:	200090bc 	.word	0x200090bc
 8002658:	20008670 	.word	0x20008670
 800265c:	2000905c 	.word	0x2000905c
 8002660:	20000004 	.word	0x20000004
 8002664:	20000310 	.word	0x20000310
 8002668:	20008c10 	.word	0x20008c10

0800266c <sqrtf>:
 800266c:	b510      	push	{r4, lr}
 800266e:	ed2d 8b02 	vpush	{d8}
 8002672:	4c2a      	ldr	r4, [pc, #168]	; (800271c <sqrtf+0xb0>)
 8002674:	b08a      	sub	sp, #40	; 0x28
 8002676:	eef0 8a40 	vmov.f32	s17, s0
 800267a:	f000 f853 	bl	8002724 <__ieee754_sqrtf>
 800267e:	f994 3000 	ldrsb.w	r3, [r4]
 8002682:	3301      	adds	r3, #1
 8002684:	eeb0 8a40 	vmov.f32	s16, s0
 8002688:	d009      	beq.n	800269e <sqrtf+0x32>
 800268a:	eeb0 0a68 	vmov.f32	s0, s17
 800268e:	f000 f89d 	bl	80027cc <__fpclassifyf>
 8002692:	b120      	cbz	r0, 800269e <sqrtf+0x32>
 8002694:	eef5 8ac0 	vcmpe.f32	s17, #0.0
 8002698:	eef1 fa10 	vmrs	APSR_nzcv, fpscr
 800269c:	d405      	bmi.n	80026aa <sqrtf+0x3e>
 800269e:	eeb0 0a48 	vmov.f32	s0, s16
 80026a2:	b00a      	add	sp, #40	; 0x28
 80026a4:	ecbd 8b02 	vpop	{d8}
 80026a8:	bd10      	pop	{r4, pc}
 80026aa:	2301      	movs	r3, #1
 80026ac:	4a1c      	ldr	r2, [pc, #112]	; (8002720 <sqrtf+0xb4>)
 80026ae:	9300      	str	r3, [sp, #0]
 80026b0:	ee18 0a90 	vmov	r0, s17
 80026b4:	2300      	movs	r3, #0
 80026b6:	9201      	str	r2, [sp, #4]
 80026b8:	9308      	str	r3, [sp, #32]
 80026ba:	f7ff fbe7 	bl	8001e8c <__aeabi_f2d>
 80026be:	7824      	ldrb	r4, [r4, #0]
 80026c0:	e9cd 0104 	strd	r0, r1, [sp, #16]
 80026c4:	e9cd 0102 	strd	r0, r1, [sp, #8]
 80026c8:	b99c      	cbnz	r4, 80026f2 <sqrtf+0x86>
 80026ca:	2200      	movs	r2, #0
 80026cc:	2300      	movs	r3, #0
 80026ce:	e9cd 2306 	strd	r2, r3, [sp, #24]
 80026d2:	4668      	mov	r0, sp
 80026d4:	f000 f878 	bl	80027c8 <matherr>
 80026d8:	b1a8      	cbz	r0, 8002706 <sqrtf+0x9a>
 80026da:	9b08      	ldr	r3, [sp, #32]
 80026dc:	b9c3      	cbnz	r3, 8002710 <sqrtf+0xa4>
 80026de:	e9dd 0106 	ldrd	r0, r1, [sp, #24]
 80026e2:	f7ff fe39 	bl	8002358 <__aeabi_d2f>
 80026e6:	ee00 0a10 	vmov	s0, r0
 80026ea:	b00a      	add	sp, #40	; 0x28
 80026ec:	ecbd 8b02 	vpop	{d8}
 80026f0:	bd10      	pop	{r4, pc}
 80026f2:	2000      	movs	r0, #0
 80026f4:	2100      	movs	r1, #0
 80026f6:	4602      	mov	r2, r0
 80026f8:	460b      	mov	r3, r1
 80026fa:	f7ff fd45 	bl	8002188 <__aeabi_ddiv>
 80026fe:	2c02      	cmp	r4, #2
 8002700:	e9cd 0106 	strd	r0, r1, [sp, #24]
 8002704:	d1e5      	bne.n	80026d2 <sqrtf+0x66>
 8002706:	f000 f87d 	bl	8002804 <__errno>
 800270a:	2321      	movs	r3, #33	; 0x21
 800270c:	6003      	str	r3, [r0, #0]
 800270e:	e7e4      	b.n	80026da <sqrtf+0x6e>
 8002710:	f000 f878 	bl	8002804 <__errno>
 8002714:	9b08      	ldr	r3, [sp, #32]
 8002716:	6003      	str	r3, [r0, #0]
 8002718:	e7e1      	b.n	80026de <sqrtf+0x72>
 800271a:	bf00      	nop
 800271c:	200004b8 	.word	0x200004b8
 8002720:	08037f90 	.word	0x08037f90

08002724 <__ieee754_sqrtf>:
 8002724:	ee10 3a10 	vmov	r3, s0
 8002728:	f023 4200 	bic.w	r2, r3, #2147483648	; 0x80000000
 800272c:	f1b2 4fff 	cmp.w	r2, #2139095040	; 0x7f800000
 8002730:	b470      	push	{r4, r5, r6}
 8002732:	d230      	bcs.n	8002796 <__ieee754_sqrtf+0x72>
 8002734:	b36a      	cbz	r2, 8002792 <__ieee754_sqrtf+0x6e>
 8002736:	2b00      	cmp	r3, #0
 8002738:	db3d      	blt.n	80027b6 <__ieee754_sqrtf+0x92>
 800273a:	f5b2 0f00 	cmp.w	r2, #8388608	; 0x800000
 800273e:	ea4f 51e3 	mov.w	r1, r3, asr #23
 8002742:	d32c      	bcc.n	800279e <__ieee754_sqrtf+0x7a>
 8002744:	f1a1 027f 	sub.w	r2, r1, #127	; 0x7f
 8002748:	f3c3 0316 	ubfx	r3, r3, #0, #23
 800274c:	07d1      	lsls	r1, r2, #31
 800274e:	f443 0300 	orr.w	r3, r3, #8388608	; 0x800000
 8002752:	bf48      	it	mi
 8002754:	005b      	lslmi	r3, r3, #1
 8002756:	2400      	movs	r4, #0
 8002758:	1056      	asrs	r6, r2, #1
 800275a:	005b      	lsls	r3, r3, #1
 800275c:	4625      	mov	r5, r4
 800275e:	2119      	movs	r1, #25
 8002760:	f04f 7280 	mov.w	r2, #16777216	; 0x1000000
 8002764:	18a8      	adds	r0, r5, r2
 8002766:	4298      	cmp	r0, r3
 8002768:	dc02      	bgt.n	8002770 <__ieee754_sqrtf+0x4c>
 800276a:	1a1b      	subs	r3, r3, r0
 800276c:	1885      	adds	r5, r0, r2
 800276e:	4414      	add	r4, r2
 8002770:	3901      	subs	r1, #1
 8002772:	ea4f 0343 	mov.w	r3, r3, lsl #1
 8002776:	ea4f 0252 	mov.w	r2, r2, lsr #1
 800277a:	d1f3      	bne.n	8002764 <__ieee754_sqrtf+0x40>
 800277c:	b113      	cbz	r3, 8002784 <__ieee754_sqrtf+0x60>
 800277e:	f004 0301 	and.w	r3, r4, #1
 8002782:	441c      	add	r4, r3
 8002784:	1064      	asrs	r4, r4, #1
 8002786:	f104 547c 	add.w	r4, r4, #1056964608	; 0x3f000000
 800278a:	eb04 53c6 	add.w	r3, r4, r6, lsl #23
 800278e:	ee00 3a10 	vmov	s0, r3
 8002792:	bc70      	pop	{r4, r5, r6}
 8002794:	4770      	bx	lr
 8002796:	eea0 0a00 	vfma.f32	s0, s0, s0
 800279a:	bc70      	pop	{r4, r5, r6}
 800279c:	4770      	bx	lr
 800279e:	f413 0200 	ands.w	r2, r3, #8388608	; 0x800000
 80027a2:	d10d      	bne.n	80027c0 <__ieee754_sqrtf+0x9c>
 80027a4:	005b      	lsls	r3, r3, #1
 80027a6:	0218      	lsls	r0, r3, #8
 80027a8:	f102 0201 	add.w	r2, r2, #1
 80027ac:	d5fa      	bpl.n	80027a4 <__ieee754_sqrtf+0x80>
 80027ae:	f1c2 0201 	rsb	r2, r2, #1
 80027b2:	4411      	add	r1, r2
 80027b4:	e7c6      	b.n	8002744 <__ieee754_sqrtf+0x20>
 80027b6:	ee30 0a40 	vsub.f32	s0, s0, s0
 80027ba:	ee80 0a00 	vdiv.f32	s0, s0, s0
 80027be:	e7e8      	b.n	8002792 <__ieee754_sqrtf+0x6e>
 80027c0:	2201      	movs	r2, #1
 80027c2:	4411      	add	r1, r2
 80027c4:	e7be      	b.n	8002744 <__ieee754_sqrtf+0x20>
 80027c6:	bf00      	nop

080027c8 <matherr>:
 80027c8:	2000      	movs	r0, #0
 80027ca:	4770      	bx	lr

080027cc <__fpclassifyf>:
 80027cc:	ee10 3a10 	vmov	r3, s0
 80027d0:	f033 4000 	bics.w	r0, r3, #2147483648	; 0x80000000
 80027d4:	d101      	bne.n	80027da <__fpclassifyf+0xe>
 80027d6:	2002      	movs	r0, #2
 80027d8:	4770      	bx	lr
 80027da:	f5a0 0300 	sub.w	r3, r0, #8388608	; 0x800000
 80027de:	f1b3 4ffe 	cmp.w	r3, #2130706432	; 0x7f000000
 80027e2:	d201      	bcs.n	80027e8 <__fpclassifyf+0x1c>
 80027e4:	2004      	movs	r0, #4
 80027e6:	4770      	bx	lr
 80027e8:	4b05      	ldr	r3, [pc, #20]	; (8002800 <__fpclassifyf+0x34>)
 80027ea:	1e42      	subs	r2, r0, #1
 80027ec:	429a      	cmp	r2, r3
 80027ee:	d801      	bhi.n	80027f4 <__fpclassifyf+0x28>
 80027f0:	2003      	movs	r0, #3
 80027f2:	4770      	bx	lr
 80027f4:	f1a0 40ff 	sub.w	r0, r0, #2139095040	; 0x7f800000
 80027f8:	fab0 f080 	clz	r0, r0
 80027fc:	0940      	lsrs	r0, r0, #5
 80027fe:	4770      	bx	lr
 8002800:	007ffffe 	.word	0x007ffffe

08002804 <__errno>:
 8002804:	4b01      	ldr	r3, [pc, #4]	; (800280c <__errno+0x8>)
 8002806:	6818      	ldr	r0, [r3, #0]
 8002808:	4770      	bx	lr
 800280a:	bf00      	nop
 800280c:	200008e8 	.word	0x200008e8

08002810 <exit>:
 8002810:	b508      	push	{r3, lr}
 8002812:	2100      	movs	r1, #0
 8002814:	4604      	mov	r4, r0
 8002816:	f000 f88b 	bl	8002930 <__call_exitprocs>
 800281a:	4b04      	ldr	r3, [pc, #16]	; (800282c <exit+0x1c>)
 800281c:	6818      	ldr	r0, [r3, #0]
 800281e:	6bc3      	ldr	r3, [r0, #60]	; 0x3c
 8002820:	b103      	cbz	r3, 8002824 <exit+0x14>
 8002822:	4798      	blx	r3
 8002824:	4620      	mov	r0, r4
 8002826:	f000 f95b 	bl	8002ae0 <_exit>
 800282a:	bf00      	nop
 800282c:	08037f9c 	.word	0x08037f9c

08002830 <__libc_init_array>:
 8002830:	b570      	push	{r4, r5, r6, lr}
 8002832:	4e0f      	ldr	r6, [pc, #60]	; (8002870 <__libc_init_array+0x40>)
 8002834:	4d0f      	ldr	r5, [pc, #60]	; (8002874 <__libc_init_array+0x44>)
 8002836:	1b76      	subs	r6, r6, r5
 8002838:	10b6      	asrs	r6, r6, #2
 800283a:	bf18      	it	ne
 800283c:	2400      	movne	r4, #0
 800283e:	d005      	beq.n	800284c <__libc_init_array+0x1c>
 8002840:	3401      	adds	r4, #1
 8002842:	f855 3b04 	ldr.w	r3, [r5], #4
 8002846:	4798      	blx	r3
 8002848:	42a6      	cmp	r6, r4
 800284a:	d1f9      	bne.n	8002840 <__libc_init_array+0x10>
 800284c:	4e0a      	ldr	r6, [pc, #40]	; (8002878 <__libc_init_array+0x48>)
 800284e:	4d0b      	ldr	r5, [pc, #44]	; (800287c <__libc_init_array+0x4c>)
 8002850:	1b76      	subs	r6, r6, r5
 8002852:	f000 f949 	bl	8002ae8 <_init>
 8002856:	10b6      	asrs	r6, r6, #2
 8002858:	bf18      	it	ne
 800285a:	2400      	movne	r4, #0
 800285c:	d006      	beq.n	800286c <__libc_init_array+0x3c>
 800285e:	3401      	adds	r4, #1
 8002860:	f855 3b04 	ldr.w	r3, [r5], #4
 8002864:	4798      	blx	r3
 8002866:	42a6      	cmp	r6, r4
 8002868:	d1f9      	bne.n	800285e <__libc_init_array+0x2e>
 800286a:	bd70      	pop	{r4, r5, r6, pc}
 800286c:	bd70      	pop	{r4, r5, r6, pc}
 800286e:	bf00      	nop
 8002870:	08037fa8 	.word	0x08037fa8
 8002874:	08037fa8 	.word	0x08037fa8
 8002878:	08037fb0 	.word	0x08037fb0
 800287c:	08037fa8 	.word	0x08037fa8

08002880 <memset>:
 8002880:	b470      	push	{r4, r5, r6}
 8002882:	0784      	lsls	r4, r0, #30
 8002884:	d046      	beq.n	8002914 <memset+0x94>
 8002886:	1e54      	subs	r4, r2, #1
 8002888:	2a00      	cmp	r2, #0
 800288a:	d041      	beq.n	8002910 <memset+0x90>
 800288c:	b2cd      	uxtb	r5, r1
 800288e:	4603      	mov	r3, r0
 8002890:	e002      	b.n	8002898 <memset+0x18>
 8002892:	1e62      	subs	r2, r4, #1
 8002894:	b3e4      	cbz	r4, 8002910 <memset+0x90>
 8002896:	4614      	mov	r4, r2
 8002898:	f803 5b01 	strb.w	r5, [r3], #1
 800289c:	079a      	lsls	r2, r3, #30
 800289e:	d1f8      	bne.n	8002892 <memset+0x12>
 80028a0:	2c03      	cmp	r4, #3
 80028a2:	d92e      	bls.n	8002902 <memset+0x82>
 80028a4:	b2cd      	uxtb	r5, r1
 80028a6:	ea45 2505 	orr.w	r5, r5, r5, lsl #8
 80028aa:	2c0f      	cmp	r4, #15
 80028ac:	ea45 4505 	orr.w	r5, r5, r5, lsl #16
 80028b0:	d919      	bls.n	80028e6 <memset+0x66>
 80028b2:	f103 0210 	add.w	r2, r3, #16
 80028b6:	4626      	mov	r6, r4
 80028b8:	3e10      	subs	r6, #16
 80028ba:	2e0f      	cmp	r6, #15
 80028bc:	f842 5c10 	str.w	r5, [r2, #-16]
 80028c0:	f842 5c0c 	str.w	r5, [r2, #-12]
 80028c4:	f842 5c08 	str.w	r5, [r2, #-8]
 80028c8:	f842 5c04 	str.w	r5, [r2, #-4]
 80028cc:	f102 0210 	add.w	r2, r2, #16
 80028d0:	d8f2      	bhi.n	80028b8 <memset+0x38>
 80028d2:	f1a4 0210 	sub.w	r2, r4, #16
 80028d6:	f022 020f 	bic.w	r2, r2, #15
 80028da:	f004 040f 	and.w	r4, r4, #15
 80028de:	3210      	adds	r2, #16
 80028e0:	2c03      	cmp	r4, #3
 80028e2:	4413      	add	r3, r2
 80028e4:	d90d      	bls.n	8002902 <memset+0x82>
 80028e6:	461e      	mov	r6, r3
 80028e8:	4622      	mov	r2, r4
 80028ea:	3a04      	subs	r2, #4
 80028ec:	2a03      	cmp	r2, #3
 80028ee:	f846 5b04 	str.w	r5, [r6], #4
 80028f2:	d8fa      	bhi.n	80028ea <memset+0x6a>
 80028f4:	1f22      	subs	r2, r4, #4
 80028f6:	f022 0203 	bic.w	r2, r2, #3
 80028fa:	3204      	adds	r2, #4
 80028fc:	4413      	add	r3, r2
 80028fe:	f004 0403 	and.w	r4, r4, #3
 8002902:	b12c      	cbz	r4, 8002910 <memset+0x90>
 8002904:	b2c9      	uxtb	r1, r1
 8002906:	441c      	add	r4, r3
 8002908:	f803 1b01 	strb.w	r1, [r3], #1
 800290c:	42a3      	cmp	r3, r4
 800290e:	d1fb      	bne.n	8002908 <memset+0x88>
 8002910:	bc70      	pop	{r4, r5, r6}
 8002912:	4770      	bx	lr
 8002914:	4614      	mov	r4, r2
 8002916:	4603      	mov	r3, r0
 8002918:	e7c2      	b.n	80028a0 <memset+0x20>
 800291a:	bf00      	nop

0800291c <register_fini>:
 800291c:	4b02      	ldr	r3, [pc, #8]	; (8002928 <register_fini+0xc>)
 800291e:	b113      	cbz	r3, 8002926 <register_fini+0xa>
 8002920:	4802      	ldr	r0, [pc, #8]	; (800292c <register_fini+0x10>)
 8002922:	f000 b86f 	b.w	8002a04 <atexit>
 8002926:	4770      	bx	lr
 8002928:	00000000 	.word	0x00000000
 800292c:	08002a11 	.word	0x08002a11

08002930 <__call_exitprocs>:
 8002930:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
 8002934:	4b31      	ldr	r3, [pc, #196]	; (80029fc <__call_exitprocs+0xcc>)
 8002936:	b083      	sub	sp, #12
 8002938:	681b      	ldr	r3, [r3, #0]
 800293a:	9300      	str	r3, [sp, #0]
 800293c:	f503 73a4 	add.w	r3, r3, #328	; 0x148
 8002940:	4681      	mov	r9, r0
 8002942:	460e      	mov	r6, r1
 8002944:	9301      	str	r3, [sp, #4]
 8002946:	9b00      	ldr	r3, [sp, #0]
 8002948:	f8d3 7148 	ldr.w	r7, [r3, #328]	; 0x148
 800294c:	b327      	cbz	r7, 8002998 <__call_exitprocs+0x68>
 800294e:	f8dd a004 	ldr.w	sl, [sp, #4]
 8002952:	687c      	ldr	r4, [r7, #4]
 8002954:	1e65      	subs	r5, r4, #1
 8002956:	d40e      	bmi.n	8002976 <__call_exitprocs+0x46>
 8002958:	3401      	adds	r4, #1
 800295a:	eb07 0484 	add.w	r4, r7, r4, lsl #2
 800295e:	f04f 0800 	mov.w	r8, #0
 8002962:	b1e6      	cbz	r6, 800299e <__call_exitprocs+0x6e>
 8002964:	f8d4 3100 	ldr.w	r3, [r4, #256]	; 0x100
 8002968:	42b3      	cmp	r3, r6
 800296a:	d018      	beq.n	800299e <__call_exitprocs+0x6e>
 800296c:	3d01      	subs	r5, #1
 800296e:	1c6b      	adds	r3, r5, #1
 8002970:	f1a4 0404 	sub.w	r4, r4, #4
 8002974:	d1f5      	bne.n	8002962 <__call_exitprocs+0x32>
 8002976:	4b22      	ldr	r3, [pc, #136]	; (8002a00 <__call_exitprocs+0xd0>)
 8002978:	b173      	cbz	r3, 8002998 <__call_exitprocs+0x68>
 800297a:	687b      	ldr	r3, [r7, #4]
 800297c:	2b00      	cmp	r3, #0
 800297e:	d136      	bne.n	80029ee <__call_exitprocs+0xbe>
 8002980:	683b      	ldr	r3, [r7, #0]
 8002982:	2b00      	cmp	r3, #0
 8002984:	d034      	beq.n	80029f0 <__call_exitprocs+0xc0>
 8002986:	4638      	mov	r0, r7
 8002988:	f8ca 3000 	str.w	r3, [sl]
 800298c:	f3af 8000 	nop.w
 8002990:	f8da 7000 	ldr.w	r7, [sl]
 8002994:	2f00      	cmp	r7, #0
 8002996:	d1dc      	bne.n	8002952 <__call_exitprocs+0x22>
 8002998:	b003      	add	sp, #12
 800299a:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
 800299e:	687b      	ldr	r3, [r7, #4]
 80029a0:	6822      	ldr	r2, [r4, #0]
 80029a2:	3b01      	subs	r3, #1
 80029a4:	42ab      	cmp	r3, r5
 80029a6:	bf0c      	ite	eq
 80029a8:	607d      	streq	r5, [r7, #4]
 80029aa:	f8c4 8000 	strne.w	r8, [r4]
 80029ae:	2a00      	cmp	r2, #0
 80029b0:	d0dc      	beq.n	800296c <__call_exitprocs+0x3c>
 80029b2:	f8d7 3188 	ldr.w	r3, [r7, #392]	; 0x188
 80029b6:	f8d7 b004 	ldr.w	fp, [r7, #4]
 80029ba:	2101      	movs	r1, #1
 80029bc:	40a9      	lsls	r1, r5
 80029be:	4219      	tst	r1, r3
 80029c0:	d108      	bne.n	80029d4 <__call_exitprocs+0xa4>
 80029c2:	4790      	blx	r2
 80029c4:	687b      	ldr	r3, [r7, #4]
 80029c6:	455b      	cmp	r3, fp
 80029c8:	d1bd      	bne.n	8002946 <__call_exitprocs+0x16>
 80029ca:	f8da 3000 	ldr.w	r3, [sl]
 80029ce:	42bb      	cmp	r3, r7
 80029d0:	d0cc      	beq.n	800296c <__call_exitprocs+0x3c>
 80029d2:	e7b8      	b.n	8002946 <__call_exitprocs+0x16>
 80029d4:	f8d7 318c 	ldr.w	r3, [r7, #396]	; 0x18c
 80029d8:	4219      	tst	r1, r3
 80029da:	d104      	bne.n	80029e6 <__call_exitprocs+0xb6>
 80029dc:	4648      	mov	r0, r9
 80029de:	f8d4 1080 	ldr.w	r1, [r4, #128]	; 0x80
 80029e2:	4790      	blx	r2
 80029e4:	e7ee      	b.n	80029c4 <__call_exitprocs+0x94>
 80029e6:	f8d4 0080 	ldr.w	r0, [r4, #128]	; 0x80
 80029ea:	4790      	blx	r2
 80029ec:	e7ea      	b.n	80029c4 <__call_exitprocs+0x94>
 80029ee:	683b      	ldr	r3, [r7, #0]
 80029f0:	46ba      	mov	sl, r7
 80029f2:	461f      	mov	r7, r3
 80029f4:	2f00      	cmp	r7, #0
 80029f6:	d1ac      	bne.n	8002952 <__call_exitprocs+0x22>
 80029f8:	e7ce      	b.n	8002998 <__call_exitprocs+0x68>
 80029fa:	bf00      	nop
 80029fc:	08037f9c 	.word	0x08037f9c
 8002a00:	00000000 	.word	0x00000000

08002a04 <atexit>:
 8002a04:	4601      	mov	r1, r0
 8002a06:	2000      	movs	r0, #0
 8002a08:	4602      	mov	r2, r0
 8002a0a:	4603      	mov	r3, r0
 8002a0c:	f000 b816 	b.w	8002a3c <__register_exitproc>

08002a10 <__libc_fini_array>:
 8002a10:	b538      	push	{r3, r4, r5, lr}
 8002a12:	4b08      	ldr	r3, [pc, #32]	; (8002a34 <__libc_fini_array+0x24>)
 8002a14:	4d08      	ldr	r5, [pc, #32]	; (8002a38 <__libc_fini_array+0x28>)
 8002a16:	1aed      	subs	r5, r5, r3
 8002a18:	10ac      	asrs	r4, r5, #2
 8002a1a:	bf18      	it	ne
 8002a1c:	18ed      	addne	r5, r5, r3
 8002a1e:	d005      	beq.n	8002a2c <__libc_fini_array+0x1c>
 8002a20:	3c01      	subs	r4, #1
 8002a22:	f855 3d04 	ldr.w	r3, [r5, #-4]!
 8002a26:	4798      	blx	r3
 8002a28:	2c00      	cmp	r4, #0
 8002a2a:	d1f9      	bne.n	8002a20 <__libc_fini_array+0x10>
 8002a2c:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
 8002a30:	f000 b860 	b.w	8002af4 <_fini>
 8002a34:	08037fb0 	.word	0x08037fb0
 8002a38:	08037fb4 	.word	0x08037fb4

08002a3c <__register_exitproc>:
 8002a3c:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
 8002a40:	4c25      	ldr	r4, [pc, #148]	; (8002ad8 <__register_exitproc+0x9c>)
 8002a42:	6825      	ldr	r5, [r4, #0]
 8002a44:	f8d5 4148 	ldr.w	r4, [r5, #328]	; 0x148
 8002a48:	4606      	mov	r6, r0
 8002a4a:	4688      	mov	r8, r1
 8002a4c:	4692      	mov	sl, r2
 8002a4e:	4699      	mov	r9, r3
 8002a50:	b3cc      	cbz	r4, 8002ac6 <__register_exitproc+0x8a>
 8002a52:	6860      	ldr	r0, [r4, #4]
 8002a54:	281f      	cmp	r0, #31
 8002a56:	dc18      	bgt.n	8002a8a <__register_exitproc+0x4e>
 8002a58:	1c43      	adds	r3, r0, #1
 8002a5a:	b17e      	cbz	r6, 8002a7c <__register_exitproc+0x40>
 8002a5c:	eb04 0580 	add.w	r5, r4, r0, lsl #2
 8002a60:	2101      	movs	r1, #1
 8002a62:	f8c5 a088 	str.w	sl, [r5, #136]	; 0x88
 8002a66:	f8d4 7188 	ldr.w	r7, [r4, #392]	; 0x188
 8002a6a:	fa01 f200 	lsl.w	r2, r1, r0
 8002a6e:	4317      	orrs	r7, r2
 8002a70:	2e02      	cmp	r6, #2
 8002a72:	f8c4 7188 	str.w	r7, [r4, #392]	; 0x188
 8002a76:	f8c5 9108 	str.w	r9, [r5, #264]	; 0x108
 8002a7a:	d01e      	beq.n	8002aba <__register_exitproc+0x7e>
 8002a7c:	3002      	adds	r0, #2
 8002a7e:	6063      	str	r3, [r4, #4]
 8002a80:	f844 8020 	str.w	r8, [r4, r0, lsl #2]
 8002a84:	2000      	movs	r0, #0
 8002a86:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 8002a8a:	4b14      	ldr	r3, [pc, #80]	; (8002adc <__register_exitproc+0xa0>)
 8002a8c:	b303      	cbz	r3, 8002ad0 <__register_exitproc+0x94>
 8002a8e:	f44f 70c8 	mov.w	r0, #400	; 0x190
 8002a92:	f3af 8000 	nop.w
 8002a96:	4604      	mov	r4, r0
 8002a98:	b1d0      	cbz	r0, 8002ad0 <__register_exitproc+0x94>
 8002a9a:	f8d5 3148 	ldr.w	r3, [r5, #328]	; 0x148
 8002a9e:	2700      	movs	r7, #0
 8002aa0:	e880 0088 	stmia.w	r0, {r3, r7}
 8002aa4:	f8c5 4148 	str.w	r4, [r5, #328]	; 0x148
 8002aa8:	4638      	mov	r0, r7
 8002aaa:	2301      	movs	r3, #1
 8002aac:	f8c4 7188 	str.w	r7, [r4, #392]	; 0x188
 8002ab0:	f8c4 718c 	str.w	r7, [r4, #396]	; 0x18c
 8002ab4:	2e00      	cmp	r6, #0
 8002ab6:	d0e1      	beq.n	8002a7c <__register_exitproc+0x40>
 8002ab8:	e7d0      	b.n	8002a5c <__register_exitproc+0x20>
 8002aba:	f8d4 118c 	ldr.w	r1, [r4, #396]	; 0x18c
 8002abe:	430a      	orrs	r2, r1
 8002ac0:	f8c4 218c 	str.w	r2, [r4, #396]	; 0x18c
 8002ac4:	e7da      	b.n	8002a7c <__register_exitproc+0x40>
 8002ac6:	f505 74a6 	add.w	r4, r5, #332	; 0x14c
 8002aca:	f8c5 4148 	str.w	r4, [r5, #328]	; 0x148
 8002ace:	e7c0      	b.n	8002a52 <__register_exitproc+0x16>
 8002ad0:	f04f 30ff 	mov.w	r0, #4294967295	; 0xffffffff
 8002ad4:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
 8002ad8:	08037f9c 	.word	0x08037f9c
 8002adc:	00000000 	.word	0x00000000

08002ae0 <_exit>:
 8002ae0:	e7fe      	b.n	8002ae0 <_exit>
 8002ae2:	bf00      	nop

08002ae4 <__EH_FRAME_BEGIN__>:
 8002ae4:	0000 0000                                   ....

08002ae8 <_init>:
 8002ae8:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 8002aea:	bf00      	nop
 8002aec:	bcf8      	pop	{r3, r4, r5, r6, r7}
 8002aee:	bc08      	pop	{r3}
 8002af0:	469e      	mov	lr, r3
 8002af2:	4770      	bx	lr

08002af4 <_fini>:
 8002af4:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 8002af6:	bf00      	nop
 8002af8:	bcf8      	pop	{r3, r4, r5, r6, r7}
 8002afa:	bc08      	pop	{r3}
 8002afc:	469e      	mov	lr, r3
 8002afe:	4770      	bx	lr
